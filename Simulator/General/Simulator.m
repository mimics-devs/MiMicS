classdef Simulator < handle
%SIMULATOR This class simulates given scenarios with given algorithms.
%It is the main component of MiMicS.
    
    properties
        % Handle to the configuration
        conf = Config.getInstance();
        
        % List of algorithm names to simulate in a battery
        list_of_algorithms = {};
        
        % List of scenarios to simulate in a battery
        list_of_scenarios = {};
    end
    
    properties (Access = private)
        % Name of currently processed algorithm
        algorithm_name = '';
        
        % Currently processed scenario
        scenario;       
        
        % Plotting handles
        plot_handles = struct();      
    end
    
    methods (Access = private)
        function obj = Simulator
            % Private Constructor
        end
    end %private methods
    
    methods (Static)
        
        function inst = getInstance()
            %GETINSTANCE Returns the one and only instance of this class.
            %
            %   inst = getInstance()
            %
            % If there is no instance, an new will be created.
            %
            % See also INIT.
            persistent simulator_instance
            if isempty(simulator_instance)
                simulator_instance = Simulator;
                inst = simulator_instance;
            else
                inst = simulator_instance;
            end
        end
        
    end %static methods
    
    methods
        
        function term = performBatteryOfSimulations(obj, list_of_algorithms, list_of_scenarios)
        %PERFORMBATTERYOFSIMULATIONS Performs a simulation for each given
        %algorithm with each given scenario.
        %
        %   term = obj.performBatteryOfSimulations(list_of_algorithms, list_of_scenarios)
        %
        % INPUT
        % list_of_algorithms - A cell array of strings naming the algorithms to simulate.
        % list_of_scenarios - A cell array of strings naming the scenarios to simulate.
        %
        % OUTPUT
        % term - True if all simulations terminated properly.
        %
        % See also PERFORMSIMULATION.
            
            obj.list_of_algorithms = list_of_algorithms;
            obj.list_of_scenarios = list_of_scenarios;
            
            disp('SIMULATOR STARTED');
            
            % General preparations
            num_of_sims = numel(list_of_algorithms) * numel(list_of_scenarios);
            
            if num_of_sims < 1
                warning('You need to select at least one algorithm and one scenario to simulate.');
                return;
            end
            
            help_str = sprintf('To perform this selection again, run the follogwing command:\nMiMicS(''simulate'', ''%s'', ''%s'');\n', ...
                strjoin(list_of_algorithms, ','), strjoin(list_of_scenarios, ','));
            disp(help_str);
             
            term = true;        % false means improper termination, due to user algorithm error
            sim_nr = 1;         % just a counter
            
            % In case maps or RobotTypes were changed recently:
            MapManager.getInstance().clear();
            RobotTypeManager.getInstance().clear();
            
            % Create scenario objects
            scenarios = Scenario.empty(numel(list_of_scenarios), 0);
            for i = 1:numel(obj.list_of_scenarios)
                scenarios(i) = Scenario();
                scenario_name = char(obj.list_of_scenarios(i));
                scenarios(i).loadFromFile(scenario_name);
            end
            
            addpath(obj.conf.dir_algorithms);
           
            for i_alg = 1:numel(obj.list_of_algorithms)
                obj.algorithm_name = obj.list_of_algorithms{i_alg};
                
                % Add correct directory
                addpath([obj.conf.dir_algorithms filesep obj.algorithm_name]);
                
                % Create empty folder for algorithm results
                path_result_alg = [obj.conf.dir_results filesep obj.algorithm_name];
                if ~isdir(path_result_alg)
                    mkdir(path_result_alg);
                end
                
                % Simulate scenarios with the current algorithm
                for i_scenario = 1:numel(obj.list_of_scenarios)
                    fprintf('----------------------------------------\n');
                    fprintf('Starting simulation %i of %i: %s/%s', sim_nr, num_of_sims, obj.algorithm_name, scenarios(i_scenario).name);
                    obj.scenario = scenarios(i_scenario);
                    term = term && obj.performSimulation();
                    sim_nr = sim_nr + 1;
                end % for every scenario
                
                rmpath([obj.conf.dir_algorithms filesep obj.algorithm_name])
            end % for every algorithm
            
            disp('ALL SIMULATIONS FINISHED');
            disp(help_str);
           
        end % function performBatteryOfSimulations
        
        function term = performSimulation(obj)
        % PERFORMSIMULATION Performs the algorithm and scenario combination
        % currently loaded (in private attributes of this class) by
        % PERFORMBATTERYOFSIMULATIONS.
        %
        %   term = obj.performSimulation()
        %
        % OUTPUT
        % term - True if the simulation terminated properly, false if an
        % exception occured.

            %% Generic preparations
            
            start_of_sim_timestamp = fix(clock);
            num_robots = size(obj.scenario.initial_stage, 1);
            fprintf(' at %s\n', datestr(start_of_sim_timestamp));
            fprintf('Number of robots: %d. Map: %s.\n', num_robots, obj.scenario.map.name);
            
            if obj.conf.enable_pct
               disp('Parallel computing is enabled. Consider disabling for better perfomance when the number of robots is not large.');
            end

            % Set up the inter-robot communication system
            comm = CommunicationSystem.getInstance();
            comm.init(num_robots);
            
            %% Prepare the robots

            % Set up the robots
            robots = RobotModel.empty( num_robots, 0);
            for i_robot = 1:num_robots
                robots(i_robot) = RobotModel(i_robot);
                robots(i_robot).setPos(obj.scenario.initial_stage(i_robot,1), obj.scenario.initial_stage(i_robot,2));
                % TODO initial velocity
                robots(i_robot).setOri(obj.scenario.initial_stage(i_robot,3));
                robots(i_robot).setGoal(obj.scenario.initial_stage(i_robot,5), obj.scenario.initial_stage(i_robot,6));
                robots(i_robot).setDistArrival(obj.conf.dist_arrival);
                
                % Report state
                robots(i_robot).reportToComm(comm);
            end

            % Robots were initialized with default type, now set actual ones
            for i = 1:size(obj.scenario.robot_types, 1)
                robot_ids = obj.scenario.robot_types{i,2};
                for r = 1:length(robot_ids)
                    robots(robot_ids(r)).setType(obj.scenario.robot_types{i,1});
                end
            end
            
            % Things that can't get done before type assignment:
            % Get min/max velocity, get edges, activate or flag as
            % improvised obstacle
            max_velocity = 0;
            min_velocity = 0;
            robots_edges = cell(num_robots,1);
            for i = 1:num_robots
                robots_edges{i} = robots(i).getEdges();
                max_velocity = max(max_velocity, robots(i).type.v_max);
                min_velocity = min(min_velocity, robots(i).type.v_min);
                if robots(i_robot).dist_to_goal < robots(i).dist_arrival
                    robots(i_robot).deactivate('Initial position is identical to goal.');
                    robots(i_robot).deactivationCleanUp();
                else
                    robots(i).activate();
                end 
            end
            
            % Calculate the maximum number of iterations allowed
            max_num_iter = obj.calcUpperIterLimit(obj.scenario.initial_stage, max_velocity);
            
            % For the deadlock detection system (checks once per minute)
            num_iter_until_speed_check = round(60/obj.conf.delta_t);
            
            %% Prepare the screen
            close all;
            fig = figure(1);
            set(fig, 'name', [obj.algorithm_name '/' obj.scenario.name]);
            obj.plot_handles.stage = axes();
            hold on;
            xlabel('t = 0.0sec');
            
            [obj.plot_handles.robots, max_dist_from_center, x_map_center, y_map_center] = ...
                initPlotStage(robots, obj.scenario.map.lines, obj.plot_handles.stage, false);
            
            num_iter_until_plot = obj.conf.delta_t_plot/obj.conf.delta_t - 1;
 
            %% Prepare results
            
            result_file = prepareLog(obj.algorithm_name, obj.scenario.name, robots, start_of_sim_timestamp);
            
            % Return value of this function, be set to false if an exception occurs
            term = true;
            % An integer code representing the reason this simulation has ended.
            % Available codes are stored as an enum in Config.m
            termination_code = -1;
            % A human-readable string explaining the reason for termination.
            termination_msg = '';
            
            % Counters for the summary file
            num_crashed_with_robot = 0;
            num_crashed_with_static = 0;
            num_reached_goal = 0;
            num_escaped = 0;
            num_on_deadlock = 0;
            
            for i = 1:num_robots
                robots(i).log(comm.current_time);
            end
            
            %% Last preparations
            is_sim_running = true;
            i_sim_step = 0;
            t_start = clock;                        % to measure the overall speed-up
            t_last_iter = clock;                    % to measure the speed-up from plot to plot
            ids_crashed = [];                       % list of robots which crashed during an iteration
            ids_of_active = 1:num_robots;           % using this may increase performance a bit by avoiding checking rob.active
            alg_error_ME = [];                      % Temporary storage for exceptions caused by user's algorithms
            
            % Copies for easy access in parfor
            map_lines = obj.scenario.map.lines;   
            
            if exist([obj.conf.dir_algorithms filesep obj.algorithm_name filesep 'setup.m'], 'file')
               if ~setup(robots, obj.scenario.map)
                   warning('Setup function was unsuccessful! Continuing to next simulation.');
                   is_sim_running = false;
                   termination_code = obj.conf.TERMINATION_CODES.SETUP_ERROR;
                   termination_msg = 'Setup was unsuccessful.';
               else
                   fprintf('Setup function complete.\n')
               end
            end
            fprintf('\n');
            
            %% Perform the simulation
            
            while is_sim_running
                i_sim_step = i_sim_step + 1;
                current_time = i_sim_step * obj.conf.delta_t;
                comm.current_time = current_time;
                num_iter_until_speed_check = num_iter_until_speed_check - 1;      
                
                %% Simulate robots    
                
                % If the enable_pct flag is activated, the simulations will
                % be done in parallel and in serial otherwise. You can
                % change the flag with the configuration GUI.
                % The inner part of the loop is almost identical in both
                % cases. This is not elegant from a computer science point
                % of view, but abides to the Parallel Computing Toolbox'
                % rules and limits overhead.
                
                if obj.conf.enable_pct
                    parfor i_robot = 1:num_robots
                        rob = robots(i_robot);

                        if ~rob.active
                            continue;
                        end                      

                        % Perform the simulation of the laser ranger.
                        [ranger_dists, beam_angles_rel, collision_detected] = ranger(rob, obj.scenario.map, robots_edges);
                        scanner = [ranger_dists, beam_angles_rel];

                        % Decide if it was a vehicle-wall or vehicle-vehicle collision.
                        if collision_detected
                            if collision_detected == 1
                                num_crashed_with_static = num_crashed_with_static + 1;
                                fprintf('[%g] Robot %i collided with a static obstacle at (%g|%g)\n', current_time, rob.id, rob.x_gt, rob.y_gt);
                                rob.deactivate('Detected collision with a static obstacle');
                            else
                                num_crashed_with_robot = num_crashed_with_robot + 1;
                                fprintf('[%g] Robot %i collided with another robot at (%g|%g)\n', current_time, rob.id, rob.x_gt, rob.y_gt);
                                rob.deactivate('Detected collision with another robot');
                            end
                            ids_crashed = [ids_crashed rob.id];
                            continue;
                        end

                        % Execute algorithm
                        v_ref = 0; phi_ref = 0;     % to suppress a specific warning
                        try
                            % Execute algorithm
                            [v_ref, phi_ref] = algorithm(rob, scanner, comm, map_lines);
                        catch ME
                            alg_error_ME = [alg_error_ME ME];
                            continue;
                        end

                        % Simulate the robot during the time step.
                        rob.runControlledSim(v_ref, phi_ref);

                        % Check if the robot arrived at its goal.
                        if rob.dist_to_goal < rob.dist_arrival && rob.v_gt < Config.getInstance().VELOCITY_EPS
                            num_reached_goal = num_reached_goal + 1;
                            fprintf('[%g] Robot %i reached its goal near (%.2f|%.2f)!\n', current_time, rob.id, rob.x_goal, rob.y_goal);
                            rob.deactivate('Reached goal');
                        end

                        % Check if the robot is too far away from the map
                        if abs(x_map_center-rob.x) > max_dist_from_center || abs(y_map_center-rob.y) > max_dist_from_center
                            num_escaped = num_escaped + 1;
                            fprintf('[%g] Robot %i escaped from the map (last known position %.1f,%.1f)\n', current_time, rob.id, rob.x, rob.y);
                            rob.deactivate('Out of the map');
                        end              

                        robots(i_robot) = rob;
                    end  % for every robot (parallel computation)
                else    % if enable_pct
                    for i_robot = ids_of_active
                        rob = robots(i_robot);

                        % Perform the simulation of the laser ranger.
                        [ranger_dists, beam_angles_rel, collision_detected] = ranger(rob, obj.scenario.map, robots_edges);
                        scanner = [ranger_dists, beam_angles_rel];

                        % Decide if it was a vehicle-wall or vehicle-vehicle collision.
                        if collision_detected
                            if collision_detected == 1
                                num_crashed_with_static = num_crashed_with_static + 1;
                                fprintf('[%g] Robot %i collided with a static obstacle at (%g|%g)\n', current_time, rob.id, rob.x_gt, rob.y_gt);
                                rob.deactivate('Detected collision with a static obstacle');
                            else
                                num_crashed_with_robot = num_crashed_with_robot + 1;
                                fprintf('[%g] Robot %i collided with another robot at (%g|%g)\n', current_time, rob.id, rob.x_gt, rob.y_gt);
                                rob.deactivate('Detected collision with another robot');
                            end
                            ids_crashed = [ids_crashed rob.id];
                            continue;
                        end

                        % Execute algorithm
                        try
                            % Execute algorithm
                            [v_ref, phi_ref] = algorithm(rob, scanner, comm, map_lines);
                        catch ME
                            alg_error_ME = [alg_error_ME ME];
                            break;
                        end

                        % Simulate the robot during the time step.
                        rob.runControlledSim(v_ref, phi_ref);

                        % Check if the robot arrived at its goal.
                        if rob.dist_to_goal < rob.dist_arrival && rob.v_gt < Config.getInstance().VELOCITY_EPS
                            num_reached_goal = num_reached_goal + 1;
                            fprintf('[%g] Robot %i reached its goal near (%.2f|%.2f)!\n', current_time, rob.id, rob.x_goal, rob.y_goal);
                            rob.deactivate('Reached goal');
                        end

                        % Check if the robot is too far away from the map
                        if abs(x_map_center-rob.x) > max_dist_from_center || abs(y_map_center-rob.y) > max_dist_from_center
                            num_escaped = num_escaped + 1;
                            fprintf('[%g] Robot %i escaped from the map (last known position %.1f,%.1f)\n', current_time, rob.id, rob.x, rob.y);
                            rob.deactivate('Out of the map');
                        end              

                        robots(i_robot) = rob;
                    end  % for every robot (serial computation)
                end % if enable_pct
                
                %% Check if the simulation is finished.
                
                % Did the algorithm throw an error?
                if numel(alg_error_ME) > 0
                    warning('An error occured during algorithm execution %s. The simulation will be aborted gracefully.\nFull report: %s\n\n', ...
                        alg_error_ME(1).identifier, getReport(alg_error_ME(1)) );
                    is_sim_running = false;
                    term = false;
                    termination_code = obj.conf.TERMINATION_CODES.ALG_ERROR;
                    termination_msg = ['Error in algorithm ' alg_error_ME(1).identifier];
                    for i_robot = ids_of_active
                        robots(i_robot).deactivate('Deactivated due to algorithm error.');
                    end
                    alg_error_ME = [];
                end
                
                % Did we reach a local minimum?
                if is_sim_running && i_sim_step >= max_num_iter
                    is_sim_running = false;
                    termination_code = obj.conf.TERMINATION_CODES.MAXITER;
                    termination_msg = ['Maximum number of iterations (' num2str(max_num_iter) ') reached.'];
                end
                
                % Did a deadlock occur or are there still robots moving?
                if is_sim_running && num_iter_until_speed_check <= 0
                    % Restarts the timer
                    num_iter_until_speed_check = round(60/obj.conf.delta_t); % Checks once each minute of sim
                    
                    if max(abs([robots.v_gt])) < obj.conf.VELOCITY_EPS
                        is_sim_running = false;
                        termination_code = obj.conf.TERMINATION_CODES.DEADLOCK;
                        termination_msg = 'Deadlock occured.';
                        % All robots have essentially stopped, we should deactivate them.
                        for i_robot = 1:num_robots
                            if robots(i_robot).active
                                num_on_deadlock = num_on_deadlock + 1;
                                fprintf('[%g] Robot %d is in a deadlock\n', comm.current_time, i_robot);
                                robots(i_robot).deactivate('On a deadlock');
                            end
                        end
                    end
                else
                    num_iter_until_speed_check = num_iter_until_speed_check - 1;
                end
                
                % Are all robots deactivated?
                if is_sim_running && ~max([robots.active])
                    is_sim_running = false;
                    termination_code = obj.conf.TERMINATION_CODES.COMPLETE;
                    termination_msg = 'All robots were deactivated.';
                end
                
                
                %% Handle recent deactivations
                
                for i_robot = 1:num_robots
                    if robots(i_robot).active
                        continue;
                    end
                    
                    if robots(i_robot).deactivated_recently
                        robots(i_robot).reportToComm(comm);
                        robots_edges{i_robot} = robots(i_robot).getEdges();
                        robots(i_robot).log(current_time);
                        
                        robots(i_robot).deactivationCleanUp();
                        set(obj.plot_handles.robots{i_robot}.filled_polygon,'FaceColor','black')
                        ids_of_active(ids_of_active == i_robot) = [];
                    end
                end
                
                %% Handle recent crashes
                
                % Check if a crash occured nearby the goal of a still
                % active robot. If this is the case, we may increase its
                % allowed arrival distance.
                if obj.conf.enable_dist_arrival_inc
                    for i_crash = ids_crashed
                        rob_cr = robots(i_crash);    % The robot which crashed
                        
                        % See if the goals of still active robots are
                        % affected. If so, increase dist_arrival.
                        for i_act = ids_of_active
                            rob_blocked = robots(i_act);             % The robot whose goal might be affected
                            cr2goal = sqrt( (rob_cr.x_gt - rob_blocked.x_goal)^2 + (rob_cr.y_gt - rob_blocked.y_goal)^2);
                            if cr2goal < (rob_cr.type.safety_radius + rob_blocked.dist_arrival)
                                new_dist_arrival = rob_blocked.dist_arrival + max(rob_cr.type.safety_radius, rob_blocked.type.safety_radius);
                                fprintf('The recent crash of robot %i occured close to the goal of robot %i.', rob_cr.id, rob_blocked.id);
                                fprintf('Will increase its allowed arrival distance from %g to %g.\n', rob_blocked.dist_arrival, new_dist_arrival);
                                rob_blocked.setDistArrival(new_dist_arrival);
                            end
                        end
                    end
                    ids_crashed = [];
                end 
                
                 %% Plotting 
                if (num_iter_until_plot <= 0 || ~is_sim_running)
                    for i = ids_of_active
                        updateRobotPlot(obj.plot_handles.robots{i}, robots(i));
                    end
                    
                    set(get(obj.plot_handles.stage,'Xlabel'), 'String', sprintf('t = %g sec, speedup %.3f', current_time, obj.conf.delta_t_plot/etime(clock, t_last_iter)));
                    t_last_iter = clock;
                    drawnow;
                    num_iter_until_plot = obj.conf.delta_t_plot/obj.conf.delta_t - 1;
                else
                    num_iter_until_plot = num_iter_until_plot - 1;
                end
                
                % If the simulation was already aborted, the next sections
                % can be omitted.
                if ~is_sim_running
                    break;
                end
                
               
                %% Prepare for next simulation step
                for i_robot = ids_of_active
                    % Report new values to communication system
                    robots(i_robot).reportToComm(comm);
                    % Update position of its edges
                    robots_edges{i_robot} = robots(i_robot).getEdges();
                    % Update log file
                    robots(i_robot).log(current_time);
                end % for every robot
                
            end % simulation main loop
            
            %% End of simulation
            
            % Stop the time needed for the simulation and the simulated time (in sec)
            elapsed_time_for_processing = etime(clock, t_start);
            elapsed_time_sim = i_sim_step*obj.conf.delta_t;
            set(get(obj.plot_handles.stage,'Xlabel'), 'String', sprintf('t = %g sec, speedup %.3f', elapsed_time_sim, elapsed_time_sim/elapsed_time_for_processing));
            fprintf('\nThe simulation finished. Elapsed processing time: %s. Simulated  %d loops (%g seconds). Speed-up: %f\n', ...
                datestr(elapsed_time_for_processing/86400,'HH:MM:SS'), i_sim_step, elapsed_time_sim, elapsed_time_sim/elapsed_time_for_processing);
            fprintf('Reason for termination: %s\n', termination_msg);
            fprintf('%i reached their goals, %i escaped, %i crashed with robots, %i crashed with static obstacles, %i were deadlocked.\n\n', ...
                num_reached_goal, num_escaped, num_crashed_with_robot, num_crashed_with_static, num_on_deadlock);
            
            % End the log file
            finishLog(result_file, termination_code, termination_msg, ...
                elapsed_time_for_processing, elapsed_time_sim, obj.scenario.map.lines, ...
                num_reached_goal, num_escaped, num_crashed_with_robot, num_crashed_with_static, num_on_deadlock);
            
            % Append a global summary
            updateSummary(obj.algorithm_name, obj.scenario.name, start_of_sim_timestamp, ...
                num_robots, num_reached_goal, num_crashed_with_robot, num_crashed_with_static, num_escaped, num_on_deadlock);
            
            if exist([obj.conf.dir_algorithms filesep obj.algorithm_name filesep 'shutdown.m'], 'file')
               shutdown();
               disp('Shutdown complete.');
            end
            
        end % function performSimulation
        
        function uplim = calcUpperIterLimit(obj, initial_stage, max_velocity)
        % CALCUPPERITERLIMIT Calculates an upper limit for the number of
        % iterations this simulation needs (utility function).
        %
        %   obj.calculateMaxNumOfIterations(initial_stage, max_velocity)
        %
        % INPUT
        % initial_stage - The stage part of the scenario (initial
        % positions, orientations etc).
        % max_velocity - The maximum possible velocity of any robot in this simulation.
        %
        % OUTPUT
        % uplim - An estimated upper limit for the number of iterations needed.

            % Of all robot origins and goals, find the maximum and minimum coordinate.
            max_coordinate = max([max(initial_stage(:,1:2)) max(initial_stage(:,5:6))]);
            min_coordinate = min([min(initial_stage(:,1:2)) min(initial_stage(:,5:6))]);

            % Calculate the distance between these extremes.
            max_dist = sqrt(2*(max_coordinate-min_coordinate)^2);

            v_multiplier = 0.1;     % estimated parameter

            uplim = ceil( (size(initial_stage,1)/2)*max_dist * 1/(v_multiplier*max_velocity) * (1/obj.conf.delta_t));

        end % calculateMaxNumOfIterations
        
    end % methods
    
end %class

