classdef MapManager < handle
%MAPMANAGER Stores objects of the 'Map' class and makes them accessible by
%name. 
%
% This saves the Simulator the need to reload a map if it was
% previously used by another scenario.
%
% See also MAP.
    
    properties
        % A map (in the computer science sense) of MiMicS 'Map' objects.
        maps = containers.Map();
    end
    
    methods (Access = private)
        function obj = MapManager
            % Private Constructor
            
            obj.maps('BLANK') = Map('BLANK', []);
        end
        
    end %private methods
    
    methods (Static)
        
        function inst = getInstance()
        %GETINSTANCE Returns the one and only instance of this class. 
        %
        %   inst = getInstance()
        %
        % If there is no instance, an new will be created.
        %
        % See also INIT. 
            persistent mapManagerInstance
            if isempty(mapManagerInstance)
                mapManagerInstance = MapManager;
                inst = mapManagerInstance;
            else
                inst = mapManagerInstance;
            end
        end
        
    end %static methods
    
    methods
        
        function clear(obj)
        %CLEAR Empties the internal container.
            remove(obj.maps, obj.maps.keys);
            obj.maps('BLANK') = Map('BLANK', []);
        end
        
        function map = getOrCreateMapByName(obj, map_name)
        %GETORCREATEMAPBYNAME Delivers a 'Map' object, either directly from
        %the internal container if it is already in there, or with prior
        %creation.
        %
        %   map = obj.getOrCreateMapByName(map_name)
        %
        % INPUT
        % map_name - Name of the map (usually file name) to return.
        %
        % OUTPUT
        % map - Instance of 'Map' class.
        

            if ~obj.maps.isKey(map_name)
                % This map has not been seen yet and should be created
                conf = Config.getInstance();
                map_path = [conf.dir_maps filesep map_name '.m'];
                if ~exist(map_path, 'file')
                    error('The file %s does not exist.', map_path);
                end

                % The map is a script that should define a variable named
                % 'map_lines'
                run(map_path);

                obj.maps(map_name) = Map(map_name, map_lines);
            end
            
            map = obj.maps(map_name);
        end
    end % methods
    
end % class

