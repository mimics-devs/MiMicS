classdef CommunicationSystem < handle
%COMMUNICATIONSYSTEM Enables communication between the robots during simulation.
%
% Communication between the robots is necessary for certain collision
% avoidance algorithms. This class takes in reports from individual robots
% about their position, orientation and velocities (possibly nois-polluted)
% and allows other robots to access this data.
%
% Since every robot should communicate over the same system, this class
% follows the Singleton-architecture. There is no public constructor,
% instead you can create and get the one and only instance of this
% class with CommunicationSystem.getInstance().
%
% See also ROBOTMODEL.
    
    properties
         % Current time of the simulation
         current_time;
    end
    
    properties (SetAccess = protected)
        % Matrix of reported values. Row i corresponds to robot i,
        % consisting of [x y v a phi omega]
        matrix
    end
    
     methods (Access = private)
        function obj = CommunicationSystem
            % Private Constructor
        end
        
    end %private methods
    
    methods (Static)
        
        function inst = getInstance()
        %GETINSTANCE Returns the one and only instance of this class. 
        %
        %   inst = getInstance()
        %
        % If there is no instance, an new one will be created.
        %
        % See also INIT. 
            persistent commsys_instance
            if isempty(commsys_instance)
                commsys_instance = CommunicationSystem;
                inst = commsys_instance;
            else
                inst = commsys_instance;
            end
        end
        
    end %static methods
    
    methods
        
        function init(obj, n)
        %INIT Initializes the class.
        %
        %   obj.init(n)
        %
        % INPUT
        % n - Number of robots
            obj.matrix = zeros(n, 6);
            obj.current_time = 0;
        end
        
        function report(obj, i, x, y, v, a, phi, omega)
        %REPORT Stores the reported values in the internal data structure.
        %
        %   obj.report(i, x, y, v, a, phi, omega)
        %
        % INPUT
        % i - Id of the robot == index within the array
        % x - Reported x-position
        % y - Reported y-position
        % v - Reported translational velocity
        % a - Reported acceleration
        % phi - Reported orientation
        % omega - Reported rotational velocity
            obj.matrix(i,:) = [x y v a phi omega];
        end
        
        function time = getTime(obj)
        %GETTIME Returns the current simulated time stored in this class.
        %
        %   obj.getTime()
        %
        % OUTPUT
        % time - The current simulated time in seconds
            time = obj.current_time;
        end
        
        function [x, y] = getPosition(obj, i)
        %GETPOSITION Returns the position robot i has reported last.
        %
        %   [x, y] = obj.getPosition(i)
        %
        % INPUT
        % i - ID of the robot
        %
        % OUTPUT
        % x - X coordinate of robot i in meters (possibly noise-polluted) 
        % y - Y coordinate of robot i in meters (possibly noise-polluted) 
            x = obj.matrix(i,1);
            y = obj.matrix(i,2);
        end
        
        function [x, y] = getAllPositions(obj)
        %GETALLPOSITIONS Returns reported positions of all robots.
        %
        %   [x, y] = obj.getAllPositions()
        %
        % INPUT
        % x - Vector of X coordinates in meters (possibly noise-polluted)
        % y - Vector of Y coordinates in meters (possibly noise-polluted)
            x = obj.matrix(:,1);
            y = obj.matrix(:,2);
        end
        
        function v = getVelocity(obj, i)
        %GETVELOCITY Returns the velocity robot i has reported last.
        %
        %   v = obj.getVelocity(i)
        %
        % INPUT
        % i - ID of the robot
        %
        % OUTPUT
        % v - Velocity of robot i in m/s (possibly noise-polluted)
            v = obj.matrix(i,3);
        end
        
        function a = getAcceleration(obj, i)
        %GETACCELERATION Returns the acceleration robot i has reported last.
        %
        %   a = obj.getAcceleration(i)
        %
        % INPUT
        % i - ID of the robot
        %
        % OUTPUT
        % a - Acceleration of robot i in m/s² (possibly noise-polluted)    
            a = obj.matrix(i,4);
        end
        
        function phi = getOrientation(obj, i)
        %GETORIENTATION Returns the orientation robot i has reported last.
        %
        %   phi = obj.getOrientation(i)
        %
        % INPUT
        % i - ID of the robot
        %
        % OUTPUT
        % phi - Orientation of robot i in rad (possibly noise-polluted)
            phi = obj.matrix(i,5);
        end
        
        function omega = getRotVelocity(obj, i)
        %GETROTVELOCITY Returns the rotational velocity robot i has reported last.
        %
        %   omega = obj.getRotVelocity(i)
        %
        % INPUT
        % i - ID of the robot
        %
        % OUTPUT
        % omaga - Rotational velocity of robot i in rad/s
            omega = obj.matrix(i,6);
        end
        
    end % methods
    
end %class

