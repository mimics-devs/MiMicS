classdef RobotTypeManager < handle
%ROBOTTYPEMANAGER Stores several RobotType instances and makes them
%accessible by name.
%
% See also ROBOTTYPE.
    
    properties
        % A map (in the computer science sense) of RobotType instances
        % TODO: Decleare type/value class in the beginning
        types = containers.Map();
    end
    
    methods (Access = private)
        function obj = RobotTypeManager
            % Private Constructor
        end
        
    end %private methods
    
    methods (Static)
        
        function inst = getInstance()
            %GETINSTANCE Returns the one and only instance of this class.
            %
            %   inst = getInstance()
            %
            % If there is no instance, an new will be created.
            %
            % See also INIT.
            persistent robot_type_manager_instance
            if isempty(robot_type_manager_instance)
                robot_type_manager_instance = RobotTypeManager();
                inst = robot_type_manager_instance;
            else
                inst = robot_type_manager_instance;
            end
        end
        
    end %static methods
    
    methods
        
        function clear(obj)
        %CLEAR Empties the internal container.
        %
        %   obj.clear()
            remove(obj.types, obj.types.keys);
        end
        
        function type = getOrCreateRobotTypeByName(obj, type_name)
        %GETORCREATEROBOTTYPEBYNAME Delivers the requested RobotType.
        % Either directly from the internal storage, or with prior
        % creation.
        %
        %   type = obj.getOrCreateRobotTypeByName(type_name)
        %
        % INPUT
        % type_name - Name of the type to get (usually file_name, or RobotType.DEFAULT_NAME)
        % 
        % OUTPUT
        % type - Instance of RobotType.
        
            if ~obj.types.isKey(type_name)
                % This map has not been seen yet and should be created
                conf = Config.getInstance();
                if strcmp(type_name, RobotType.DEFAULT_NAME)
                    % The default type is requested
                    full_path = type_name;
                else
                    full_path = [conf.dir_robot_types filesep type_name '.m'];
                    if ~exist(full_path, 'file')
                        error('The specified type file %s does not exist.', full_path);
                    end
                end
                
                % Create object and load type data
                obj.types(type_name) = RobotType(type_name, full_path);
            end
            
            type = obj.types(type_name);
            
        end
    end % methods
    
end % class

