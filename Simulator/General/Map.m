classdef Map < handle
    %MAP Represents a 2D map with a name and some lines.
    %
    % See also MAPMANAGER.
    
    properties
        % Name of the map
        name
        
        % Nx4 matrix of n lines. Each line is a vector [x1 x2 y1 y2].
        lines;
        
        % Bounding rectangle [x y w h]
        rect;
    end
    
    methods
        
        function obj = Map(name, lines)
            % Constructor
            
            if ~ischar(name)
                error('name must be a string');
            end
            obj.name = name;
            
            if numel(lines) > 0 && size(lines,2) ~= 4
                % numel(lines) == 0 means blank map
                error('The given line matrix for map %s does not have the requrired number of 4 columns', obj.name);
            end
            
            obj.lines = lines;
            
            if numel(lines) == 0
                obj.rect = [0 0 0 0];
            else
                x_min = min([lines(:,1); lines(:,2)]);
                x_max = max([lines(:,1); lines(:,2)]);
                y_min = min([lines(:,3); lines(:,4)]);
                y_max = max([lines(:,3); lines(:,4)]);
                obj.rect(1) = x_min; 
                obj.rect(2) = y_min;
                obj.rect(3) = x_max - x_min;
                obj.rect(4) = y_max - y_min;
            end
            
        end % constructor
        
        function occ_grid = convertToOccupancyGrid(obj, res)
            %CONVERTTOOCCUPANCYGRID Converts the map to a grid-based
            %representation.
            %
            %    grid = obj.convertToOccupancyGridGrid(res)
            %
            % INPUT
            % res - Resolution in meters
            %
            % OUTPUT
            % occ_grid - Struct representing the occupancy grid map. Has
            % fields 'data' for the actual map (0 = free, 1 = blocked) and
            % 'info' for metainfo.
            
            occ_grid = struct;
            occ_grid.info = struct;
            occ_grid.info.res = res;
            occ_grid.info.origin = [obj.rect(1), obj.rect(2)];
            occ_grid.info.num_rows = ceil((obj.rect(4) + res) / res);
            occ_grid.info.num_cols = ceil((obj.rect(3) + res) / res);
            occ_grid.data = zeros(occ_grid.info.num_rows, occ_grid.info.num_cols);
            
            for l_idx=1:size(obj.lines,1)
                % Bresenham's algorithm
                line = obj.lines(l_idx,:);
                line(1:2) = (line(1:2) - occ_grid.info.origin(1)) .* 1/res;
                line(3:4) = (line(3:4) - occ_grid.info.origin(2)) .* 1/res;
                x1 = round(line(1));
                x2 = round(line(2));
                y1 = round(line(3));
                y2 = round(line(4));
                dx = abs(x2-x1);
                dy = abs(y2-y1);
                steep = abs(dy) > abs(dx);
                if steep, t=dx;dx=dy;dy=t; end
                
                if dy==0
                    q = zeros(dx+1,1);
                else
                    q = [0;diff(mod([floor(dx/2):-dy:-dy*dx+floor(dx/2)]',dx)) >= 0];
                end
                if steep
                    if y1<=y2 
                        y = (y1:y2)';
                    else
                        y = (y1:-1:y2)';
                    end
                    if x1<=x2 
                        x=x1+cumsum(q);
                    else
                        x = x1-cumsum(q); 
                    end
                else
                    if x1<=x2 
                        x=(x1:x2)'; 
                    else
                        x=(x1:-1:x2)'; 
                    end
                    if y1<=y2 
                        y=y1+cumsum(q); 
                    else
                        y=y1-cumsum(q); 
                    end
                end
                
                x = x+1;
                y = y+1;
                
                for i = 1:numel(x)
                   if x(i) > occ_grid.info.num_cols || x(i) < 1 ...
                     || y(i) > occ_grid.info.num_rows || y(i) < 1
                       continue;
                   end
                   occ_grid.data(y(i), x(i)) = 1;
                end
                
            end
            
%             grid_map = obj.scenario.map.convertToOccupancyGrid(0.1);
%             for i = 1:grid_map.info.num_rows
%                 for j = 1:grid_map.info.num_cols
%                     if grid_map.data(i,j)
%                         x = [j*grid_map.info.res, (j+1)*grid_map.info.res, ...
%                             (j+1)*grid_map.info.res, j*grid_map.info.res] ...
%                             + grid_map.info.origin(1) - grid_map.info.res;
%                         y = [i*grid_map.info.res, i*grid_map.info.res, ...
%                             (i+1)*grid_map.info.res, (i+1)*grid_map.info.res] ...
%                             + grid_map.info.origin(2) - grid_map.info.res;
%                         p = patch(x,y,'b');   
%                     end
%                 end
%             end
        end %convertToGrid
    end % methods
    
end % class

