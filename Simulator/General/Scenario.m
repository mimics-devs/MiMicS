classdef Scenario < handle
%SCENARIO Describes a MiMicS scenario to simulate.
%   A scenario is described in a MATLAB script (typically in the folder
%   'Scenarios'. If requested, its values are loaded from such a script
%   into an object of this class.
%   At the minimum, a scenario must define the variable initial_stage,
%   which describes initial positions, orientations, velocities and
%   goals for N robots in a Nx6 matrix.
%   Optionally, it can also define the variables map_name and
%   robot_types. See the documentation for more details.
    
    properties
        % Name of the scenario (string)
        name = 'UNKNOWN';
        
        % Initial stage (a matrix where each row represents initial
        % position, orientation, velocity and goal of a robot)
        initial_stage = [];
        
        % Stores the type of each robot
        robot_types;
        
        % Handle of a map object
        map;
    end
    
    methods
        
        function loadFromFile(obj, name)
        %LOADFROMFILE Loads the scenario from a specified MATLAB script.
        %
        %   obj.loadFromFile(name)
        %
        % INPUT
        % name - The name of the scenario. Also the name of the file within the
        % 'Scenarios' folder to use.
            obj.name = name;
            
            conf = Config.getInstance();
            
            full_path = [conf.dir_scenarios filesep name '.m'];
            if ~exist(full_path, 'file')
                error('The specified scenario file %s does not exist.', full_path);
            end
            
            % Run the script that represents the scenario
            run(full_path);
            
            if ~exist('initial_stage', 'var')
                error('The scenario script %s does not define the variable initial_stage', name);
            end
            if size(initial_stage,2) ~= 6
                error('initial_stage does not have 6 columns (%s)', name);
            end
            
            obj.initial_stage = initial_stage;
            
            % TODO Handle empty robot_types
            
            if exist('robot_types', 'var')
                % TODO Check format of cell array
                obj.robot_types = robot_types;
            else
                obj.robot_types = cell(0,2);
            end
            
            mapManager = MapManager.getInstance();
            if exist('map_name', 'var')
                if ~ischar(map_name)
                    error('map_name is not a string (%s)', name);
                end
                obj.map = mapManager.getOrCreateMapByName(map_name);
            else
                obj.map = mapManager.getOrCreateMapByName('BLANK');
            end
            
            
        end % function loadScenarioFromFile
        
    end %methods
    
end %class

