function varargout = menuConfig(varargin)
% MENUCONFIG MATLAB code for menuConfig.fig
%      MENUCONFIG, by itself, creates a new MENUCONFIG or raises the existing
%      singleton*.
%
%      H = MENUCONFIG returns the handle to a new MENUCONFIG or the handle to
%      the existing singleton*.
%
%      MENUCONFIG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MENUCONFIG.M with the given input arguments.
%
%      MENUCONFIG('Property','Value',...) creates a new MENUCONFIG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before menuConfig_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to menuConfig_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help menuConfig

% Last Modified by GUIDE v2.5 16-Jun-2017 17:03:53

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @menuConfig_OpeningFcn, ...
                   'gui_OutputFcn',  @menuConfig_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before menuConfig is made visible.
function menuConfig_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to menuConfig (see VARARGIN)

% Choose default command line output for menuConfig
handles.output = hObject;

handles.conf = Config.getInstance();

handles.temp_dir_algorithms = handles.conf.dir_algorithms;
handles.temp_dir_results = handles.conf.dir_results;
handles.temp_dir_scenarios = handles.conf.dir_scenarios;
handles.temp_dir_maps = handles.conf.dir_maps;
handles.temp_dir_robottypes = handles.conf.dir_robot_types;

displayConfData(hObject, handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes menuConfig wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = menuConfig_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton_save_config.
function pushbutton_save_config_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_save_config (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    saveConfig(handles);

% --- Executes on button press in pushbutton_default.
function pushbutton_default_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_default (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    button = questdlg('Are you sure? All changes will be lost.', ...
        'Restore factory defaults', ...
        'Restore factory defaults', 'Cancel' , 'Cancel');
    if strcmp(button, 'Cancel')
        return;
    end

    handles.conf.restoreDefaults();
    displayConfData(hObject, handles);

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

button = questdlg('Would you like to save before leaving?', ...
    'Leave configuration editor', ...
    'Save and Exit', 'Just Exit' , 'Save and Exit');
if strcmp(button, 'Save and Exit')
    saveConfig(handles);
end

% Hint: delete(hObject) closes the figure
delete(hObject);

menuMain();

%% General


function edit_delta_t_Callback(hObject, eventdata, handles)
% hObject    handle to edit_delta_t (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_delta_t as text
%        str2double(get(hObject,'String')) returns contents of edit_delta_t as a double

    value = str2double(get(hObject,'String'));
    if isnan(value) || value <= 0
        errordlg('Please enter a valid non-negative number!', 'Invalid input for delta_t');
    set(hObject, 'String', handles.conf.delta_t);
        return;
    end
    
    % TODO What work exactly? Dynamic systems of robots are defined as
    % continous tf
    button = questdlg('Are you sure you want to change the time between steps? This will possibly result in a lot of work!', ...
        'Change delta_t', ...
        'Change delta_t', 'Cancel' , 'Cancel');
    if ~strcmp(button, 'Change delta_t')
         set(hObject, 'String', handles.conf.delta_t);
    end


% --- Executes during object creation, after setting all properties.
function edit_delta_t_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_delta_t (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_dist_arrival_Callback(hObject, eventdata, handles)
% hObject    handle to edit_dist_arrival (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_dist_arrival as text
%        str2double(get(hObject,'String')) returns contents of edit_dist_arrival as a double

    value = str2double(get(hObject,'String'));
    if isnan(value) || value <= 0
        errordlg('Please enter a valid non-negative number!', 'Invalid input for dist_arrival');
        set(hObject, 'String', handles.conf.dist_arrival);
    end


% --- Executes during object creation, after setting all properties.
function edit_dist_arrival_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_dist_arrival (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% Paths

function edit_path_algorithms_Callback(hObject, eventdata, handles)
% hObject    handle to edit_path_algorithms (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_path_algorithms as text
%        str2double(get(hObject,'String')) returns contents of edit_path_algorithms as a double

folder_name = get(hObject,'String');
if ~isdir(folder_name)
    errordlg([folder_name ' is not a valid path!'], 'Invalid input for dir_algorithms');
    set(hObject, 'String', handles.temp_dir_algorithms);
else
    handles.temp_dir_algorithms = folder_name;
end

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_path_algorithms_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_path_algorithms (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_path_results_Callback(hObject, eventdata, handles)
% hObject    handle to edit_path_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_path_results as text
%        str2double(get(hObject,'String')) returns contents of edit_path_results as a double

folder_name = get(hObject,'String');
if isdir(folder_name)
    handles.temp_dir_results = folder_name;
else
    errordlg([folder_name ' is not a valid path!'], 'Invalid input for dir_results');
    set(hObject, 'String', handles.temp_dir_results);
end

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_path_results_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_path_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_path_scenarios_Callback(hObject, eventdata, handles)
% hObject    handle to edit_path_scenarios (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_path_scenarios as text
%        str2double(get(hObject,'String')) returns contents of edit_path_scenarios as a double

folder_name = get(hObject,'String');
if isdir(folder_name)
    handles.temp_dir_scenarios = folder_name;
else
    errordlg([folder_name ' is not a valid path!'], 'Invalid input for dir_scenarios');
    set(hObject, 'String', handles.temp_dir_scenarios);
end

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_path_scenarios_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_path_scenarios (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_path_maps_Callback(hObject, eventdata, handles)
% hObject    handle to edit_path_maps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_path_maps as text
%        str2double(get(hObject,'String')) returns contents of edit_path_maps as a double

folder_name = get(hObject,'String');
if isdir(folder_name)
    handles.temp_dir_maps = folder_name;
else
    errordlg([folder_name ' is not a valid path!'], 'Invalid input for dir_maps');
    set(hObject, 'String', handles.temp_dir_maps);
end

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_path_maps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_path_maps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_path_robottypes_Callback(hObject, eventdata, handles)
% hObject    handle to edit_path_robottypes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_path_robottypes as text
%        str2double(get(hObject,'String')) returns contents of edit_path_robottypes as a double

    folder_name = get(hObject,'String');

    if ~ischar(folder_name) || ~isdir(folder_name)
        errordlg([folder_name ' is not a valid path!'], 'Invalid input for dir_robot_types');
        set(hObject, 'String', handles.temp_dir_robottypes);
        return;
    end

    types_available = dir([folder_name filesep '*.m']);
    if numel(types_available) <= 0
        str = sprintf(['The directory %s does not seem to contain any robot type files, ' ...
            'but there has to be a least one (the default type).\n\n' ...
            'Please select another one or create a robot type within the folder and reselect it.'], folder_name);
        warndlg(str, 'Did not find robot type files.');
        set(hObject, 'String', handles.temp_dir_robottypes);
        return;
    end
     
    handles.temp_dir_robottypes = folder_name;
    
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_path_robottypes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_path_robottypes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_algorithms.
function pushbutton_algorithms_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_algorithms (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

folder_name = uigetdir(handles.temp_dir_algorithms, 'Select folder for algorithms');

if folder_name ~= 0
    handles.temp_dir_algorithms = folder_name;
    set(handles.edit_path_algorithms, 'String', folder_name);
end

guidata(hObject, handles);


% --- Executes on button press in pushbutton_results.
function pushbutton_results_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

folder_name = uigetdir(handles.temp_dir_results, 'Select folder for result storage');

if folder_name ~= 0
    handles.temp_dir_results = folder_name;
    set(handles.edit_path_results, 'String', folder_name);
end

% --- Executes on button press in pushbutton_scenarios.
function pushbutton_scenarios_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_scenarios (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

folder_name = uigetdir(handles.temp_dir_scenarios, 'Select folder for scenarios');

if folder_name ~= 0
    handles.temp_dir_scenarios = folder_name;
    set(handles.edit_path_scenarios, 'String', folder_name);
end

guidata(hObject, handles);

% --- Executes on button press in pushbutton_maps.
function pushbutton_maps_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_maps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

folder_name = uigetdir(handles.temp_dir_maps, 'Select folder of maps');

if folder_name ~= 0
    handles.temp_dir_maps = folder_name;
    set(handles.edit_path_maps, 'String', folder_name);
end


% --- Executes on button press in pushbutton_robottypes.
function pushbutton_robottypes_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_robottypes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

folder_name = uigetdir(handles.temp_dir_robottypes, 'Select folder of robot types');

if folder_name == 0
    return;
end
    types_available = dir([folder_name filesep '*.m']);
    if numel(types_available) <= 0
        str = sprintf(['The directory %s does not seem to contain any robot type files, ' ...
            'but there has to be a least one (the default type).\n\n' ...
            'Please select another one or create a robot type within the folder and reselect it.'], folder_name);
        warndlg(str, 'Did not find robot type files.');
        return;
    end

    handles.temp_dir_robottypes = folder_name;
    set(handles.edit_path_robottypes, 'String', folder_name);
    
%% Plotting

function edit_delta_t_plot_Callback(hObject, eventdata, handles)
% hObject    handle to edit_delta_t_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_delta_t_plot as text
%        str2double(get(hObject,'String')) returns contents of edit_delta_t_plot as a double

    value = str2double(get(hObject,'String'));
    if isnan(value) || value <= 0
        errordlg('Please enter a valid non-negative number!', 'Invalid input for delta_t_plot');
        set(hObject, 'String', handles.conf.delta_t_plot);
    end

% --- Executes during object creation, after setting all properties.
function edit_delta_t_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_delta_t_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_plot_margin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_plot_margin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_plot_margin as text
%        str2double(get(hObject,'String')) returns contents of edit_plot_margin as a double

    value = str2double(get(hObject,'String'));
    if isnan(value) || value <= 0
        errordlg('Please enter a valid non-negative number!', 'Invalid input for plot_margin');
        set(hObject, 'String', handles.conf.plot_margin);
    end

% --- Executes during object creation, after setting all properties.
function edit_plot_margin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_plot_margin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Util

function saveConfig(handles)

    data_robot_log_extra = get(handles.uitable_robot_log_extra, 'Data');

    %TODO Can't robot_log_extra be saved as a cell-array directly?
    robot_log_extra = '';
    for i=1:size(data_robot_log_extra);
        if data_robot_log_extra{i,2}
            if ~strcmp(robot_log_extra, '')
                robot_log_extra = [robot_log_extra ';'];
            end
            robot_log_extra = [robot_log_extra data_robot_log_extra{i,1}];
        end
    end

    handles.conf.robot_log_extra = robot_log_extra;

    handles.conf.dir_algorithms = handles.temp_dir_algorithms;
    handles.conf.dir_results = handles.temp_dir_results;
    handles.conf.dir_scenarios = handles.temp_dir_scenarios;
    handles.conf.dir_maps = handles.temp_dir_maps;
    handles.conf.dir_robot_types = handles.temp_dir_robottypes;

    handles.conf.delta_t = str2double(get(handles.edit_delta_t, 'String'));
    handles.conf.dist_arrival = str2double(get(handles.edit_dist_arrival, 'String'));
    handles.conf.enable_dist_arrival_inc = get(handles.checkbox_enable_dist_arrival_inc, 'Value');
    handles.conf.enable_pct = get(handles.checkbox_enable_pct, 'Value');
    handles.conf.delta_t_plot = str2double(get(handles.edit_delta_t_plot, 'String'));
    handles.conf.plot_margin = str2double(get(handles.edit_plot_margin, 'String'));
    
    handles.conf.saveToFile();

function displayConfData(hObject, handles)
    
% Plotting
set(handles.edit_delta_t_plot, 'String', num2str(handles.conf.delta_t_plot));
set(handles.edit_plot_margin, 'String', num2str(handles.conf.plot_margin));
% Paths
set(handles.edit_path_algorithms, 'String', handles.conf.dir_algorithms);
set(handles.edit_path_results, 'String', handles.conf.dir_results);
set(handles.edit_path_scenarios, 'String', handles.conf.dir_scenarios);
set(handles.edit_path_maps, 'String', handles.conf.dir_maps);
set(handles.edit_path_robottypes, 'String', handles.conf.dir_robot_types);

% Logging
fields =  fieldnames(RobotModel.UNITS);
fields(strcmp(fields, 'x_gt')) = [];    % Remove parts that are always logged
fields(strcmp(fields, 'y_gt')) = [];
fields(strcmp(fields, 'phi_gt')) = [];
data_robot_log_extra = cell(size(fields,1), 2);
robot_log_extra = strsplit(handles.conf.robot_log_extra, ';');
for i = 1:size(fields,1)
    data_robot_log_extra{i,1} = fields{i};
    if sum(strcmp(fields{i}, robot_log_extra)) > 0
        data_robot_log_extra{i,2} = true;
    else
        data_robot_log_extra{i,2} = false;
    end
end
set(handles.uitable_robot_log_extra, 'Data', data_robot_log_extra);
% General
set(handles.edit_delta_t, 'String', num2str(handles.conf.delta_t));
set(handles.edit_dist_arrival, 'String', num2str(handles.conf.dist_arrival));
set(handles.checkbox_enable_dist_arrival_inc, 'Value', handles.conf.enable_dist_arrival_inc);
set(handles.checkbox_enable_pct, 'Value', handles.conf.enable_pct);

guidata(hObject, handles);


% --- Executes on button press in checkbox_enable_dist_arrival_inc.
function checkbox_enable_dist_arrival_inc_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_enable_dist_arrival_inc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_enable_dist_arrival_inc


% --- Executes on button press in checkbox_enable_pct.
function checkbox_enable_pct_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_enable_pct (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_enable_pct

if get(hObject,'Value')
    warndlg(['Enabling the Parallel Computing Toolbox can significantly slow down your simulations.' ...
        'Use only if you want to simulate a very large number of robots.'], 'Parallel Computing');
end