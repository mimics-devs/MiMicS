function simpleMenuDisplay(title, str_functions_cell, callback)

    choice = menu(title, str_functions_cell{:,1} );
    
    if choice > 0
        feval(str_functions_cell{choice,2});
    else
        feval(callback);
    end

end



