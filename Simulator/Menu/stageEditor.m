function varargout = stageEditor(varargin)
% STAGEDITOR MATLAB code for scenarioEditor.fig
%      SCENARIOEDITOR, by itself, creates a new STAGEDITOR or raises the existing
%      singleton*.
%
%      H = STAGEDITOR returns the handle to a new STAGEDITOR or the handle to
%      the existing singleton*.
%
%      STAGEDITOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STAGEDITOR.M with the given input arguments.
%
%      STAGEDITOR('Property','Value',...) creates a new STAGEDITOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before stageEditor_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to scenarioEditor via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help stageEditor

% Last Modified by GUIDE v2.5 12-Apr-2017 14:26:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @stageEditor_OpeningFcn, ...
                   'gui_OutputFcn',  @stageEditor_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before stageEditor is made visible.
function stageEditor_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to stageEditor (see VARARGIN)

% Choose default command line output for stageEditor
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes stageEditor wait for user response (see UIRESUME)
% uiwait(handles.figure1);

conf = Config.getInstance();
rtm = RobotTypeManager.getInstance();
rtm.clear();

cla(handles.axes_stage, 'reset');
hold(handles.axes_stage, 'on');
handles.robots = [];
handles.rob_plots = {};
handles.sel_idx = [];
handles.map_lines = [];
handles.map_rect_default = [-5 -5 10 10];
handles.map_rect = handles.map_rect_default;
set(handles.axes_stage, 'Xlim', [handles.map_rect(1) (handles.map_rect(1) + handles.map_rect(3))]);
set(handles.axes_stage, 'YLim', [handles.map_rect(2) (handles.map_rect(2) + handles.map_rect(4))]);

% Display available maps
maps_available = dir([conf.dir_maps filesep '*.m']);
map_list = {'None'};
for i = 1:numel(maps_available)
    [~, map_name, ~] = fileparts(maps_available(i).name);
    map_list{i+1} = map_name;
end
set(handles.popup_map_selection, 'String', map_list);

% Display available robot types (default and scripts in designated folder)
types_available = dir([conf.dir_robot_types filesep '*.m']);
type_list = cell(size(types_available,1) +1,1 );
type_list{1} = RobotType.DEFAULT_NAME;
for i = 1:numel(types_available)
    [~, type_name, ~] = fileparts(types_available(i).name);
    type_list{i+1} = type_name;
end
set(handles.popupmenu_robot_type, 'String', type_list);

% Predefine colors for up to 20 robots
handles.colors = jet(20);
handles.colors = handles.colors(randperm(20), :);
% Prepare robot array
handles.robots = RobotModel.empty(0,0);
% Prepare id_counter (to keep track of robots created and assign unique
% colors)
handles.rob_id = 1;

% Shrink/Enlarge window to fit 2/3 of minimum of screen width or height
mon_pos = get(0, 'MonitorPosition');
% Select biggest monitor
[~, idx] = max(mon_pos(:,3));   
mon_pos = mon_pos(idx(1), :);
scr_left = mon_pos(1); scr_top = mon_pos(2); scr_width = mon_pos(3); scr_height = mon_pos(4);
set(gcf, 'Units', 'pixels');
win_pos = get(gcf, 'OuterPosition');
new_width = win_pos(3);
new_height = win_pos(4);
if scr_width > scr_height
    % Normal case: Landscape format
    new_height = scr_height/1.5;
    new_width = new_width*(new_height/win_pos(4));
else
    % Portrait
    new_width = scr_width/1.5;
    new_height = new_height*(new_width/win_pos(3));
end
new_left = scr_left + scr_width/2 - new_width/2;
new_top = scr_top + scr_height/2 - new_height/2;
 set(gcf,'OuterPosition', [new_left new_top new_width new_height]);
 
%TODO Load Images
% [button_im, button_im_map] = loadImage('zoom-in.png', 0.8*[1 1 1]);
% button_sz = getpixelposition(handles.pushbutton_zoom_in);
% button_im = imresize(button_im, [button_sz(3) button_sz(4)]);
% set(handles.pushbutton_zoom_in, 'cdata', button_im);
% 
% [button_im, button_im_map] = loadImage('zoom-out.png', 0.8*[1 1 1]);
% button_sz = getpixelposition(handles.pushbutton_zoom_out);
% button_im = imresize(button_im, [button_sz(3) button_sz(4)]);
% set(handles.pushbutton_zoom_out, 'cdata', button_im);


% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = stageEditor_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popup_map_selection.
function popup_map_selection_Callback(hObject, eventdata, handles)
% hObject    handle to popup_map_selection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_map_selection contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_map_selection

map_names = cellstr(get(hObject,'String'));
value = get(hObject, 'Value');

if isfield(handles, 'map_lines')
    delete(handles.map_lines);
    handles.map_lines = [];
end

if value > 1
    % A map was selected. Draw it!
    map = MapManager.getInstance().getOrCreateMapByName(map_names{value});
    [handles.map_lines, handles.map_rect] = drawMap(map.lines, handles.axes_stage);  
else
    % The user does not want to use a map.
    handles.map_rect = handles.map_rect_default;
end

resetView(hObject, handles);

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function popup_map_selection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_map_selection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_zoom_out.
function pushbutton_zoom_out_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_zoom_out (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

xlim = get(handles.axes_stage, 'XLim');
ylim = get(handles.axes_stage, 'YLim');
x_center = (xlim(2) + xlim(1))/2;
y_center = (ylim(2) + ylim(1))/2;

half_a = (x_center - xlim(1))*1.25;
xlim(1) = x_center - half_a;
xlim(2) = x_center + half_a;
ylim(1) = y_center - half_a;
ylim(2) = y_center + half_a;

set(handles.axes_stage, 'Xlim', xlim);
set(handles.axes_stage, 'YLim', ylim);

% --- Executes on button press in pushbutton_zoom_in.
function pushbutton_zoom_in_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_zoom_in (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

xlim = get(handles.axes_stage, 'XLim');
ylim = get(handles.axes_stage, 'YLim');
x_center = (xlim(2) + xlim(1))/2;
y_center = (ylim(2) + ylim(1))/2;

half_a = (x_center - xlim(1))*0.75;
xlim(1) = x_center - half_a;
xlim(2) = x_center + half_a;
ylim(1) = y_center - half_a;
ylim(2) = y_center + half_a;

set(handles.axes_stage, 'Xlim', xlim);
set(handles.axes_stage, 'YLim', ylim);

%% Add robots, clear all
% --- Executes on button press in pushbutton_add_robot.
function pushbutton_add_robot_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_add_robot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% User has to click in the axes first, nothing else allowed
enableCurrentRobotPanel('off');
set(handles.pushbutton_add_robot, 'Enable', 'off');
set(handles.pushbutton_clear_all_robs, 'Enable', 'off');
set(findall(handles.uipanel_map, '-property', 'enable'), 'enable', 'off')
set(handles.text_help, 'String', 'Click somewhere within the axes to place the robot.');

new_rob = RobotModel(handles.rob_id);
handles.rob_id = handles.rob_id + 1;

% Get position
[x_pos, y_pos] =  ginputax(handles.axes_stage, 1);

% Move robot to position, save and plot it
new_rob.setPos(x_pos, y_pos);
handles.robots = [handles.robots new_rob];

% Set type to last one selected
type_list = get(handles.popupmenu_robot_type, 'String');
type_idx = get(handles.popupmenu_robot_type, 'Value');
new_rob.setType(type_list{type_idx});

handles = addRobotPlot(handles, new_rob);

% Display data and enable editing
handles.sel_idx = numel(handles.robots);
curr_rob = handles.robots(handles.sel_idx);
displayCurrentRobotData(hObject, handles, curr_rob);

set(handles.text_help, 'String', '');
set(handles.pushbutton_clear_all_robs, 'Enable', 'on');
set(handles.pushbutton_add_robot, 'Enable', 'on');
set(findall(handles.uipanel_map, '-property', 'enable'), 'enable', 'on');

% Update handles structure
guidata(hObject, handles);

function handles = addRobotPlot(handles, rob)
    
    color = getColor(handles, rob.id);
    handles.rob_plots{end+1} = initPlotRobot(handles.axes_stage, color, rob, true);

    % Make robot selectable in GUI
    fields = fieldnames(handles.rob_plots{end});
    for i = 1:numel(fields)
      set(handles.rob_plots{end}.(fields{i}), 'ButtonDownFcn', @robot_selection_Callback);
    end
    
 % --- Executes on button press in pushbutton_clear_all_robs.
function pushbutton_clear_all_robs_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_clear_all_robs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    button = questdlg('Are you sure?', ...
        'Clear all robots', ...
        'Clear all robots', 'Cancel' , 'Cancel');
    if ~strcmp(button, 'Clear all robots')
        return
    end

    enableCurrentRobotPanel('off');
    for i = 1:numel(handles.ro)
        handles.robots(i).deletePlot();
    end
    %set(handles.popupmenu_robot_type, 'Value', handles.default_type_idx);
    handles.sel_idx = [];
    handles.robots = [];
    handles.rob_id = 1;
    resetView(hObject, handles);
    
    guidata(hObject, handles);

%% Position

% --- Executes on button press in pushbutton_position.
function pushbutton_position_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_position (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    set(handles.text_help, 'String', 'Click somewhere within the axes to place the robot.');
    [x_pos, y_pos] =  ginputax(handles.axes_stage, 1);
    rob = handles.robots(handles.sel_idx);
    rob.setPos(x_pos, y_pos);
    set(handles.edit_position_x, 'String', num2str(x_pos));
    set(handles.edit_position_y, 'String', num2str(y_pos));
    set(handles.text_help, 'String', '');
    updateRobotPlot(handles.rob_plots{handles.sel_idx}, rob);
    

function edit_position_x_Callback(hObject, eventdata, handles)
% hObject    handle to edit_position_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_position_x as text
%        str2double(get(hObject,'String')) returns contents of edit_position_x as a double

val = str2double(get(hObject,'String'));
if isnan(val)
    warning('You must enter a number');
    set(hObject, 'String', num2str(handles.sel_idx.x_gt));
else
    rob = handles.robots(handles.sel_idx);
    rob.setPos(val, rob.y_gt);
    updateRobotPlot(handles.rob_plots{handles.sel_idx}, rob);
end

% --- Executes during object creation, after setting all properties.
function edit_position_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_position_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_position_y_Callback(hObject, eventdata, handles)
% hObject    handle to edit_position_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_position_y as text
%        str2double(get(hObject,'String')) returns contents of edit_position_y as a double

val = str2double(get(hObject,'String'));
if isnan(val)
    warning('You must enter a number');
    set(hObject, 'String', num2str(handles.sel_idx.y_gt));
else
    rob = handles.robots(handles.sel_idx);
    rob.setPos(rob.x_gt, val);
    updateRobotPlot(handles.rob_plots{handles.sel_idx}, rob);
end

% --- Executes during object creation, after setting all properties.
function edit_position_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_position_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% Orientation 

% --- Executes on button press in pushbutton_orientation.
function pushbutton_orientation_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_orientation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    set(handles.text_help, 'String', 'Click somewhere within the axes to define where the robot should be headed.');
    [x_ori, y_ori] =  ginputax(handles.axes_stage, 1);
    rob = handles.robots(handles.sel_idx);
    x_gt = rob.x_gt;
    y_gt = rob.y_gt;
    
    phi = atan2( (y_ori-y_gt), (x_ori-x_gt) );
    rob.setOri(phi);
    set(handles.edit_orientation, 'String', num2str(phi));
    updateRobotPlot(handles.rob_plots{handles.sel_idx}, rob);
    set(handles.text_help, 'String', '');

function edit_orientation_Callback(hObject, eventdata, handles)
% hObject    handle to edit_orientation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_orientation as text
%        str2double(get(hObject,'String')) returns contents of edit_orientation as a double

val = str2double(get(hObject,'String'));
rob = handles.robots(handles.sel_idx);
if isnan(val)
    warning('You must enter a number');
    set(hObject, 'String', num2str(rob.phi_gt));
else
    rob.setOri(val);
    updateRobotPlot(handles.rob_plots{handles.sel_idx}, rob);
end

% --- Executes during object creation, after setting all properties.
function edit_orientation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_orientation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% Goal
% --- Executes on button press in pushbutton_goal.
function pushbutton_goal_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_goal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    set(handles.text_help, 'String', 'Click somewhere within the axes to select a goal for the robot.');
    [x_goal, y_goal] =  ginputax(handles.axes_stage, 1);
    rob = handles.robots(handles.sel_idx);
    rob.setGoal(x_goal, y_goal);
    set(handles.rob_plots{handles.sel_idx}.goal, 'XData', x_goal);
    set(handles.rob_plots{handles.sel_idx}.goal, 'YData', y_goal);
    set(handles.edit_goal_x, 'String', num2str(x_goal));
    set(handles.edit_goal_y, 'String', num2str(y_goal));
    set(handles.text_help, 'String', '');

function edit_goal_x_Callback(hObject, eventdata, handles)
% hObject    handle to edit_goal_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_goal_x as text
%        str2double(get(hObject,'String')) returns contents of edit_goal_x as a double

val = str2double(get(hObject,'String'));
if isnan(val)
    warning('You must enter a number');
    set(hObject, 'String', num2str(handles.sel_idx.x_goal));
else
    rob = handles.robots(handles.sel_idx);
    rob.setGoal(val, rob.y_goal);
    set(handles.rob_plots{handles.sel_idx}.goal, 'XData', val);
    updateRobotPlot(handles.rob_plots{handles.sel_idx}, rob);
end

% --- Executes during object creation, after setting all properties.
function edit_goal_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_goal_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_goal_y_Callback(hObject, eventdata, handles)
% hObject    handle to edit_goal_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_goal_y as text
%        str2double(get(hObject,'String')) returns contents of edit_goal_y as a double

val = str2double(get(hObject,'String'));
if isnan(val)
    warning('You must enter a number');
    set(hObject, 'String', num2str(handles.sel_idx.y_goal));
else
    rob = handles.robots(handles.sel_idx);
    rob.setGoal(rob.x_goal, val);
    set(handles.rob_plots{handles.sel_idx}.goal, 'YData', val);
end

% --- Executes during object creation, after setting all properties.
function edit_goal_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_goal_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_robot_type.
function popupmenu_robot_type_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_robot_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_robot_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_robot_type

    type_list = get(handles.popupmenu_robot_type, 'String');
    type_idx = get(handles.popupmenu_robot_type, 'Value');
    rob = handles.robots(handles.sel_idx);
    rob.setType(type_list{type_idx});
    updateRobotPlot(handles.rob_plots{handles.sel_idx}, rob);
    
    guidata(hObject, handles); 


% --- Executes during object creation, after setting all properties.
function popupmenu_robot_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_robot_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_delete_robot.
function pushbutton_delete_robot_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_delete_robot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    enableCurrentRobotPanel('off');
    
    sel_idx = handles.sel_idx;
    handles.robots(sel_idx) = [];
    deleteRobotPlot(handles.rob_plots{sel_idx});
    handles.rob_plots = {handles.rob_plots{1:(sel_idx-1)}, handles.rob_plots{(sel_idx+1):end}};
    handles.sel_idx = [];
    guidata(hObject, handles);
    
    
%% uimenu generate scenarios

% --------------------------------------------------------------------
function uimenu_generate_scenario_Callback(hObject, eventdata, handles)
% hObject    handle to uimenu_generate_scenario (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function uimenu_generate_random_Callback(hObject, eventdata, handles)
% hObject    handle to uimenu_generate_random (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    set(handles.text_help, 'String', '');
    
    xlim = get(handles.axes_stage, 'XLim');
    ylim = get(handles.axes_stage, 'YLim');
    num_robots = 0;   
    x_center = (xlim(2) + xlim(1))/2;
    y_center = (ylim(2) + ylim(1))/2;
    window_width = xlim(2) - xlim(1);
    
    prompt = {'Enter number of robots:','Enter x coordinate of center:', 'Enter y coordinate of center:', 'Enter window width:'};
    dlg_title = 'Random robot placement';
    num_lines = 1;

    input_complete = false;
    while ~input_complete
        default_ans = {num2str(num_robots), num2str(x_center), num2str(y_center), num2str(window_width)};
        answer = inputdlg(prompt,dlg_title,num_lines,default_ans);
        
        if isempty(answer)   % User pressed cancel
            return;
        end
        
        num_robots = str2double(answer{1});
        x_center = str2double(answer{2});
        y_center = str2double(answer{3});
        window_width = str2double(answer{4});
        if isnan(num_robots) || num_robots < 1
            warning('You must enter a valid integer for the number of robots!');
            continue;
        end
        if isnan(x_center) || isnan(y_center)
            warning('Invalid input for circle coordinates');
            continue;
        end
        if isnan(window_width) || window_width <= 0
            warning('Invalid input for window width');
            continue;
        end
        input_complete = true;
    end
    
    % Type selection
    types = get(handles.popupmenu_robot_type, 'String');
    [type_select, ok] = listdlg('PromptString','Select a type:',...
                'SelectionMode','single',...
                'ListString', types);
            
    if ~ok      % User pressed cancel
        return;
    end
    
    type_name = types{type_select};
    
    w_half = window_width/2;
    
    for i_rob = 1:num_robots
        new_rob = RobotModel(handles.rob_id);
        handles.rob_id = handles.rob_id + 1;
        new_rob.setType(type_name);

        % Position, orientation goal
        new_rob.setPos(x_center-w_half + rand()*window_width, ...
                       y_center-w_half + rand()*window_width);
        %TODO Set goal away from robot?
        new_rob.setGoal(x_center-w_half + rand()*window_width, ...
                       y_center-w_half + rand()*window_width);
        new_rob.setOri(-pi + rand()*2*pi);

        handles.robots = [handles.robots new_rob];
        handles = addRobotPlot(handles, new_rob);
    end % for every robot
    
    set(handles.text_help, 'String', ['Random placement of ' num2str(num_robots) ' robots complete.']);
    guidata(hObject, handles);
    
 % --------------------------------------------------------------------
function uimenu_generate_circle_Callback(hObject, eventdata, handles)
% hObject    handle to uimenu_generate_circle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    set(handles.text_help, 'String', '');
    
    xlim = get(handles.axes_stage, 'XLim');
    ylim = get(handles.axes_stage, 'YLim');
    num_robots = 0;   
    x_center = (xlim(2) + xlim(1))/2;
    y_center = (ylim(2) + ylim(1))/2;
    radius = xlim(2) - x_center;
     
    prompt = {'Enter number of robots:','Enter x coordinate of circle center:', 'Enter y coordinate of circle center:', 'Enter radius:'};
    dlg_title = 'Circular robot placement';
    num_lines = 1;
    
    input_complete = false;
    while ~input_complete
        default_ans = {num2str(num_robots), num2str(x_center), num2str(y_center), num2str(radius)};
        answer = inputdlg(prompt,dlg_title,num_lines,default_ans);
        
        if isempty(answer)   % User pressed cancel
            return;
        end
        
        num_robots = str2double(answer{1});
        x_center = str2double(answer{2});
        y_center = str2double(answer{3});
        radius = str2double(answer{4});
        if isnan(num_robots) || num_robots < 1
            warning('You must enter a valid integer for the number of robots!');
            continue;
        end
        if isnan(x_center) || isnan(y_center)
            warning('Invalid input for circle coordinates');
            continue;
        end
        if isnan(radius) || radius <= 0
            warning('Invalid input for circle radius');
            continue;
        end
        input_complete = true;
    end
    
    % Type selection
    types = get(handles.popupmenu_robot_type, 'String');
    [type_select, ok] = listdlg('PromptString','Select a type:',...
                'SelectionMode','single',...
                'ListString', types);       
    if ~ok          % User pressed cancel
        return;
    end
    type_name = types{type_select};
    
    for i_rob = 1:num_robots
        new_rob = RobotModel(handles.rob_id);
        handles.rob_id = handles.rob_id + 1;
        new_rob.setType(type_name);
        
        angle_outward = (i_rob-1)*(2*pi/num_robots);
        
        pos_x = x_center + radius*cos(angle_outward);
        pos_y = y_center + radius*sin(angle_outward);
        goal_x = x_center + radius*cos(angle_outward + pi);
        goal_y = y_center + radius*sin(angle_outward + pi);

        % Position, orientation goal
        new_rob.setPos(pos_x, pos_y);
        new_rob.setGoal(goal_x, goal_y);
        new_rob.setOri(limitAngleToPi(angle_outward + pi));

        handles.robots = [handles.robots new_rob];
        handles = addRobotPlot(handles, new_rob);

    end % for every robot
    
    set(handles.text_help, 'String', ['Circular placement of ' num2str(num_robots) ' robots complete.']);
    
    guidata(hObject, handles);

    
%% New, open, save (menu panel)
% --------------------------------------------------------------------
function uipushtool_new_scenario_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool_new_scenario (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    button = questdlg('Are you sure? All progress will be lost if not saved.', ...
        'Create new scenario', ...
        'Create new scenario', 'Cancel' , 'Cancel');
    if strcmp(button, 'Create new scenario')
        cleanUp(hObject, handles);
    end
% --------------------------------------------------------------------
function uipushtool_open_scenario_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool_open_scenario (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    default_name = [Config.getInstance().dir_scenarios filesep '*.m'];
    [file_name, ~, ~] = uigetfile({'*.m'}, 'Open scenario', default_name );
    
    % TODO: What if user selects a scenario outside of the current scenario
    % folder?
    
    if strcmp(file_name, '')
        % User pressed cancel
        return;
    end
    
    handles = cleanUp(hObject, handles);
    
    [~, scenario_name, ~] = fileparts(file_name);
    scenario = Scenario();
    % TODO: Fail-safe this
    scenario.loadFromFile(scenario_name);
    
    % Plot map
    if isa(scenario.map, 'Map')
        map_names = get(handles.popup_map_selection, 'String'); 
        for i_map = 1:numel(map_names)
            if strcmp(map_names{i_map}, scenario.map.name)
                set(handles.popup_map_selection, 'Value', i_map)
                [handles.map_lines, handles.map_rect] = drawMap(scenario.map.lines, handles.axes_stage);
                break;
            end
        end
    end
    
    % Add robots
    num_rob = size(scenario.initial_stage, 1);
    handles.robots = RobotModel.empty(num_rob, 0);
    for i_rob = 1:num_rob
        handles.robots(i_rob) = RobotModel(i_rob);
        handles.robots(i_rob).setPos(scenario.initial_stage(i_rob, 1), scenario.initial_stage(i_rob, 2));
        handles.robots(i_rob).setOri(scenario.initial_stage(i_rob, 3));
        handles.robots(i_rob).setGoal(scenario.initial_stage(i_rob, 5), scenario.initial_stage(i_rob, 6));  
    end
    handles.rob_id = numel(handles.robots) + 1;
    
    % Set types
    for i_type = 1:size(scenario.robot_types, 1)
       type_name = scenario.robot_types{i_type, 1};
       robs_of_type = scenario.robot_types{i_type, 2};
       for i_rob = 1:numel(robs_of_type)
           handles.robots(robs_of_type(i_rob)).setType(type_name);
       end
    end
    
    % Adapt color scheme to number of robots
    num_colors = max(20, num_rob);
    handles.colors = hsv(num_colors);
    handles.colors = handles.colors(randperm(num_colors), :);
    
    % Plot
    for i_rob = 1:num_rob
        handles = addRobotPlot(handles, handles.robots(i_rob));
    end
    
    resetView(hObject, handles)
    
    guidata(hObject, handles);
    
% --------------------------------------------------------------------
function uipushtool_save_scenario_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool_save_scenario (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    if numel(handles.robots) < 1
        warning('You have not created any robots');
        return;
    end
    
    default_name = [Config.getInstance().dir_scenarios filesep 'untitled.m'];
    
    [file_name, path_name] = uiputfile({'*.m'}, 'Save Scenario', default_name);
    
    if file_name == 0
        % User clicked 'Cancel'
        return;
    end
    
    map_names = cellstr(get(handles.popup_map_selection, 'String'));
    value = get(handles.popup_map_selection, 'Value');
    map_name = map_names{value};
    if value == 1
        map_name = '';
    end
    saveStage(path_name, file_name, handles.robots, map_name );

%% Util: Robot selection

function robot_selection_Callback(hObject, eventdata)
% hObject    handle to object that triggered this function (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB

    handles = guidata(gcbo);
    
    % Reset goal marker of formerly selected robot to normal size
    handles.sel_idx
    if ~isempty(handles.sel_idx)
        set(handles.rob_plots{handles.sel_idx}.goal, 'MarkerSize', 12);
    end
   
    is_found = false;
    %TODO Is there a better way?
    
    for i_rob = 1:numel(handles.rob_plots)
        fields = fieldnames(handles.rob_plots{i_rob});
        for i_h = 1:numel(fields)
            if handles.rob_plots{i_rob}.(fields{i_h}) == hObject
                handles.sel_idx = i_rob;
                
                displayCurrentRobotData(hObject, handles, handles.robots(i_rob)); 
                is_found = true;
                break;
            end     
        end 
        if is_found
            break;
        end
    end

function enableCurrentRobotPanel(cmd)

    handles = guidata(gcbo);
    set(findall(handles.panel_current_rob, '-property', 'enable'), 'enable', cmd);    
    if strcmp(cmd, 'off')
        set(handles.edit_position_x, 'String', '');
        set(handles.edit_position_y, 'String', '');
        set(handles.edit_orientation, 'String', '');
        set(handles.edit_goal_x, 'String', '');
        set(handles.edit_goal_y, 'String', '');
        set(handles.panel_current_rob, 'HighlightColor', 'w');
        set(handles.panel_current_rob, 'ForegroundColor', 'k');
    end

function displayCurrentRobotData(hObject, handles, rob)
    
    % Show position, orientation and goal
    set(handles.edit_position_x, 'String', num2str(rob.x_gt));
    set(handles.edit_position_y, 'String', num2str(rob.y_gt));
    set(handles.edit_orientation, 'String', num2str(rob.phi_gt));
    set(handles.edit_goal_x, 'String', num2str(rob.x_goal));
    set(handles.edit_goal_y, 'String', num2str(rob.y_goal));
    
    % Show type
    type_list = get(handles.popupmenu_robot_type,'String'); 
    for i_type = 1:numel(type_list)
        if strcmp(type_list{i_type}, rob.type.name)
            set(handles.popupmenu_robot_type, 'Value', i_type);
            break;
        end
    end
    
    % Make the current panel show the color of the robot
    color = getColor(handles, rob.id);
    set(handles.panel_current_rob, 'HighlightColor', color);
    set(handles.panel_current_rob, 'ForegroundColor', color);
    
    % Make the goal bigger
    set(handles.rob_plots{handles.sel_idx}.goal, 'MarkerSize', 30);
    
    enableCurrentRobotPanel('on');
    
    guidata(hObject, handles);
    
%% Util cleanUp, reset
   
function handles = cleanUp(hObject, handles)

    enableCurrentRobotPanel('off');
    
    set(handles.popup_map_selection, 'Value', 1);
    set(handles.popupmenu_robot_type, 'Value', 1);
    
    cla(handles.axes_stage, 'reset');
    hold(handles.axes_stage, 'on');
    handles.robots = [];
    handles.rob_plots = {};
    handles.sel_idx = [];
    handles.map_lines = [];
    handles.map_rect = [-2 -2 4 4];
    handles.rob_id = 1;
    
    resetView(hObject, handles);
    
function resetView(hObject, handles)

plot_margin = Config.getInstance().plot_margin;

% Adapt view such that map is completely visible
    x_min = handles.map_rect(1) - plot_margin; 
    y_min = handles.map_rect(2) - plot_margin; 
    x_max = handles.map_rect(1) + handles.map_rect(3) + plot_margin; 
    y_max = handles.map_rect(2) + handles.map_rect(4) + plot_margin;

% Adapt view so that previously placed robots remain visible
if numel(handles.robots) > 0
    for i_rob = 1:numel(handles.robots);
        x_min = min(x_min, min(handles.robots(i_rob).corners(:,1)) - plot_margin);
        x_max = max(x_max, max(handles.robots(i_rob).corners(:,1)) + plot_margin);
        y_min = min(y_min, min(handles.robots(i_rob).corners(:,2)) - plot_margin);
        y_max = max(x_max, max(handles.robots(i_rob).corners(:,2)) + plot_margin);
    end
end
% Make view square
x_max = max(x_max, y_max); y_max = x_max;
x_min = min(x_min, y_min); y_min = x_min;

set(handles.axes_stage, 'Xlim', [x_min x_max]);
set(handles.axes_stage, 'YLim', [y_min y_max]);
zoom reset;

guidata(hObject, handles);

function color = getColor(handles, idx)
    while idx > size(handles.colors, 1)
        idx = idx - size(handles.colors, 1);
    end
    color = handles.colors(idx, :);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);
menuMain();