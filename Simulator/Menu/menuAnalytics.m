function menuAnalytics( )
%MENUANALYTICS Summary of this function goes here
%   Detailed explanation goes here

conf = Config.getInstance();

alg_name = '';
scenario_name = '';

% Get available algorithm folders
subdirs = dir(conf.dir_results); 
isub = [subdirs(:).isdir]; %# returns logical vector
alg_names = {subdirs(isub).name}';
alg_names(ismember(alg_names,{'.','..'})) = [];

while isempty(scenario_name)

    [selection, ok] = listdlg('ListString', alg_names, ...
                              'SelectionMode', 'single', ...
                              'Name', 'MiMicS - Analyze results', ...
                              'PromptString', 'Select an algorithm:');             
    if ~ok
        menuMain();
        return;
    end
    
    alg_name = alg_names{selection};

    path_to_log = [conf.dir_results filesep alg_name];

    % Scenario selection
    subdirs = dir(path_to_log); 
    isub = [subdirs(:).isdir]; %# returns logical vector
    scenario_names = {subdirs(isub).name}';
    scenario_names(ismember(scenario_names,{'.','..'})) = [];

    [selection, ok] = listdlg('ListString', scenario_names, ...
                              'SelectionMode', 'single', ...
                              'Name', 'MiMicS - Analyze results', ...
                              'PromptString', 'Select a scenario:');
    if ~ok
        continue;
    end

    scenario_name = scenario_names{selection};
    path_to_log = [path_to_log filesep scenario_name];
end

log = readLog(path_to_log);
fprintf('Starting simReplayer.\n');
simReplayer(log);

                    
end

