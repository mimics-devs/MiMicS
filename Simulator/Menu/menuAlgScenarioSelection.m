function varargout = menuAlgScenarioSelection(varargin)
% MENUALGSCENARIOSELECTION MATLAB code for menuAlgScenarioSelection.fig
%      MENUALGSCENARIOSELECTION, by itself, creates a new MENUALGSCENARIOSELECTION or raises the existing
%      singleton*.
%
%      H = MENUALGSCENARIOSELECTION returns the handle to a new MENUALGSCENARIOSELECTION or the handle to
%      the existing singleton*.
%
%      MENUALGSCENARIOSELECTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MENUALGSCENARIOSELECTION.M with the given input arguments.
%
%      MENUALGSCENARIOSELECTION('Property','Value',...) creates a new MENUALGSCENARIOSELECTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before menuAlgScenarioSelection_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to menuAlgScenarioSelection_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help menuAlgScenarioSelection

% Last Modified by GUIDE v2.5 11-Jan-2017 14:04:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @menuAlgScenarioSelection_OpeningFcn, ...
                   'gui_OutputFcn',  @menuAlgScenarioSelection_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before menuAlgScenarioSelection is made visible.
function menuAlgScenarioSelection_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to menuAlgScenarioSelection (see VARARGIN)

% Choose default command line output for menuAlgScenarioSelection
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

conf = Config.getInstance();

% Add algorithms
algorithms_subdirs = dir(conf.dir_algorithms); 
isub = [algorithms_subdirs(:).isdir]; %# returns logical vector
algorithm_names = {algorithms_subdirs(isub).name}';
algorithm_names(ismember(algorithm_names,{'.','..'})) = [];
data_algtable = cell(numel(algorithm_names), 2);
for i = 1:numel(algorithm_names)
    data_algtable(i,1) = algorithm_names(i);
    data_algtable{i,2} = false;
end
table_algorithms = findobj('Tag', 'table_algorithms');
set(table_algorithms,'data',data_algtable);

% Add scenarios
scenario_list = dir([conf.dir_scenarios filesep '*.m']);
data_scenario_table = cell(numel(scenario_list), 2);
for i = 1:numel(scenario_list)
    [~, scenario_name, ~] = fileparts(scenario_list(i).name);
    data_scenario_table(i,1) = {scenario_name};
    data_scenario_table{i,2} = false;
end
set(handles.table_scenarios,'data', data_scenario_table);

% UIWAIT makes menuAlgScenarioSelection wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = menuAlgScenarioSelection_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton_run.
function pushbutton_run_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data_algorithms = get(handles.table_algorithms, 'data');
data_scenarios = get(handles.table_scenarios, 'data');

list_of_algorithms = {};
for i = 1:size(data_algorithms,1)
    if data_algorithms{i,2} 
        list_of_algorithms = [list_of_algorithms, data_algorithms(i,1)];
    end
end

list_of_scenarios = {};
for i = 1:size(data_scenarios,1)
    if data_scenarios{i,2} 
        list_of_scenarios = [list_of_scenarios, data_scenarios(i,1)];
    end
end

simulator = Simulator.getInstance();
simulator.performBatteryOfSimulations(list_of_algorithms, list_of_scenarios);

% --- Executes on button press in pushbutton_alg_select_all.
function pushbutton_alg_select_all_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_alg_select_all (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data = get(handles.table_algorithms, 'data');
for i = 1:size(data,1)
    data{i,2} = true;
end
set(handles.table_algorithms, 'data', data);

% --- Executes on button press in pushbutton_alg_select_none.
function pushbutton_alg_select_none_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_alg_select_none (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data = get(handles.table_algorithms, 'data');
for i = 1:size(data,1)
    data{i,2} = false;
end
set(handles.table_algorithms, 'data', data);

% --- Executes on button press in pushbutton_scenario_select_all.
function pushbutton_scenario_select_all_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_scenario_select_all (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data = get(handles.table_scenarios, 'data');
for i = 1:size(data,1)
    data{i,2} = true;
end
set(handles.table_scenarios, 'data', data);

% --- Executes on button press in pushbutton_scenario_select_none.
function pushbutton_scenario_select_none_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_scenario_select_none (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data = get(handles.table_scenarios, 'data');
for i = 1:size(data,1)
    data{i,2} = false;
end
set(handles.table_scenarios, 'data', data);

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);

menuMain();
