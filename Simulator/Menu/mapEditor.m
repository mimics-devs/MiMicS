function varargout = mapEditor(varargin)
% MAPEDITOR MATLAB code for mapEditor.fig
%      MAPEDITOR, by itself, creates a new MAPEDITOR or raises the existing
%      singleton*.
%
%      H = MAPEDITOR returns the handle to a new MAPEDITOR or the handle to
%      the existing singleton*.
%
%      MAPEDITOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAPEDITOR.M with the given input arguments.
%
%      MAPEDITOR('Property','Value',...) creates a new MAPEDITOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mapEditor_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mapEditor_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mapEditor

% Last Modified by GUIDE v2.5 31-May-2017 16:48:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mapEditor_OpeningFcn, ...
                   'gui_OutputFcn',  @mapEditor_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mapEditor is made visible.
function mapEditor_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mapEditor (see VARARGIN)

% Choose default command line output for mapEditor
handles.output = hObject;

handles.h_lines = [];
handles.sel_idx = 0;
handles.default_xlim = [-10 10];
handles.default_ylim = [-10 10];
%handles.default_grid_width = 1;
%handles.grid_width = handles.default_grid_width;
handles.snapping = true;

set(handles.axes_map, 'Xlim', handles.default_xlim);
set(handles.axes_map, 'Ylim', handles.default_ylim);
%set(handles.axes_map, 'xtick', [handles.default_xlim(1):handles.default_grid_width:handles.default_xlim(2)]);
%set(handles.axes_map, 'ytick', [handles.default_ylim(1):handles.default_grid_width:handles.default_ylim(2)]);
set(handles.edit_defview_xmin, 'String', num2str(handles.default_xlim(1)));
set(handles.edit_defview_xmax, 'String', num2str(handles.default_xlim(2)));
set(handles.edit_defview_ymin, 'String', num2str(handles.default_ylim(1)));
set(handles.edit_defview_ymax, 'String', num2str(handles.default_ylim(2)));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes mapEditor wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = mapEditor_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);
menuMain();

% --- Executes on button press in checkbox_snap.
function checkbox_snap_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_snap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_snap

handles.snapping = get(hObject,'Value');
guidata(hObject, handles);


%% Default view

function edit_defview_xmin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_defview_xmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_defview_xmin as text
%        str2double(get(hObject,'String')) returns contents of edit_defview_xmin as a double

val = str2double(get(hObject,'String'));
curr_lim = get(handles.axes_map, 'Xlim');
if isnan(val)
    warndlg('Input must be numerical');
    set(hObject, 'String', num2str(curr_lim(1)));
    return;
end
set(handles.axes_map, 'Xlim', [val curr_lim(2)]);

% --- Executes during object creation, after setting all properties.
function edit_defview_xmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_defview_xmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_defview_xmax_Callback(hObject, eventdata, handles)
% hObject    handle to edit_defview_xmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_defview_xmax as text
%        str2double(get(hObject,'String')) returns contents of edit_defview_xmax as a double

val = str2double(get(hObject,'String'));
curr_lim = get(handles.axes_map, 'Xlim');
if isnan(val)
    warndlg('Input must be numerical');
    set(hObject, 'String', num2str(curr_lim(2)));
    return;
end
set(handles.axes_map, 'Xlim', [curr_lim(1) val]);


% --- Executes during object creation, after setting all properties.
function edit_defview_xmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_defview_xmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_defview_ymin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_defview_ymin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_defview_ymin as text
%        str2double(get(hObject,'String')) returns contents of edit_defview_ymin as a double

val = str2double(get(hObject,'String'));
curr_lim = get(handles.axes_map, 'Ylim');
if isnan(val)
    warndlg('Input must be numerical');
    set(hObject, 'String', num2str(curr_lim(1)));
    return;
end
set(handles.axes_map, 'Ylim', [val curr_lim(2)]);

% --- Executes during object creation, after setting all properties.
function edit_defview_ymin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_defview_ymin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_defview_ymax_Callback(hObject, eventdata, handles)
% hObject    handle to edit_defview_ymax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_defview_ymax as text
%        str2double(get(hObject,'String')) returns contents of edit_defview_ymax as a double

val = str2double(get(hObject,'String'));
curr_lim = get(handles.axes_map, 'Ylim');
if isnan(val)
    warndlg('Input must be numerical');
    set(hObject, 'String', num2str(curr_lim(2)));
    return;
end
set(handles.axes_map, 'Ylim', [curr_lim(1) val]);

% --- Executes during object creation, after setting all properties.
function edit_defview_ymax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_defview_ymax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% Line add, prev, next

% --- Executes on button press in pushbutton_add_line.
function pushbutton_add_line_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_add_line (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.pushbutton_add_line, 'Enable', 'off');
set(handles.pushbutton_prev_line, 'Enable', 'off');
set(handles.pushbutton_next_line, 'Enable', 'off');
deselectCurrentLine(hObject, handles);
set(handles.text_help, 'String', 'Click 2 times within the axes to select starting and end point of the line.');

% Get coordinates
[x, y] =  ginputax(handles.axes_map, 2);
if (handles.snapping)
   x_ticks = [handles.axes_map.XTick, handles.axes_map.XRuler.MinorTick];
   x = interp1(x_ticks, x_ticks, x, 'nearest');
   y_ticks = [handles.axes_map.YTick, handles.axes_map.YRuler.MinorTick];
   y = interp1(y_ticks, y_ticks, y, 'nearest');
end

handles = addLine(hObject, handles, [x(1) x(2) y(1) y(2)]);

%resetView(hObject, handles);
displayCurrentLine(hObject, handles);
set(handles.pushbutton_add_line, 'Enable', 'on');
set(handles.pushbutton_prev_line, 'Enable', 'on');
set(handles.pushbutton_next_line, 'Enable', 'on');
set(handles.text_help, 'String', ['Added line number ' num2str(handles.sel_idx)]);

% --- Executes on button press in pushbutton_prev_line.
function pushbutton_prev_line_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_prev_line (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

select_prev_line(hObject, handles);


% --- Executes on button press in pushbutton_next_line.
function pushbutton_next_line_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_next_line (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.sel_idx == 0
    if numel(handles.h_lines) > 0
        handles.sel_idx = numel(handles.h_lines);
    end
else
    deselectCurrentLine(hObject, handles);
    handles.sel_idx = handles.sel_idx + 1;
    if handles.sel_idx > numel(handles.h_lines); 
        handles.sel_idx = 1;
    end
end
displayCurrentLine(hObject, handles);

%% Coordinate setters - Pushbuttons

% --- Executes on button press in pushbutton_l_start.
function pushbutton_l_start_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_l_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.pushbutton_add_line, 'Enable', 'off');
set(handles.pushbutton_prev_line, 'Enable', 'off');
set(handles.pushbutton_next_line, 'Enable', 'off');
set(handles.text_help, 'String', 'Click within the axes to select a new starting point.');

% Get old data
xdata = get(handles.h_lines(handles.sel_idx), 'XData');
ydata = get(handles.h_lines(handles.sel_idx), 'YData');

% Get coordinates
[x, y] =  ginputax(handles.axes_map, 1);
if (handles.snapping)
   x_ticks = [handles.axes_map.XTick, handles.axes_map.XRuler.MinorTick];
   x = interp1(x_ticks, x_ticks, x, 'nearest');
   y_ticks = [handles.axes_map.YTick, handles.axes_map.YRuler.MinorTick];
   y = interp1(y_ticks, y_ticks, y, 'nearest');
end

% Set them
set(handles.h_lines(handles.sel_idx), 'XData', [x xdata(2)]);
set(handles.h_lines(handles.sel_idx), 'YData', [y ydata(2)]);

displayCurrentLine(hObject, handles);
set(handles.pushbutton_add_line, 'Enable', 'on');
set(handles.pushbutton_prev_line, 'Enable', 'on');
set(handles.pushbutton_next_line, 'Enable', 'on');
set(handles.text_help, 'String', ['Changed line start to ' num2str(x) '|' num2str(y)]);

% --- Executes on button press in pushbutton_l_end.
function pushbutton_l_end_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_l_end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.pushbutton_add_line, 'Enable', 'off');
set(handles.pushbutton_prev_line, 'Enable', 'off');
set(handles.pushbutton_next_line, 'Enable', 'off');
set(handles.text_help, 'String', 'Click within the axes to select a new ending point.');

% Get old data
xdata = get(handles.h_lines(handles.sel_idx), 'XData');
ydata = get(handles.h_lines(handles.sel_idx), 'YData');

% Get coordinates
[x, y] =  ginputax(handles.axes_map, 1);
if (handles.snapping)
   x_ticks = [handles.axes_map.XTick, handles.axes_map.XRuler.MinorTick];
   x = interp1(x_ticks, x_ticks, x, 'nearest');
   y_ticks = [handles.axes_map.YTick, handles.axes_map.YRuler.MinorTick];
   y = interp1(y_ticks, y_ticks, y, 'nearest');
end

% Set them
set(handles.h_lines(handles.sel_idx), 'XData', [xdata(1) x]);
set(handles.h_lines(handles.sel_idx), 'YData', [ydata(1) y]);

displayCurrentLine(hObject, handles);
set(handles.pushbutton_add_line, 'Enable', 'on');
set(handles.pushbutton_prev_line, 'Enable', 'on');
set(handles.pushbutton_next_line, 'Enable', 'on');
set(handles.text_help, 'String', ['Changed line end to ' num2str(x) '|' num2str(y)]);

%% Coordinate setters - Edit boxes

function edit_l_start_x_Callback(hObject, eventdata, handles)
% hObject    handle to edit_l_start_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_l_start_x as text
%        str2double(get(hObject,'String')) returns contents of edit_l_start_x as a double

if (handles.sel_idx == 0)
    return;
end
val = str2double(get(hObject,'String'));
curr_data = get(handles.h_lines(handles.sel_idx), 'XData');
if isnan(val)
    warndlg('Input must be numerical');
    set(hObject, 'String', num2str(curr_data(1)));
    val = curr_data(1);
end
set(handles.h_lines(handles.sel_idx), 'XData', [val curr_data(2)]);
set(handles.text_help, 'String', ['Set starting X coordinate to ' num2str(val)]);


% --- Executes during object creation, after setting all properties.
function edit_l_start_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_l_start_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_l_start_y_Callback(hObject, eventdata, handles)
% hObject    handle to edit_l_start_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_l_start_y as text
%        str2double(get(hObject,'String')) returns contents of edit_l_start_y as a double

if (handles.sel_idx == 0)
    return;
end
val = str2double(get(hObject,'String'));
curr_data = get(handles.h_lines(handles.sel_idx), 'YData');
if isnan(val)
    warndlg('Input must be numerical');
    set(hObject, 'String', num2str(curr_data(1)));
    val = curr_data(1);
end
set(handles.h_lines(handles.sel_idx), 'YData', [val curr_data(2)]);
set(handles.text_help, 'String', ['Set starting Y coordinate to ' num2str(val)]);


% --- Executes during object creation, after setting all properties.
function edit_l_start_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_l_start_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_l_end_x_Callback(hObject, eventdata, handles)
% hObject    handle to edit_l_end_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_l_end_x as text
%        str2double(get(hObject,'String')) returns contents of edit_l_end_x as a double

if (handles.sel_idx == 0)
    return;
end
val = str2double(get(hObject,'String'));
curr_data = get(handles.h_lines(handles.sel_idx), 'XData');
if isnan(val)
    warndlg('Input must be numerical');
    set(hObject, 'String', num2str(curr_data(2)));
    val = curr_data(2);
end
set(handles.h_lines(handles.sel_idx), 'XData', [curr_data(1) val]);
set(handles.text_help, 'String', ['Set ending X coordinate to ' num2str(val)]);


% --- Executes during object creation, after setting all properties.
function edit_l_end_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_l_end_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_l_end_y_Callback(hObject, eventdata, handles)
% hObject    handle to edit_l_end_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_l_end_y as text
%        str2double(get(hObject,'String')) returns contents of edit_l_end_y as a double

if (handles.sel_idx == 0)
    return;
end
val = str2double(get(hObject,'String'));
curr_data = get(handles.h_lines(handles.sel_idx), 'YData');
if isnan(val)
    warndlg('Input must be numerical');
    set(hObject, 'String', num2str(curr_data(2)));
    val = curr_data(2);
end
set(handles.h_lines(handles.sel_idx), 'YData', [curr_data(1) val]);
set(handles.text_help, 'String', ['Set ending Y coordinate to ' num2str(val)]);

% --- Executes during object creation, after setting all properties.
function edit_l_end_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_l_end_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_delete_line.
function pushbutton_delete_line_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_delete_line (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.sel_idx == 0
    warning('No line selected.');
    return;
end

old_sel_idx = handles.sel_idx;
deselectCurrentLine(hObject, handles);
delete(handles.h_lines(handles.sel_idx));
handles.h_lines(handles.sel_idx) = [];
handles.sel_idx = old_sel_idx -1;
if (handles.sel_idx <= 0)
    if numel(handles.h_lines) > 0
        handles.sel_idx = numel(handles.h_lines);
    else
        handles.sel_idx = 0;
    end
end
displayCurrentLine(hObject, handles);
set(handles.text_help, 'String', ['Deleted line ' num2str(old_sel_idx)]);

guidata(hObject, handles);

%% Util functions

function enableCurrentLinePanel(cmd)

    handles = guidata(gcbo);
    set(findall(handles.uipanel_curr_line, '-property', 'enable'), 'enable', cmd);    
    if strcmp(cmd, 'off')
        set(handles.edit_l_start_x, 'String', '');
        set(handles.edit_l_start_y, 'String', '');
        set(handles.edit_l_end_x, 'String', '');
        set(handles.edit_l_end_y, 'String', '');
    end
    
function handles = addLine(hObject, handles, coord)

% Draw line and add to known lines
new_line = line([coord(1) coord(2)], [coord(3) coord(4)]);
handles.h_lines = [handles.h_lines; new_line];
handles.sel_idx = numel(handles.h_lines);
set(new_line, 'ButtonDownFcn', @cb_line_selection);

guidata(hObject, handles);

function displayCurrentLine(hObject, handles)
    if handles.sel_idx <= 0
        return;
    end
    
    curr_line = handles.h_lines(handles.sel_idx);
    xdata = get(curr_line, 'XData');
    ydata = get(curr_line, 'YData');
    
    % Display data in panel
    set(handles.edit_l_start_x, 'String', num2str(xdata(1)));
    set(handles.edit_l_start_y, 'String', num2str(ydata(1)));
    set(handles.edit_l_end_x, 'String', num2str(xdata(2)));
    set(handles.edit_l_end_y, 'String', num2str(ydata(2)));
    
    % Highlight line itself
    set(curr_line, 'Marker', 'o');
    
    enableCurrentLinePanel('on');
    set(handles.text_help, 'String', ['Selected line ' num2str(handles.sel_idx)]);
    
    guidata(hObject, handles);
    
function deselectCurrentLine(hObject, handles)
    if (handles.sel_idx ~= 0)
        curr_line = handles.h_lines(handles.sel_idx);
        set(curr_line, 'Marker', 'none');
        handles.sel_idx = 0;
    end
    enableCurrentLinePanel('off');
    guidata(hObject, handles);

    
function cb_line_selection(hObject, eventdata)
% hObject    handle to object that triggered this function (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB

    handles = guidata(gcbo);   
    deselectCurrentLine(hObject, handles);
    for i = 1:numel(handles.h_lines)
       if handles.h_lines(i) == hObject
           handles.sel_idx = i;
           break;
       end
    end 
    displayCurrentLine(hObject, handles);
    
function select_prev_line(hObject, handles)

if handles.sel_idx <= 0
    if numel(handles.h_lines) > 0
        handles.sel_idx = 1;
    end
else
    deselectCurrentLine(hObject, handles);
    handles.sel_idx = handles.sel_idx - 1;
    if handles.sel_idx <= 0 
        handles.sel_idx = numel(handles.h_lines);
    end
end

displayCurrentLine(hObject, handles);

guidata(hObject, handles);

function handles = cleanUp(hObject, handles)
% Deletes all lines, resets view.

enableCurrentLinePanel('off');
handles.sel_idx = 0;
for i = 1:numel(handles.h_lines)
    delete(handles.h_lines);
end
handles.h_lines = [];
set(handles.text_help, 'String', 'Deleted all lines');

set(handles.axes_map, 'Xlim', handles.default_xlim);
set(handles.axes_map, 'Ylim', handles.default_ylim);
set(handles.edit_defview_xmin, 'String', num2str(handles.default_xlim(1)));
set(handles.edit_defview_xmax, 'String', num2str(handles.default_xlim(2)));
set(handles.edit_defview_ymin, 'String', num2str(handles.default_ylim(1)));
set(handles.edit_defview_ymax, 'String', num2str(handles.default_ylim(2)));

guidata(hObject, handles);

%% Toolbar new, load, save

% --------------------------------------------------------------------
function uipushtool_new_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool_new (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

button = questdlg('Are you sure?', ...
        'New map', ...
        'Reset', 'Cancel' , 'Cancel');
    if strcmp(button, 'Cancel')
        return
    end

handles = cleanUp(hObject, handles);
guidata(hObject, handles);

% --------------------------------------------------------------------
function uipushtool_save_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if numel(handles.h_lines) < 1
    warndlg('There are no map lines to save.');
    return;
end
    
default_name = [Config.getInstance().dir_maps filesep 'untitled.m'];

[file_name, path_name] = uiputfile({'*.m'}, 'Save map', default_name);
    
if file_name == 0
    % User clicked 'Cancel'
    return;
end
map_lines = zeros(numel(handles.h_lines), 4);
for i = 1:numel(handles.h_lines)
   map_lines(i, 1:2) = get(handles.h_lines(i), 'XData');
   map_lines(i, 3:4) = get(handles.h_lines(i), 'YData');
end
rslt = saveMap(path_name, file_name, map_lines);

if rslt
    set(handles.text_help, 'String', ['Successfully saved map in ' path_name file_name]);
end


% --------------------------------------------------------------------
function uipushtool_open_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool_open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

default_name = [Config.getInstance().dir_maps filesep '*.m'];
[file_name, path_name, ~] = uigetfile({'*.m'}, 'Open scenario', default_name );
    
% TODO: What if user selects outside of the current maps folder?
    
if strcmp(file_name, '')    % User pressed cancel
    return;
end

% Delete previously drawn lines
handles = cleanUp(hObject, handles);

% Load new map
[~, bare_name, ~] = fileparts(file_name);
mm = MapManager.getInstance();
mm.clear();
map = mm.getOrCreateMapByName(bare_name);

% Draw it
num_lines = size(map.lines, 1);
for i = 1:num_lines
    coord = [map.lines(i,1) map.lines(i,2) map.lines(i,3) map.lines(i,4)];
    handles = addLine(hObject, handles, coord);
end

% Select last drawn line
handles.sel_idx = numel(handles.h_lines);
displayCurrentLine(hObject, handles);

% Change view
plot_margin = Config.getInstance().plot_margin;
x_min = min([map.lines(:,1); map.lines(:,2)]) - plot_margin;
x_max = max([map.lines(:,1); map.lines(:,2)]) + plot_margin;
y_min = min([map.lines(:,3); map.lines(:,4)]) - plot_margin;
y_max = max([map.lines(:,3); map.lines(:,4)]) + plot_margin;
% Make square
x_max = max(x_max, y_max); y_max = x_max;
x_min = min(x_min, y_min); y_min = x_min;  
% Change view
set(handles.axes_map, 'Xlim', [x_min x_max]);
set(handles.axes_map, 'Ylim', [y_min y_max]);
zoom reset;

set(handles.edit_defview_xmin, 'String', num2str(x_min));
set(handles.edit_defview_xmax, 'String', num2str(x_max));
set(handles.edit_defview_ymin, 'String', num2str(y_min));
set(handles.edit_defview_ymax, 'String', num2str(y_max));


set(handles.text_help, 'String', ['Openend map ' file_name ' with ' num2str(num_lines) ' lines.']);

guidata(hObject, handles);
