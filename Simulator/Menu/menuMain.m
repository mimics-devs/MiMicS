function menuMain()

    % Preparing the first menu
    str_introduction = {'Welcome to MiMicS,', ...
                        'a Matlab Based Multi-robot Simulator'};

    choices = { 'Perform simulations',  @menuAlgScenarioSelection;                   
                'Scenario editor',      @stageEditor;    
                'Map editor',           @mapEditor;
                'Analyze results',      @menuAnalytics;
                'Configure',            @menuConfig;
                'About',                @about ...
                };
    
    simpleMenuDisplay(str_introduction, choices, @goodbye);          

end

function goodbye()
    disp('Goodbye!');
end