function varargout = simReplayer(varargin)
% SIMREPLAYER MATLAB code for simReplayer.fig
%      SIMREPLAYER, by itself, creates a new SIMREPLAYER or raises the existing
%      singleton*.
%
%      H = SIMREPLAYER returns the handle to a new SIMREPLAYER or the handle to
%      the existing singleton*.
%
%      SIMREPLAYER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SIMREPLAYER.M with the given input arguments.
%
%      SIMREPLAYER('Property','Value',...) creates a new SIMREPLAYER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before simReplayer_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to simReplayer_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help simReplayer

% Last Modified by GUIDE v2.5 03-May-2017 16:23:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @simReplayer_OpeningFcn, ...
                   'gui_OutputFcn',  @simReplayer_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before simReplayer is made visible.
function simReplayer_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to simReplayer (see VARARGIN)

% Choose default command line output for simReplayer
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

cla(handles.axes_stage, 'reset');

if nargin >= 4
    handles.log = varargin{1};
else
    error('simReplayer requires a simulation log as argument.');
end
conf = Config.getInstance();
handles.curr_time = 0;
handles.pause_length = conf.delta_t;

title([handles.log.algorithm '/' handles.log.scenario]);
handles.num_robots = handles.log.number_of_robots;
str = sprintf('Creation time:\n%s\n\nUUID:\n%s\n\nTermination:\n(%i) %s\n\nNumber of robots: %i\nReached goal: %i\nCrashed: %i (static), %i (other robots)\nEscaped: %i\nOn deadlock: %i\n', ...
    handles.log.creation_time, handles.log.log_uuid, handles.log.termination_code, handles.log.termination_msg, ...
    handles.num_robots, handles.log.num_reached_goal, handles.log.num_crashed_with_static, handles.log.num_crashed_with_robot, handles.log.num_escaped, handles.log.num_on_deadlock);
set(handles.text_generalinfo, 'String', str);

% Configure time control and display
set(handles.slider_time, 'Min',0);
set(handles.slider_time, 'Max', handles.log.elapsed_simulated_time);
set(handles.slider_time, 'SliderStep', [conf.delta_t conf.delta_t]);
set(handles.text_totaltime, 'String', sprintf('/%g s', handles.log.elapsed_simulated_time));
set(handles.text_currtime, 'String', num2str(handles.curr_time));

% Create robots
handles.robots = RobotModel.empty(handles.num_robots, 0);
for i_robot = 1:handles.num_robots
    handles.robots(i_robot) = RobotModel(i_robot);
    handles.robots(i_robot).setType(handles.log.robots(i_robot).type);
    handles.robots(i_robot).setPos(handles.log.robots(i_robot).x_gt(1), handles.log.robots(i_robot).y_gt(1));
    handles.robots(i_robot).setOri(handles.log.robots(i_robot).phi_gt(1));
    
    handles.robots(i_robot).setGoal(handles.log.robots(i_robot).x_gt(end), handles.log.robots(i_robot).y_gt(end));
    
end

% Plot initial state
axes(handles.axes_stage);
hold on;

% Plot map, robots (without reference lines or velocity bars)
[handles.rob_plots, ~, ~, ~] = initPlotStage(handles.robots, handles.log.map_lines, handles.axes_stage, true);

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes simReplayer wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = simReplayer_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider_time_Callback(hObject, eventdata, handles)
% hObject    handle to slider_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles.curr_time = get(hObject,'Value');
plotSimAtTime(hObject, handles, handles.curr_time);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider_time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton_back2start.
function pushbutton_back2start_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_back2start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

new_time = get(handles.slider_time,'Min');
set(handles.slider_time,'Value', new_time);
handles.curr_time = new_time;
plotSimAtTime(hObject, handles, new_time);
guidata(hObject, handles);

% --- Executes on button press in pushbutton_skip2end.
function pushbutton_skip2end_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_skip2end (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.curr_time = get(handles.slider_time,'Max');
set(handles.slider_time,'Value', handles.curr_time);
set(handles.togglebutton_play, 'Value', 0);
plotSimAtTime(hObject, handles, handles.curr_time);
guidata(hObject, handles);


% --- Executes on button press in togglebutton_play.
function togglebutton_play_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_play (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_play

if handles.curr_time >= handles.log.elapsed_simulated_time
    set(hObject, 'Value', 0);
    return;
end

conf = Config.getInstance();
while get(hObject, 'Value') && handles.curr_time < handles.log.elapsed_simulated_time
    t_loopstart = clock;
    % Advance one delta_t forward and plot
    handles.curr_time = get(handles.slider_time ,'Value');      % getting the value here makes interrupts from slider or skip buttons easier to handle
    handles.curr_time = round((handles.curr_time + conf.delta_t)/conf.delta_t)*conf.delta_t;
    set(handles.slider_time, 'Value', handles.curr_time);
    plotSimAtTime(hObject, handles, handles.curr_time);
    drawnow;
    
    % Don't pause if this was the last plot possible
    if handles.curr_time == handles.log.elapsed_simulated_time
        set(hObject, 'Value', 0);   % deactivate button
        break;
    end
    
    % Get play speed and pause accordingly
    contents = cellstr(get(handles.popupmenu_playspeed, 'String'));
    val = get(handles.popupmenu_playspeed,'Value');
    if val == size(contents,1)      % playspeed = 'max'
        handles.pause_length = 0;
    else 
        handles.pause_length = conf.delta_t / str2double(contents{val});
    end
    
    pause(handles.pause_length - etime(clock, t_loopstart)); 
end


% --- Executes on selection change in popupmenu_playspeed.
function popupmenu_playspeed_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_playspeed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_playspeed contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_playspeed

% --- Executes during object creation, after setting all properties.
function popupmenu_playspeed_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_playspeed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_makevideo.
function pushbutton_makevideo_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_makevideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

conf = Config.getInstance();

% Disable controls
set(handles.togglebutton_play, 'Value', 0);
set(handles.togglebutton_play, 'Enable', 'off');
set(handles.pushbutton_back2start, 'Enable', 'off');
set(handles.pushbutton_skip2end, 'Enable', 'off');
set(handles.popupmenu_playspeed, 'Enable', 'off');
set(handles.pushbutton_makevideo, 'Enable', 'off');
set(handles.slider_time, 'Enable', 'off');

% Get path and file name
default_full_path = [Config.getInstance().dir_results filesep handles.log.algorithm filesep handles.log.scenario filesep handles.log.scenario '.avi'];
[file_name, path_name] = uiputfile({'*.avi'}, 'Save video as', default_full_path);

if file_name ~= 0
    fprintf('Generating video, please wait...');
    full_path = [path_name file_name];
    
    % Ask user for video quality
    ok = false;
    while ~ok
        user_input = inputdlg('Enter video quality level (1-100):', 'Export as video', 1, {'50'});
        vid_qual = fix(str2double(user_input{:}));
        if isnan(vid_qual) || vid_qual < 1 || vid_qual > 100
            warndlg('You must enter a number between 1 and 100', 'Export as video');
            continue;
        end
        ok = true;
    end

    % Create video writer
    vid_writer = VideoWriter(full_path);
    vid_writer.FrameRate = 1/conf.delta_t;
    vid_writer.Quality = vid_qual;
    open(vid_writer);
    
    % Plot as fast as possible and pack frames into video writer
    for t = 0:conf.delta_t:get(handles.slider_time, 'Max')
        plotSimAtTime(hObject, handles, t);
        frame = getframe(handles.axes_stage);
        writeVideo(vid_writer, frame);
    end
    
    close(vid_writer);
    fprintf('done.\n');
end

% Restore old plot
plotSimAtTime(hObject, handles, handles.curr_time);

% Enable controls
set(handles.togglebutton_play, 'Enable', 'on');
set(handles.pushbutton_back2start, 'Enable', 'on');
set(handles.pushbutton_skip2end, 'Enable', 'on');
set(handles.popupmenu_playspeed, 'Enable', 'on');
set(handles.pushbutton_makevideo, 'Enable', 'on');
set(handles.slider_time, 'Enable', 'on');

% Update handles structure
guidata(hObject, handles);


%% Helper functions

function plotSimAtTime(hObject, handles, new_time)

conf = Config.getInstance();
new_time = round(new_time/conf.delta_t)*conf.delta_t;   % TODO Does this work if delta_t > 1 ?

set(handles.text_currtime, 'String', sprintf('%g', new_time));

for i_robot = 1:handles.num_robots
    rob = handles.robots(i_robot);
    idx = fix(min(new_time/conf.delta_t + 1, numel(handles.log.robots(i_robot).x_gt)));
    new_pos = [handles.log.robots(i_robot).x_gt(idx), handles.log.robots(i_robot).y_gt(idx)];
    new_ori = handles.log.robots(i_robot).phi_gt(idx);
    rob.setPos(new_pos(1), new_pos(2));
    rob.setOri(new_ori);
    
    updateRobotPlot(handles.rob_plots{i_robot}, rob)
end

drawnow;

% Update handles structure
guidata(hObject, handles);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);

menuMain();
