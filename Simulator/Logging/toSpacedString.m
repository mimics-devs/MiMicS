function str = toSpacedString(value, nspaces)
% Converts value to a string that uses exactly 'nspaces' spaces, leaving the
% maximum ammount of white spaces availables in the left side.

    if isfloat(value)
        str = num2str(value);
    else
        str = value;
    end

    if isinf(nspaces)
        nspaces = length(str);
    end

    w_spaces = nspaces - length(str);

    if w_spaces >= 0
        str = [blanks(w_spaces) str];
    else
        % We have to reduce the size of the string
        % Reducing from the front
        str = str(end - nspaces + 1,end);
    end

end