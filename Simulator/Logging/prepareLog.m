function result_file = prepareLog( algorithm_name, scenario_name, robots, start_of_sim_timestamp, log_extra )
%PREPARELOG Prepares log files for a simulation.
%
%   result_file = prepareLog( sim_name, algorithm_to_use, robots, 
%       start_of_sim_timestamp, log_extra)
%
% Creates one general log file for the simulation, and a csv-formatted log
% for each robot. Every file gets filled with some header information,
% before the csv-handles are passed to the respective robots and the
% overall log-handle is returned.
%
% INPUT
% sim_name - Name of the simulation
% algorithm_to_use - Name of the algorithm used.
% robots - Array of the robots (RobotModel instances).
% start_of_sim_timestamp - Real world timestamp (created with fix(clock))
%   indicating when the simulation was started.
% log_extra - List of RobotModel attributes to log (next to the essentials, 
%   i.e. position, orientation and messages) seperated by semicolons. Can 
%   be set in the configuration.
%
% OUTPUT
% result_file - File pointer to the log.
%
% See also FINISHLOG, READLOG.


    conf = Config.getInstance();
    num_robots = numel(robots);
    
    if numel(conf.robot_log_extra) > 0
        log_extra = strsplit(conf.robot_log_extra, ';');

        % Sanity check
        for i = 1:numel(log_extra)
            if ~isfield(RobotModel.UNITS, log_extra{i})
                error('%s is either not a field of the class RobotModel or not loggable.', log_extra{i});
            end
        end
    else
        log_extra = {};
    end
    
    
    path_to_log = [conf.dir_results filesep algorithm_name filesep scenario_name];
    if isdir(path_to_log)
        rmdir(path_to_log, 's');
    end
    mkdir(path_to_log);
    
    result_file_full_path = [path_to_log filesep scenario_name '.txt'];

    % Open file for writing
    result_file = fopen(result_file_full_path, 'w');
    if result_file == -1
        error(['Error while trying to open file: ' result_file_full_path]);
    end

    % Generates a universally unique identifier 
    % (does not work with old MATLAB versions unfortunately)
    log_uuid = char(java.util.UUID.randomUUID);

    str_header = ['scenario = ' scenario_name '\n' ...
                  'algorithm = ' algorithm_name '\n' ...
                  'creation_time = ' datestr(start_of_sim_timestamp) '\n' ...
                  'log_uuid = ' log_uuid '\n' ...
                  ];   
    % Convert back: datevec(datenum(str, 'dd-mmm-yyyy HH:MM:SS'))   
    
    % Write header and number of robots into overall result file
    fprintf(result_file, str_header);
    fprintf(result_file, '\nnumber_of_robots = %d\n', num_robots);
    
    % Create and write headers to individual robot logs
    rob_log_files = zeros(num_robots,1);
    for i_robot = 1:num_robots
        % Open file
        rob_log_files(i_robot) = fopen( ...
            [path_to_log filesep 'robot_' num2str(i_robot) '.csv'], 'w');
        if rob_log_files(i_robot) == -1
            error(['Error while trying to open file: ' sim_name]);
        end
        
        fprintf(rob_log_files(i_robot), str_header);
        fprintf(rob_log_files(i_robot), 'robot_id = %i\n', robots(i_robot).id);
        fprintf(rob_log_files(i_robot), 'type = %s\n', robots(i_robot).type.name);
        fprintf(rob_log_files(i_robot), 'vehicle_width = %g\n', robots(i_robot).type.vehicle_width);
        fprintf(rob_log_files(i_robot), 'vehicle_length = %g\n\n', robots(i_robot).type.vehicle_length);
        
        
        robots(i_robot).setLog(rob_log_files(i_robot), log_extra);
        
        table_header = cell(2, 6+numel(log_extra));  % Names, units, format, width
        table_header(1,1:4) = {'time', 'x_gt', 'y_gt',   'phi_gt'};
        table_header(2,1:4) = {'[s]',    ['[' RobotModel.UNITS.x_gt ']'],  ['[' RobotModel.UNITS.y_gt ']'], ['[' RobotModel.UNITS.phi_gt ']']};
        
        for i_prop = 1:numel(log_extra)
            table_header{1,4+i_prop} = log_extra{i_prop};
            table_header{2,4+i_prop} = ['[' RobotModel.UNITS.(log_extra{i_prop}) ']'];
        end
        
        table_header(1,end-1) = {'user_msg'};
        table_header(2,end-1) = {'[]'};
        
        table_header(1,end) = {'sys_msg'};
        table_header(2,end) = {'[]'};
        
        % Print table
        for i = 1:size(table_header,1)
            for j = 1:size(table_header,2)
                fprintf(rob_log_files(i_robot), table_header{i,j});
                if j < size(table_header,2)
                    fprintf(rob_log_files(i_robot), ';');
                end
            end
            if i < size(table_header,1)
                fprintf(rob_log_files(i_robot), '\n');
            end
        end
    end

end

