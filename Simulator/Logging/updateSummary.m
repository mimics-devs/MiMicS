function updateSummary(algorithm_name, scenario_name, date, ...
    num_robots, num_reached_goal, num_crashed_with_robot, num_crashed_with_static, ...
    num_escaped, num_on_deadlock)
%UPDATESUMMARY Checks if the summary.txt file exists under the path 'route'. 
%If yes it will be updated, if not a new one is created.
%
%   updateSummary(algorithm_name, scenario_name, date, num_robots, 
%       num_reached_goal, num_crashed_with_robot, num_crashed_with_static, 
%       num_escaped, num_on_deadlock)
%
% INPUT
% algorithm_name - Name of the algorithm
% scenario_name - Name of the scenario
% date - Time when the simulation was performed [YYYY MM DD hh mm ss]
% num_robots - Number of robots in the scenario
% num_reached_goal - Number of robots which has reached its goal during the
% simulation.
% num_crashed_with_robot - Number of robots which crashed with other
% robots.
% num_crashed_with_staic - Number of robots which crashed against static
% obstacles (such as map walls).
% num_escaped - Number of robots which escaped (drove too far away from the
% stage center)
% num_deadlock - Number of robots who ended up in a deadlock situation
% (active but not able to move)

    conf = Config.getInstance();
    
    summary_file_name = [conf.dir_results filesep 'summary.txt'];

    is_new_summary = ~exist(summary_file_name, 'file'); 
    % Prepare summary file.
    if (is_new_summary)
        summary_file = fopen(summary_file_name, 'w');
        fprintf(summary_file, 'Summary file\n');
        fprintf(summary_file, '-- END OF THE HEADER --\n');
        % The names are at the end because a possible unexpected size
        fprintf(summary_file, 'YYYY MM DD hh mm ss robots reached_goal crashed_with_robot crashed_with_static escaped on_deadlock      Algorithm_Used Name_of_the_simulated_stage\n');
        fprintf(summary_file, ' [y][m][d][h][m][s]    [#]          [#]                [#]                 [#]     [#]         [#]                 [#]                      [name]\n');
        fprintf(summary_file, '  %%d %%d %%d %%d %%d %%d     %%d           %%d                 %%d                  %%d      %%d          %%d                  %%s                         %%s\n');
        fclose(summary_file);
    end

    % Open the file
    summary_file = fopen(summary_file_name, 'r+');

    % Going to the botton of the file 
    while ~feof(summary_file)
        fgetl(summary_file);
    end

    if feof(summary_file)
        % We are introducing a new line
        fprintf(summary_file,'%4d %2d %2d %2d %2d %2d', date(1), date(2), date(3), date(4), date(5), date(6));    % Date
        %format_string = '%4d %2d %2d %2d %2d %2d %1d %9d %12d %18d %20s %23s\n'; %27
        fprintf(summary_file, ' %s', toSpacedString(num_robots,             length('robots')));
        fprintf(summary_file, ' %s', toSpacedString(num_reached_goal,       length('reached_goal')));
        fprintf(summary_file, ' %s', toSpacedString(num_crashed_with_robot, length('crashed_with_robot')));
        fprintf(summary_file, ' %s', toSpacedString(num_crashed_with_static,length('crashed_with_static')));
        fprintf(summary_file, ' %s', toSpacedString(num_escaped,            length('escaped')));
        fprintf(summary_file, ' %s', toSpacedString(num_on_deadlock,        length('on_deadlock')));
        fprintf(summary_file, ' %s', toSpacedString(algorithm_name,         length('     Algorithm_Used')));
        fprintf(summary_file, ' %s', toSpacedString(scenario_name,         inf));
        fprintf(summary_file, '\n');
    end

    if (fclose(summary_file)~=0)
        disp(['Error while saving the file: ' summary_file_name])
    end 

end % updateSummary
