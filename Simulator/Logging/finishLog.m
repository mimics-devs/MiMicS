function finishLog(result_file, termination_code, termination_msg, ...
        elapsed_time_for_processing, elapsed_time_sim, map, ...
        num_reached_goal, num_escaped, num_crashed_with_robot, ...
        num_crashed_with_static, num_on_deadlock)
%FINISHLOG Finishes the log file of a simulation.
%
%   finishLog(result_file, termination_code, termination_msg, 
%       elapsed_time_for_processing, elapsed_time_sim, map, 
%       num_reached_goal, num_escaped, num_crashed_with_robot, 
%       num_crashed_with_static, num_on_deadlock)
%
% INPUT
% result_file - File pointer to the log file.
% termination_code - Numerical code representing the reason for termination.
% terminatino_msg - Human-readable string explaining the reason for termination.
% elapsed_time_for_processing - The time (in sec) which has passed in the 
%   real world from the start of the simulation until the end.
% elapsed_time_sim - The amount of time (in sec) that was simulated.
% map - An array of lines [X1 X2 Y1 Y2] representing the map.
% num_reached_goal - The number of robots which have reached their respective goals.
% num_escaped - The number of robots which have escaped from the map.
% num_chrased_with_robot - The number of robots which have crashed with other robots.
% num_crashed_with_static - The number of robots which have crashed with 
%   static obstacles (i.e. lines from the map).
% num_on_deadlock - The number of robots which have ended up in deadlock situation.
%
% OUTPUT
% Formats the input data and writes it into the log file.
%
% See also PREPARELOG, READLOG.


    fprintf(result_file, 'num_reached_goal = %d\n', num_reached_goal);
    fprintf(result_file, 'num_escaped = %d\n', num_escaped);
    fprintf(result_file, 'num_crashed_with_robot = %d\n', num_crashed_with_robot);
    fprintf(result_file, 'num_crashed_with_static = %d\n', num_crashed_with_static);
    fprintf(result_file, 'num_on_deadlock = %d\n', num_on_deadlock);
    
    fprintf(result_file, '\ntermination_code = %d\n', termination_code);
    fprintf(result_file, 'termination_msg = %s\n', termination_msg);
    fprintf(result_file, 'elapsed_simulated_time = %g\n', elapsed_time_sim);
    fprintf(result_file, 'elapsed_processing_time = %g\n', elapsed_time_for_processing);
    fprintf(result_file, '\n');
    fprintf(result_file, 'Map = [\n');
    for i = 1:size(map,1)
        fprintf(result_file, '%d %d %d %d\n', map(i,1), map(i,2), map(i,3), map(i,4));
    end
    fprintf(result_file, ']\n'); 
    
    fclose(result_file);
    
end

