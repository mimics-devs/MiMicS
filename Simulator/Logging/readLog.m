function log = readLog( path_to_dir )
%READLOG Reads all the information of a simulation's log folder into
%variables for easer post-processing.
%
%   log = readLog(path_to_dir)
% 
% INPUT
% path_to_dir - Path to log folder. Should contain a txt-file containing
% some general information and one CSV-formatted log per robot.
%
% OUTPUT
% log - Struct with all information read.
%
% See also PREPARELOG, FINISHLOG.

    number_of_robots_str = 'number_of_robots';
    start_of_map_definition_str = 'Map';
    num_header_lines_rob_csv = 9;
    delimiter = ';';

    if ~exist(path_to_dir, 'dir')
        error('The specified directory ''%s'' does not exist.', path_to_dir);
    end
    
    [~, folder_name, ~] = fileparts(path_to_dir);
    file_name = [path_to_dir filesep folder_name '.txt'];
    if ~exist(file_name, 'file')
        error('Could not find ''%s''.', file_name);
    end
    
    log = struct;
    
    % try to open file
    file_id = fopen(file_name);
    if file_id == -1
        error('Can not open file ''%s''.', file_name);
    end
    
    number_of_robots = 0;
    
    %% Read the general log file.
    fprintf('readLog: Processing %s ...', file_name);
    line = fgetl(file_id);
    while ischar(line)
        
        if strfind(line, start_of_map_definition_str)
            break;
        end
        if strcmp(line, '');
            line = fgetl(file_id);
            continue;
        end
        scan = textscan(line, '%s %s', 'delimiter', '=');
        
        name = strtrim(char(scan{1}));
        value = char(scan{2});
        if ~isnan(str2double(value))
            value = str2double(value);
            if strcmp(name, number_of_robots_str)
                number_of_robots = value;
            end
        end
        
        log.(name) = value;    
        line = fgetl(file_id);
    end % while-loop for header reading
    
    % Read map
    log.map_lines = [];
    line = fgetl(file_id);
    while ischar(line);
       if strfind(line, ']')
          break;
       end
       
       numbers = textscan(line, '%d %d %d %d');
       log.map_lines = [log.map_lines; numbers{1} numbers{2} numbers{3} numbers{4}];
       line = fgetl(file_id);
    end
    
    fprintf('done.\nreadLog: Processing robot CSV files (%i total)...', number_of_robots);
    
    log.robots = [];
    
    %% Read the robot CSVs
    for i_robot = 1:number_of_robots
        fprintf(' %d', i_robot);
        % Open file
        file_name = [path_to_dir filesep 'robot_' num2str(i_robot) '.csv'];
        if ~exist(file_name, 'file')
            error('Could not find ''%s''.', file_name);
        end
        file_id = fopen(file_name);
        if file_id == -1
            error('Can not open file ''%s''.', file_name);
        end
        
        % Read header
        for i = 1:num_header_lines_rob_csv
            line = fgetl(file_id);
            if strcmp(line, '');
                continue;
            end
            scan = textscan(line, '%s %s', 'delimiter', '=');

            name = strtrim(char(scan{1}));
            value = char(scan{2});
            
            if ~isnan(str2double(value))
                value = str2double(value);
            end
            
            % Does this CSV belong to the overall log?
            if isfield(log, name)
                if ischar(value)
                    if ~strcmp(log.(name), value)
                        error('Mismatch in field %s! In %s it is %s, should be %s\n', name, file_name, char(value), char(log.(name)));
                    end
                elseif isdouble(value)
                    if ~(log.(name) == value)
                        error('Mismatch in field %s! In %s it is %d, should be %d\n', name, file_name, value, log.(name));
                    end
                end
                
            else
                % Some robot specific field, e.g. vehicle width and length
                log.robots(i_robot).(name) = value;
            end

        end % for every header line
        
        % Read field names
        line = fgetl(file_id);
        field_names = strsplit(line, delimiter);
        num_field_names = numel(field_names);
        
        % Skip units
        fgetl(file_id);
        
        % Read data line by line
        % NOTE: Using MATLAB functions like csvread and dlmread seem to
        % fail since there are text fields (user_msg and sys_msg),
        % therefore this is done line by line.
        line = fgetl(file_id);
        while ischar(line)
            semicolon_idxs = find(line == ';'); 
            for i = 1:num_field_names
                % Extract value from text line
                if i > 1
                    char_start = semicolon_idxs(i-1) + 1;
                else
                    char_start = 1;
                end
                if i < num_field_names
                    char_end = semicolon_idxs(i) - 1;
                else
                    char_end = numel(line);
                end
                value = line(char_start:char_end);
                
                % Only the last 2 fields are allowed to be strings
                if i < num_field_names - 1
                    % value is a number
                    value = str2double(value);
                    if isfield(log.robots(i_robot), field_names(i) ) 
                        log.robots(i_robot).(field_names{i}) = [log.robots(i_robot).(field_names{i}); value];
                    else
                        log.robots(i_robot).(field_names{i}) = value;
                    end
                else
                    % value is a string (message)
                    if isfield(log.robots(i_robot), field_names(i) )  
                        log.robots(i_robot).(field_names{i}) = [log.robots(i_robot).(field_names{i}); {value}];
                    else
                        log.robots(i_robot).(field_names{i}) = {value};
                    end 
                end
                
            end % for every field
            line = fgetl(file_id);
        end
    end % for every robot
    fprintf(' done.\n');
    
    
end % function

