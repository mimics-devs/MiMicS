function diff = discontAngleDifference(angle1, angle2)
%DISCONTANGLEDIFFERENCE Computes the difference between two angles.
%
%   diff = discontAngleDifference(angle1, angle2)  
%
% Angles are from the [-pi,pi] range, the discontiunity is taken into
% account.
%
% INPUT
% angle1 - First angle in rad, -pi <= angle1 <= pi
% angle2 - Second angle, -pi <= angle2 <= pi
%
% OUTPUT
% diff- The difference between angle1 and angle2, -pi <= diff <= pi
%
% See also LIMITANGLETOPI.

    % shift angles from [-pi,pi] to [0,2pi] range
    angle1 = angle1 + pi;
    angle2 = angle2 + pi;

    % duplicate first angle
    angle1 = [angle1-2*pi angle1 angle1+2*pi];
    % gets the minimum of the 3 errors
    diff = 2*pi;
    for i=1:3
        diffAux = angle1(i) - angle2;
        if abs(diffAux) < abs(diff)
            diff = diffAux;
        end
    end
end