function angle = limitAngleToPi(angle)
%LIMITANGLETOPI Converts any angle into the [-pi,pi] range.
%
%   angle = limitAngleToPi(angle)
%
% INPUT
% angle - An arbitrary angle in rad
%
% OUTPUT
% angle - The same angle in the [-pi, pi] range.

    while angle > pi || angle < -pi
        if angle > pi
                angle = angle - 2*pi;
        else
                angle = angle + 2*pi;
        end
    end    
end