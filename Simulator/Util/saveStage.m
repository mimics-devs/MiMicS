function rslt = saveStage(path_to_dir, file_name, robots, map_name )
%SAVESTAGE Saved a scenario.
%
%   rslt = saveStage(path_name, file_name, robots, map_name)
%
% INPUT
% path_to_dir - Path to folder in which to save
% file_name - File name == scenario name
% robots - Array of RobotModel instances
% map_name - Name of the map.
%
% OUTPUT
% rslt - True if the scenario was successfully saved.

    initial_stage = zeros(numel(robots), 6);
    robot_types = cell(0,2);
    
    for i_rob = 1:numel(robots)
        initial_stage(i_rob, 1) = robots(i_rob).x_gt;
        initial_stage(i_rob, 2) = robots(i_rob).y_gt;
        initial_stage(i_rob, 3) = robots(i_rob).phi;
        initial_stage(i_rob, 4) = robots(i_rob).v_gt;
        initial_stage(i_rob, 5) = robots(i_rob).x_goal;
        initial_stage(i_rob, 6) = robots(i_rob).y_goal;
        
        % Check if this type has already occured
        type_name = robots(i_rob).type.name;
        type_found = false;
        for i_type = 1:size(robot_types,1)
            if strcmp(robot_types{i_type,1}, type_name)
                robot_types{i_type, 2} = [robot_types{i_type, 2} i_rob];
                type_found = true;
                break;
            end
        end
        if ~type_found
            % This type was not listed yet
            robot_types{end + 1, 1} = type_name;
            robot_types{end, 2} = i_rob;
        end
    end
    
    full_path = [path_to_dir file_name];
    file = fopen(full_path, 'w');
    if file == -1
        error(['Error while trying to open file: ' full_path]);
    end
    
    % TODO Headers
    [~, bare_name, ~] = fileparts(full_path);
    fprintf(file, '%% Scenario: %s\n', bare_name);
    fprintf(file, '%% Created: %s\n', datestr(now));
    fprintf(file, '\n');
    
    if numel(map_name) > 0
        fprintf(file, 'map_name = ''%s'';\n\n', map_name);
    end
    
    fprintf(file, 'initial_stage = [ ...\n');
    for i_rob = 1:size(initial_stage, 1)
        fprintf(file, '\t%d %d %d %d %d %d', ...
            initial_stage(i_rob,1), initial_stage(i_rob,2), ...
            initial_stage(i_rob,3), initial_stage(i_rob,4), ...
            initial_stage(i_rob,5), initial_stage(i_rob,6) ...
         );
         if i_rob ~= size(initial_stage, 1)
             fprintf(file, '; ...\n');
         else
             fprintf(file, ' ...\n];\n');
         end
    end
    fprintf('\n');
    fprintf(file, 'robot_types = { ...\n');
    for i_type = 1:size(robot_types,1)
        fprintf(file, '\t''%s'', ', robot_types{i_type,1});
        if numel(robot_types{i_type, 2}) > 1
            fprintf(file, '[%s]', num2str(robot_types{i_type,2}));
        else
            fprintf(file, '%s', num2str(robot_types{i_type,2}));
        end
        if i_type ~= size(robot_types, 1)
            fprintf(file, '; ...\n');
        else
            fprintf(file, ' ...\n};\n');
        end
    end
    
    rslt = 1;
    fprintf('Successfully saved %s\n', full_path);
end

