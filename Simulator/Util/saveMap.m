function rslt = saveMap(path_to_dir, file_name, map_lines)
%SAVEMAP Saves a map into a file.
%
%   rslt = saveMap(path_name, file_name, map_lines)
%
% INPUT
% path_to_dir - Path to folder in which to save
% file_name - File name == map name
% map_lines - Lines of the map in N x [x1 x2 y1 y2] format 
%
% OUTPUT
% rslt - True if the map was successfully saved.

rslt = false;

num_lines = size(map_lines, 1);

full_path = [path_to_dir file_name];
file = fopen(full_path, 'w');
if file == -1
    error(['Error while trying to open file: ' full_path]);
end

[~, bare_name, ~] = fileparts(full_path);
fprintf(file, '%% Map: %s\n', bare_name);
fprintf(file, '%% Created: %s\n', datestr(now));
fprintf(file, '\n');

fprintf(file, 'map_lines = [ ...');
for i = 1:num_lines
    fprintf(file, '\n\t%g %g %g %g; ...', map_lines(i,1), map_lines(i,2), map_lines(i,3), map_lines(i,4));
end
fprintf(file, '\n];');

rslt = true;

end

