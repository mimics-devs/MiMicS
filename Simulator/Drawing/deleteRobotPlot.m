function deleteRobotPlot(handles)
%DELETEROBOTPLOT Deletes all graphical objects representing a specific
%robot. Before doing so, checks if the handle is valid.
%   
%   deleteRobotPlot(handles)
%
% INPUT
% handles - A struct containing all handles to be deleted.

    if isfield(handles, 'filled_polygon') && ishandle(handles.filled_polygon)
        delete(handles.filled_polygon);
    end

    if isfield(handles, 'frontMarker') && ishandle(handles.frontMarker)
        delete(handles.frontMarker);
    end

    if isfield(handles, 'goal') && ishandle(handles.goal)
        delete(handles.goal);
    end

    if isfield(handles, 'reference') && ishandle(handles.reference)
        delete(handles.reference);
    end
end

