function [rob_plot_handles, max_dist_from_center, x_mapcenter, y_mapcenter] = ...
    initPlotStage(robots, map_lines, h_axes, replay_mode)
%INITPLOTSTAGE Prepares the simulation plotting.
%
%   [rob_plot_handles, max_dist_from_center, x_mapcenter, y_mapcenter] = initPlotStage(robots, map_lines, h_axes, replay_mode)
%
% INPUT
% robots - Array of RobotModel objects
% map_lines - If set, these lines will be plotted as the map.
% h_axes - The axes in which to plot
% replay_mode - If true, the plot will lack the lines indicating the
% robot's orientation and velocity reference.
%
% OUTPUT
% rob_plot_handles - A cell array containing the handles of the plotted
% robots.
% max_dist_from_center - If a robot exceeds this distance from the
% scenario's center, it is considered escaped.
% x_mapcenter - X-coordinate of the scenario's center.
% y_mapcenter - Y-coordinate of the scenario's center.
%
% See also INITPLOTROBOT, DRAWMAP.

    conf = Config.getInstance();
    
    % Get the limits of the plot considering the coordinates of the robots and a margin
    x_min = min( [robots.x_gt robots.x_goal] ) - conf.plot_margin;
    x_max = max( [robots.x_gt robots.x_goal] ) + conf.plot_margin;
    y_min = min( [robots.y_gt robots.y_goal] ) - conf.plot_margin;
    y_max = max( [robots.y_gt robots.y_goal] ) + conf.plot_margin;
    
    use_map = ~isempty(map_lines);
    if use_map
        [~, rect] = drawMap(map_lines, h_axes);
        % rect = [x y w h]
        x_min = min(x_min, rect(1));
        y_min = min(y_min, rect(2));
        x_max = max(x_max, rect(1) + rect(3));
        y_max = max(y_max, rect(2) + rect(4));
    end
    % Make square
    x_max = max(x_max, y_max); y_max = x_max;
    x_min = min(x_min, y_min); y_min = x_min;  

    set(h_axes, 'XLim', [x_min x_max]);
    set(h_axes, 'YLim', [y_min y_max]);
    
    % Initial plots of all robots
    color = hsv(numel(robots));
    rob_plot_handles = cell(numel(robots), 1);
    for i_rob = 1:numel(robots)
        %robots(iRobot).initialPlot(color(iRobot, :), h_stage, replay_mode, h_velbars);
        rob_plot_handles{i_rob} = initPlotRobot(h_axes, color(i_rob, :), robots(i_rob), replay_mode);
    end
    
    % Compute the necesary variables to determine if a robot is out of the
    % map
    x_mapcenter = (x_max - x_min)/2 + x_min;
    y_mapcenter = (y_max - y_min)/2 + y_min;
    
    % If a robot's distance to the center of the map is greater than this
    % distance, the robot is considered to be out of the map.
    max_dist_from_center = max([(x_max - x_min) (y_max - y_min)]/2);

    drawnow;
end % function

