function handles = initPlotRobot(h_axes, color, robot, replay_mode)
% INITIALPLOT Creates the first plot of the robot, in order to 
% assign the handles for later updates.
%
%   handles = initPlotRobot(h_axes, color, robot, replay_mode)
%
% INPUT
% h_axes - Handles of axes to plot into.
% color - RGB triplet, should ideally be unique to this robot
% robot - Instance of RobotModel to represent graphically
% replay_mode - If true, the velocity and orientation reference will not be
% drawn.
%
% OUTPUT
% handles - A struct of all plot handles created.
    
  
    % Plot a cross indicating the robot's goal
    if ~isnan(robot.x_goal) && ~isnan(robot.y_goal)
        handles.goal = plot(robot.x_goal, robot.y_goal, 'x', 'MarkerSize', 12, 'Color', color, 'Parent', h_axes);
    end

    % Plot a polygon representing the robot itself
    handles.filled_polygon = fill(robot.corners(:,1), robot.corners(:,2), 'white', ...
        'Parent', h_axes);

    % Plot a colored marker indicating the robot's current heading
    x_front = (robot.corners(1,1) + robot.corners(2,1))/2;
    y_front = (robot.corners(1,2) + robot.corners(2,2))/2;
    handles.frontMarker = fill( ...
        [x_front; robot.corners(3,1); robot.corners(4,1)], ...
        [y_front; robot.corners(3,2); robot.corners(4,2)], ...
        color, 'Parent', h_axes);

    % Plot a line that represents orientation and velocity reference
    if ~replay_mode
        handles.reference = line( ...
            [robot.x_gt, robot.x_gt + (robot.type.vehicle_length+robot.v_ref)*cos(robot.phi_ref)], ...
            [robot.y_gt, robot.y_gt + (robot.type.vehicle_length+robot.v_ref)*sin(robot.phi_ref)], ...
            'Color', color, 'Parent', h_axes);
    end

end
