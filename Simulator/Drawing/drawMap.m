function [line_handles, rect] = drawMap(map_lines, axes_handle)
%DRAWMAP Draws the map lines into given axes.
%
%   [line_handles, rect] = drawMap(map_lines, axes_handle)
%
% INPUT
% map_lines - The array of lines in [X1 X2 Y1 Y2] format to be drawn.
% axes_handle - The handle of the axes in which to plot.
%
% OUTPUT
% line_handles - Handles of the drawn lines
% rect - The rectangle specifying the map's dimensions [minX, minY, width, height]
%
% See also INITPLOTSTAGE.

    minX = min( [map_lines(:,1); map_lines(:,2)] );
    maxX = max( [map_lines(:,1); map_lines(:,2)] );
    minY = min( [map_lines(:,3); map_lines(:,4)] );
    maxY = max( [map_lines(:,3); map_lines(:,4)] );
    
    width = maxX - minX;
    height = maxY - minY;
    rect = [minX, minY, width, height];

    line_handles = zeros(size(map_lines,1), 1);
    if ~isempty(map_lines)
        for i = 1:size(map_lines,1)
            line_handles(i) = line( ...
                [map_lines(i,1) map_lines(i,2)], ...
                [map_lines(i,3) map_lines(i,4)], ...
                'Color', 'k', 'Parent', axes_handle);
        end
    end
    
    drawnow;

end

