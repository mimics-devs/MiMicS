function updateRobotPlot(handles, rob)
% UPDATEROBOTPLOT Takes data from the robot and updates the plot
% accordingly.
%
%   updatePlot(handles, rob)
%
% INPUT
% handles - Handles of the graphical objects representing the robot
% robot - Instance of RobotModel
%
% See also ROBOTMODEL, INITPLOTROBOT.

    % Update the polygon frame
    set(handles.filled_polygon, ...
        'XData', rob.corners(:,1), 'YData', rob.corners(:,2));

    % Update the colored triangle
    x_front = (rob.corners(1,1) + rob.corners(2,1))/2;
    y_front = (rob.corners(1,2) + rob.corners(2,2))/2;
    set(handles.frontMarker, ...
        'XData', [x_front; rob.corners(3,1); rob.corners(4,1)], ...
        'YData', [y_front; rob.corners(3,2); rob.corners(4,2)]);

    % Update the reference line
    if isfield(handles, 'reference')
        set(handles.reference, ...
            'XData', [rob.x_gt, rob.x_gt + (rob.type.vehicle_length+rob.v_ref)*cos(rob.phi_ref)], ...
            'YData', [rob.y_gt, rob.y_gt + (rob.type.vehicle_length+rob.v_ref)*sin(rob.phi_ref)]);
    end

    % Update the velocity bar
    if isfield(handles, 'velocity_bar')
        set(handles.velocity_bar, 'YData', rob.v_gt);
    end
end
