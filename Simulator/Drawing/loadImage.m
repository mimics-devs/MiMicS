function [im, map] = loadImage(image_name, background_color)
%LOADIMAGE Loads a MiMicS image (both jpeg and png).
%
%   [im, map] = loadImage(image_name, background_color)
%
% INPUT
% image_name - Name of image within Simulator/Drawing/Images.
% background_color (optional) - RGB triplet, to be used if the image has
% transparent parts.
%
% OUTPUT
% im - The image
% map - A color map

image_path = ['.' filesep 'Simulator' filesep 'Drawing' filesep 'Images' filesep image_name];

% Loading the image
if nargin > 1
    % Image is a PNG, imread need an extra argument
    [im, map] = imread( image_path, 'BackgroundColor', background_color);
else
    % JPGs
    [im, map] = imread( image_path);
end

if (length(size(im)) == 2)
    % Black&White image, converting to RGB
    im = ind2rgb(im, map);
end


end

