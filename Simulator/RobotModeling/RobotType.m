classdef RobotType < handle
    %ROBOTTYPE Defines a type of robot.
    %
    % This class is used to store vehicle characteristics such as dynamics,
    % physical dimensions, PID controller settings etc shared by a multitude of
    % robots. It uses the characteristics of an Pioneer3AT as default. Other
    % characteristics may be defined through script files, which can be copied
    % to an instance of this class using the sanitizeAndCopy() function.
    %
    % See also ROBOTMODEL.
    
    properties (Constant)
        % Name of the default robot type
        DEFAULT_NAME = 'Pioneer3AT (default)';
    end
    
    properties
        % Name of this robot type
        name = 'Pioneer3AT (default)';
        % Speed/Traction dynamic (Note: robot is actuated by acceleration,
        % don't forget the integrator!)
        % Should be a transfer function, but the simulator will make it a
        % discrete state space model later.
        sptr_dyn = tf(1, [0.5 1 0]);
        % These are short for sptr_dyn.a, .b etc and save a ridiciulous
        % amount of time, since MATLAB was not designed for OOP.
        sptr_dyn_a;
        sptr_dyn_b;
        sptr_dyn_c;
        sptr_dyn_d;
        % Steering dynamic
        steer_dyn = tf(1, [0.2 1]);
        steer_dyn_a;
        steer_dyn_b;
        steer_dyn_c;
        steer_dyn_d;
        % Parameters [Kp Ki Kd] for PID-control of v
        % Values computed with pidtool(sys,'pid') [Response time=0.08899s Transient behavior=0.9 ]
        v_pid = [80.0 0.1 11.0];
        % Parameters [Kp Ki Kd] for PID-control of phi
        phi_pid = [20 0 3];
        % maximum translational velocity in m/s
        v_max = 1
        % minimum translational velocity in m/s
        v_min = -1;
        % maximum rotational velocity (both clock- and counter-clockwise) in rad/s
        omega_max = 1
        % width in meters
        vehicle_width = 0.5;
        % length in meters
        vehicle_length = 0.7;
        % safety radius >= sqrt( (vehicle_width/2)^2 + (vehicle_width/2)^2 )
        safety_radius = 0.44;
        % Function handle on the velocity for the minimum steering radius
        min_steer_radius = @(v) 1*v;
        
        % The minimum and maximum distances the scanner can detect in meters
        ranger_minmax = [0.1 30];
        % The field-of-view of the ranger in degrees, one half spreading to the
        % left and one half to the right of the direction the sensor is facing
        ranger_fov = 270;
        % The number of beams generated within the fov of the laserscanner
        ranger_num_beams = 270;
        
        % standard deviation of gaussian noise on position values
        noise_pos = 0;
        % standard deviation of gaussian noise on position odometry
        noise_odom_pos = 0;
        % standard deviation of gaussian noise on the velocity
        noise_v = 0;
        % standard deviation of gaussian noise on the acceleration
        noise_a = 0;
        % standard deviation of gaussian noise on the orientation
        noise_phi = 0;
        % standard deviation of gaussian noise on orientation odometry
        noise_odom_phi = 0;
        % mean of gaussian noise on the laser scanner
        noise_ranger_mean = 0;
        % standard deviation of gaussian noise on the laser scanner
        noise_ranger_stddev = 0;
    end % properties
    
    methods
        
        
        function obj = RobotType(name, full_path)
        % CONSTRUCTOR Creates an instance of the 'RobotType' class.
        %
        %   obj = RobotType(name, full_path)
        %
        % Takes the values set in the script located by full_path and
        % copies them to this instance.
        %
        % INPUT
        % name - Type name to be used (usually file name of script)
        % full_path - Path to script file
                 
            % get the values from the file
            if ~strcmp(full_path, obj.DEFAULT_NAME)
                run(full_path);
                obj.name = name;
                obj.sptr_dyn = sptr_dyn;
                obj.steer_dyn = steer_dyn;
                obj.v_pid = v_pid;
                obj.phi_pid = phi_pid;
                obj.v_max = v_max;
                obj.v_min = v_min;
                obj.omega_max = omega_max;
                obj.vehicle_width = vehicle_width;
                obj.vehicle_length = vehicle_length;
                obj.safety_radius = safety_radius;
                obj.ranger_minmax = ranger_minmax;
                obj.ranger_fov = ranger_fov;
                obj.ranger_num_beams = ranger_num_beams;
                obj.min_steer_radius = min_steer_radius;
                obj.noise_pos = noise_pos;
                obj.noise_odom_pos = noise_odom_pos;
                obj.noise_v = noise_v;
                obj.noise_phi = noise_phi;
                obj.noise_odom_phi = noise_odom_phi;
                obj.noise_ranger_mean = noise_ranger_mean;
                obj.noise_ranger_stddev = noise_ranger_stddev;

                % PID
                if ~isnumeric(obj.v_pid) || size(obj.v_pid,1) ~= 1 || size(obj.v_pid,2) ~= 3
                    error('%s: v_pid needs to be a vector of the form [Kp Ki Kd]', class(obj));
                end
                if  ~isnumeric(obj.phi_pid) || size(obj.phi_pid,1) ~= 1 || size(obj.phi_pid,2) ~= 3
                    error('%s: phi_pid needs to be a vector of the form [Kp Ki Kd]', class(obj));
                end
                % maximum velocities
                if ~isscalar(obj.v_max) || obj.v_max <= 0
                    error('%s: v_max has to be a positive scalar', class(obj));
                end
                if ~isscalar(obj.v_min) || obj.v_min > 0
                    error('%s: v_min has to be a non-positive scalar', class(obj));
                end
                if ~isscalar(obj.omega_max) || obj.omega_max <= 0
                    error('%s: omega_max has to be a positive scalar', class(obj));
                end
                % dimensions
                if ~isscalar(obj.vehicle_width) || obj.vehicle_width <= 0
                    error('%s: vehicle_width has to be a positive scalar', class(obj));
                end
                if ~isscalar(obj.vehicle_length) || obj.vehicle_length <= 0
                    error('%s: vehicle_length has to be a positive scalar', class(obj));
                end
                if ~isscalar(obj.safety_radius) || obj.safety_radius <= 0
                    error('%s: safety_radius has to be a positive scalar', class(obj));
                end
                % laserscanner
                if ~isnumeric(obj.ranger_minmax) || size(obj.ranger_minmax,2) ~= 2 || size(obj.ranger_minmax,1) ~= 1
                    error('%s: laserscanner has to be a vector of the form [minrange maxrange]', class(obj));
                end
                if ~isscalar(obj.ranger_fov) || obj.ranger_fov < 0 || obj.ranger_fov > 360
                    error('%s: ranger_fov has to be a scalar from interval (0,360)', class(obj));
                end
                if ~isPositiveIntegerValuedNumeric(obj.ranger_num_beams)
                    error('%s: ranger_num_beams has to be a positive integer', class(obj));
                end
                % minimum steering radius
                if ~isa(obj.min_steer_radius, 'function_handle')
                    error('%s: min_steer_radius has to be a function handle', class(obj));
                end
                % noise
                if ~isscalar(obj.noise_pos) || obj.noise_pos < 0
                    error('%s: noise_pos has to be a non-negative scalar', class(obj));
                end
                if ~isscalar(obj.noise_v) || obj.noise_v < 0
                    error('%s: noise_v has to be a non-negative scalar', class(obj));
                end
                if ~isscalar(obj.noise_phi) || obj.noise_phi < 0
                    error('%s: noise_phi has to be a non-negative scalar', class(obj));
                end
                if ~isscalar(obj.noise_ranger_mean)
                    error('%s: noise_ranger_mean has to be a scalar', class(obj));
                end
                if ~isscalar(obj.noise_ranger_stddev) || obj.noise_ranger_stddev < 0
                    error('%s: noise_ranger_stddev has to be a non-negative scalar', class(obj));
                end
            end % if other than default type
            
            % Get discretized state-space models of the robot dynamics. We
            % could as well use just the tf in combination with lsim, but
            % discretizing it here and using ltitr (core part of lsim) is much
            % faster.
            obj.sptr_dyn = c2d(ss(obj.sptr_dyn), Config.getInstance().delta_t);
            obj.sptr_dyn_a = obj.sptr_dyn.a;
            obj.sptr_dyn_b = obj.sptr_dyn.b;
            obj.sptr_dyn_c = obj.sptr_dyn.c;
            obj.sptr_dyn_d = obj.sptr_dyn.d;
            obj.steer_dyn = c2d(ss(obj.steer_dyn), Config.getInstance().delta_t);
            obj.steer_dyn_a = obj.steer_dyn.a;
            obj.steer_dyn_b = obj.steer_dyn.b;
            obj.steer_dyn_c = obj.steer_dyn.c;
            obj.steer_dyn_d = obj.steer_dyn.d;
            
        end % constructor
        
    end % methods
    
end % class

