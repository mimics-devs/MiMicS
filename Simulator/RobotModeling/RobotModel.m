classdef RobotModel < handle
%ROBOTMODEL Represents a robot.
%
% This class models a single robot. It can simulate the robot's movement
% during a simulation time step, and has many utility functions for
% plotting, logging, collision detection etc.
%
% See also ROBOTTYPE.
    
    properties
        % Storage for user-defined data
        vars = struct();
    end % properties (normal access)
    
    properties (SetAccess = protected)
        % ID of the robot, normally an integer (read-only)
        id;
        % Steptime in seconds
        delta_t = 0.1;
        % Reference to a RobotType instance (read-only)
        type;
        % is the robot active?
        active = false;
        % has a deactivation been triggered during THIS sim step?
        deactivated_recently = false;
        % X-coordinate of the robot's goal in meters
        x_goal = 0.0;
        % Y-coordinate of the robot's goal in meters
        y_goal = 0.0;
        % Distance to goal at which the robot is considered to be on it.
        dist_arrival = 0.25;
        
        % Position in X with noise in meters (read-only)
        x = 0.0;
        % Odometric X-position in meters (read-only)
        x_odom = 0.0;
        % Groundtruth X-position in meters (read-only)
        x_gt = 0.0;
        % Position in Y with noise in meters (read-only)
        y = 0.0;
        % Odometric Y-position in meters (read-only)
        y_odom
        % Groundtruth Y-position in meters (read-only)
        y_gt = 0.0;
        % Translational velocity with noise in m/s (read-only)
        v = 0.0;
        % Groundtruth velocity in m/s (read-only)
        v_gt = 0.0;
        % Translational acceleration with noise in m/s² (read-only)
        a = 0.0;
        % Groundtruth translational acceleration in m/s²
        a_gt = 0.0;
        % Orientation with noise in rad (read-only)
        phi = 0.0;
        % Odometric orientation in rad (read-only)
        phi_odom
        % Groundtruth orientation in rad (read-only)
        phi_gt = 0.0;
        % Rotational velocity in rad/s (read-only)
        omega = 0.0;
        % Rotational acceleration rad/s² (read-only)
        alpha = 0.0;
        
        % distance to the goal (read-only)
        dist_to_goal = 0.0;
        
        % current velocity reference (read-only)
        v_ref = 0.0;
        % current orientation reference (read-only)
        phi_ref = 0.0;
        % velocity actuator (read-only)
        a_act = 0.0;
        % rotational velocity actuator (read-only)
        omega_act = 0.0;
        
        % X/Y coordinates of this robots corners
        corners = zeros(4,2)
        
    end % properties (protected SetAccess) (read-only)
    
    properties (Access = private)
        % Integral of v_error for PID-control (read-only)
        v_error_integral = 0.0
        % Integral of phiErr for PID-control (read-only)
        phi_error_integral = 0.0
        
        % Pointer to log file
        log_file
        % Cell array containing the names of properties to log,
        % additionally to the basic ones (x_gt,y_gt,phi_gt). Will be
        % checked and set by setLog().
        log_extra = {};
        % User message to log
        log_user_msg = '';
        % System message to log
        log_sys_msg = '';
    end % properties (private)
    
    properties (Constant)
        % Contains the units of all loggable class properties.
        UNITS = struct( ...
            'x','m', 'x_odom', 'm', 'x_gt','m', 'y','m', 'y_odom', 'm', 'y_gt','m', ...
            'v','m/s', 'v_gt','m/s', 'a','m/s²', 'a_gt','m/s²', ...
            'phi','rad', 'phi_odom', 'rad', 'phi_gt','rad', ...
            'omega','rad/s', 'alpha','rad/s²', ...
            'x_goal','m', 'y_goal','m', 'dist_to_goal','m', ...
            'v_ref','m/s', 'phi_ref', 'rad/s', ...
            'a_act','m/s²','omega_act','rad/s');
    end % constants
    
    methods
        
        function obj = RobotModel(id)
        % CONSTRUCTOR
            
            obj.id = id;
            obj.active = false;
            obj.type = RobotTypeManager.getInstance().getOrCreateRobotTypeByName(RobotType.DEFAULT_NAME);
            
            % Save local copies to reduce time for access
            obj.delta_t = Config.getInstance().delta_t;
        end
        
        function setType(obj, type_name)
        %SETTYPE Sets a type for the robot.
        %
        %   obj.setType(type_name)
        %
        % INPUT
        % type_name - Filename (without path) of the script that defines a
        % RobotType variant.
            if ~obj.active
                obj.type = RobotTypeManager.getInstance().getOrCreateRobotTypeByName(type_name);
                obj.updateCornerPoints();
            end
        end
        
        function setPos(obj, x, y)
        %SETPOS Sets a new position for the robot.
        %
        %   obj.setPos(x, y)
        %
        % INPUT
        % x - X-coordinate of new (groundtruth) position
        % y - Y-coordinate of new (groundtruth) position
            if ~obj.active
                obj.x = x;
                obj.x_odom = x;
                obj.x_gt = x;
                obj.y = y;
                obj.y_odom = y;
                obj.y_gt = y;
                obj.updateGoalDistance();
                obj.updateCornerPoints();
            end
        end
        
        function setOri(obj, phi)
        %SETORI Sets a new orientation for the robot.
        %
        %   obj.setOri(phi)
        %
        % INPUT
        % phi - The new orientation [-pi,pi]
            if ~obj.active
                obj.phi = phi;
                obj.phi_odom = phi;
                obj.phi_gt = phi;
                obj.updateCornerPoints();
            end
        end
        
        function setOdom(obj, x, y, phi)
        %SETODOM Reset the odometry estimation of position and orientation.
        %
        %   obj.setOdom(x, y, phi)
        %
        % INPUT
        % x - X-coordinate of new odometry position
        % y - Y-coordinate of new odometry position
        % phi - The new odometry orientation [-pi,pi]
            obj.x_odom = x;
            obj.y_odom = y;
            obj.phi_odom = phi;
        end
        
        function setGoal(obj, x, y)
        %SETORI Sets a new goal for the robot.
        %
        %   obj.setGoal(x, y)
        %
        % INPUT
        % x - X-coordinate of the new goal
        % y - Y-coordinate of the new goal
            obj.x_goal = x;
            obj.y_goal = y;
            
            obj.updateGoalDistance();
        end
        
        function obj = setDistArrival(obj, dist)
        %SETDISTARRIVAL sets the distance between the robot and its goal
        % at which is is considered to have arrived on it.
        %
        %   obj.setDistArrival(dist)
        %
        % INPUT
        % dist - The new distance in meters.
            obj.dist_arrival = dist;
        end
        
        function ready = checkReady(obj)
        %CHECKREADY reurns true if all necessay variables are correctly
        % initialized.
        %
        %   ready = obj.checkReady()
            
            nNan = sum( isnan([obj.id obj.x obj.x_gt obj.y obj.y_gt obj.x_goal obj.y_goal]));
            if nNan > 0
                ready = false;
            else
                ready = true;
            end
        end
        
        function active = activate(obj)
        %SETACTIVE Sets the robot's status to 'active' if all necessary 
        %values have been set.
        %
        %   obj.activate()
            if (checkReady(obj))
                obj.active = true;
            else
                warning('Robot with id %d is not ready and could therefore not be activated. Check if all necessary values were set.', obj.id);
            end
            active = obj.active;
        end
        
        function deactivate(obj, msg)
        %DEACTIVATE the robot, no further simulation is perfomed.
        %
        % deactivate(obj, msg)
        %
        % msg: A string describing the reason why the robot was
        % deactivated. Will be added to the log file as a system message.
            
            obj.active = false;
            obj.deactivated_recently = true;
            obj.v_ref = 0;
            obj.v = 0;
            obj.v_gt = 0;
            obj.a = 0;
            obj.a_gt = 0;
            obj.omega = 0;
            obj.alpha = 0;
            
            % Log the deactivation message and close the file
            obj.log_sys_msg = ['Deactivation: ' msg];
            
        end % deactivate
        
        function deactivationCleanUp(obj)
        %DEACTIVATIONCLEANUP Does some final clean up after deactivation.
        %
        %   obj.deactivationCleanUp()
        %
        % Has to be in its own function rather than within deactivate in
        % case the user wants the main loop to be done with a parfor.
            
            fprintf(obj.log_file, '%s', obj.log_sys_msg);
            obj.closeLog();
            
            obj.deactivated_recently = false;
        end
        
        %% Simulation
        
        function runControlledSim(obj, v_ref, phi_ref)
        %RUNCONTROLLEDSIM Short-hand for runControl + runSim.
        %
        %   obj.runControlledSim(v_ref, phi_ref)
        %
        % INPUT
        % v_ref - The velocity reference (m/s)
        % phi_ref - The orientation reference (rad)
        %
        % See also RUNCONTROL, RUNSIM.
            [obj.a_act, obj.omega_act] = obj.runControl(v_ref, phi_ref);
            obj.runSim(obj.a_act, obj.omega_act);
            return
        end % function runControlledSim
        
        function [a_act, omega_act] = runControl(obj, v_ref, phi_ref)
        %RUNCONTROL Computes the output of the PID-controllers.
        %
        %   [a_act, omega_act] = obj.runControl(v_ref, phi_ref)
        %
        % INPUT
        % v_ref - The velocity reference (m/s)
        % phi_ref - The orientation reference (rad)
        %
        % OUTPUT
        % a_act - Translational acceleration actuator
        % omega_act - Rotational velocity actuator
        
            obj.v_ref = v_ref;
            obj.phi_ref = phi_ref;
            
            % getting the velocity error is easy...
            v_error = v_ref - obj.v_gt;
            % but for the orientation error, we have to mind the
            % discontinuity at 0°/360°
            phi_error = discontAngleDifference(phi_ref, obj.phi);
            
            % add the errors to the integrals
            obj.v_error_integral = obj.v_error_integral + v_error*obj.delta_t;
            obj.phi_error_integral = obj.phi_error_integral + phi_error*obj.delta_t;
            % anti wind-up: limit integrals to maximal error
            if obj.v_error_integral > 2*obj.type.v_max
                obj.v_error_integral = 2*obj.type.v_max;
            elseif obj.v_error_integral < -2*obj.type.v_max
                obj.v_error_integral = -2*obj.type.v_max;
            end
            if obj.phi_error_integral > pi/4
                obj.phi_error_integral = pi/4;
            elseif obj.phi_error_integral < -pi/4
                obj.phi_error_integral = -pi/4;
            end
            
            % compute actuators
            a_act = obj.type.v_pid(1) * v_error ...
                + obj.type.v_pid(2) * obj.v_error_integral ...
                + obj.type.v_pid(3) * (-obj.a);
            
            omega_act = obj.type.phi_pid(1)*phi_error ...
                + obj.type.phi_pid(2)*obj.phi_error_integral ...
                + obj.type.phi_pid(3)*(-obj.omega);
            %limit omega actuator
            if omega_act > obj.type.omega_max
                omega_act = obj.type.omega_max;
            elseif omega_act < -obj.type.omega_max
                omega_act = -obj.type.omega_max;
            end
            
            obj.a_act = a_act;
            obj.omega_act = omega_act;
            return
        end % function runControl
        
        function runSim(obj, a_act, omega_act)
        %RUNSIM Run a simulation step.
        %
        %   obj.runSim(a_act, omega_act)
        %
        % INPUT
        % a_act - The translational acceleration actuator
        % omega_act - The rotational velocity actuator
              
            % get new velocity
            sptr_state = ltitr(obj.type.sptr_dyn_a, obj.type.sptr_dyn_b, [a_act; a_act], [obj.a; obj.v_gt/obj.type.sptr_dyn_c(2)]);
            sptr_out = obj.type.sptr_dyn_c*sptr_state' + obj.type.sptr_dyn_d*a_act;
            next_v_gt = sptr_out(2);
            % limit velocity
            next_v_gt = min(next_v_gt, obj.type.v_max);
            next_v_gt = max(next_v_gt, obj.type.v_min);
            
            % get new rotational velocity
            steer_state = ltitr(obj.type.steer_dyn_a, obj.type.steer_dyn_b, [omega_act; omega_act], obj.omega/obj.type.steer_dyn_c);
            steer_out = obj.type.steer_dyn_c*steer_state' + obj.type.steer_dyn_d*omega_act;
            next_omega = steer_out(2);
            % limit rotational velocity
            next_omega = min(next_omega, obj.type.omega_max);
            next_omega = max(next_omega, -obj.type.omega_max);
            
            delta_phi = ((next_omega+obj.omega)/2)*obj.delta_t;
            next_phi_gt = obj.phi_gt + delta_phi;
            
            % helper variables
            v_h = (obj.v_gt + next_v_gt)/2;
            phi_h = (obj.phi_gt + next_phi_gt)/2;
            
            % get new groundtruth position
            obj.x_gt = obj.x_gt + v_h*cos(phi_h)*obj.delta_t;
            obj.y_gt = obj.y_gt + v_h*sin(phi_h)*obj.delta_t;
            
            obj.a_gt = (next_v_gt - obj.v_gt)/obj.delta_t;
            obj.alpha = (next_omega - obj.omega)/obj.delta_t;
            
            % limit phi to -pi:pi, but only after calculation of x and y to avoid errors
            obj.phi_gt = limitAngleToPi(next_phi_gt);
            
            % Set speed
            obj.v_gt = next_v_gt;
            obj.omega = next_omega;
            
            % calculate noise-polluted values
            obj.x = obj.x_gt + obj.type.noise_pos*randn();
            obj.y = obj.y_gt + obj.type.noise_pos*randn();
            obj.v = obj.v_gt + obj.type.noise_v*randn();
            obj.a = obj.a_gt + obj.type.noise_a*randn();
            obj.phi = limitAngleToPi(obj.phi_gt + obj.type.noise_phi*randn());
            
            % Odometry
            %TODO Is this an accurate model?
            if abs(delta_phi) > 0  
                obj.phi_odom = limitAngleToPi(obj.phi_odom + delta_phi + obj.type.noise_odom_phi*randn());
            end
            err_odo_dist = obj.type.noise_odom_pos*randn();
            obj.x_odom = obj.x_odom + v_h*cos(obj.phi_odom)*obj.delta_t + err_odo_dist;
            obj.y_odom = obj.y_odom + v_h*sin(obj.phi_odom)*obj.delta_t + err_odo_dist;
            
            obj.updateGoalDistance();
            obj.updateCornerPoints();
        end % function runSim
        
        function updateCornerPoints(obj)
        %UPDATECORNERPOINTS Calculates the coordinates of the corner points
        % and saves them in the corresponding property.
        %
        %   obj.updateCornerPoints()
        %
        % If phi=0, the order of the points is given like this:
        % 4--------------1
        % |              |
        % |             >|
        % |              |
        % 3--------------2
            
            width_h = obj.type.vehicle_width/2;
            length_h = obj.type.vehicle_length/2;
            
            obj.corners = [obj.x_gt+length_h obj.y_gt+width_h; ...
                obj.x_gt+length_h obj.y_gt-width_h; ...
                obj.x_gt-length_h obj.y_gt-width_h; ...
                obj.x_gt-length_h obj.y_gt+width_h];
            % rotate
            cosphi = cos(obj.phi_gt);
            sinphi = sin(obj.phi_gt);
            obj.corners = [(obj.corners(:,1) - obj.x_gt)*cosphi - (obj.corners(:,2)-obj.y_gt)*sinphi + obj.x_gt, ...
                (obj.corners(:,1) - obj.x_gt)*sinphi + (obj.corners(:,2)-obj.y_gt)*cosphi + obj.y_gt];
            
            return;
        end % updateCornerPoints
        
        function edges = getEdges(obj)
        %GETEDGES Returns the edges of the robot's outline (a polygon).
        %
        % edges = getEdges(obj)
        %
        % OUTPUT
        % edges - A matrix, each row consists of a line representing an edge
        % in [X1 X2 Y1 Y2] format.
            num_edges = size(obj.corners,1);
            edges = zeros(num_edges, 4);
            for i_edge = 1:num_edges
                edges(i_edge,:) = [obj.corners(i_edge, 1) obj.corners(mod(i_edge,num_edges)+1, 1) ...
                    obj.corners(i_edge, 2) obj.corners(mod(i_edge,num_edges)+1, 2)];
            end
        end
        
        %% Utility
        
        function rslt = collision(obj, robot)
        %COLLISION Checks for a collision with another robot.
        %
        % rslt = collision(obj, robot)
        %
        % INPUT
        % robot - The other robot (instance of RobotModel)
        %
        % OUTPUT
        % rlst - 1 if there is a collision, 0 if not
            
            % at first, assume there is no collision.
            rslt = false;
            
            % Own corner points
            x_own = obj.corners(:,1);
            y_own = obj.corners(:,2);
            % corner points of the other robot
            x_oth = robot.corners(:,1);
            y_oth = robot.corners(:,2);
            
            % is a corner point of this robot inside the other one?
            in = inpolygon(x_own, y_own, x_oth, y_oth);
            if any(in > 0)
                rslt = true;
                return
            end
            % is a corner point of the other robot inside this one?
            in = inpolygon(x_oth, y_oth, x_own, y_own);
            if any(in > 0)
                rslt = true;
                return
            end
            
            % What if the robots share approximately the same position but
            % are rotated so that the corner points don't fall into each
            % others rectangles? This case is only a theoretical one (a
            % collision should be detected before this happens), but just
            % to be sure for the simulation let's test this as well.
            min_dist = min(robot.type.vehicle_width/2, robot.type.vehicle_length/2) ...
                + min(obj.type.vehicle_width/2, obj.type.vehicle_length/2);
            dist = sqrt( (obj.x_gt - robot.x_gt)^2 + (obj.y_gt - robot.y_gt)^2 );
            if dist < min_dist
                rslt = true;
                return
            end
        end % collision
        
        function v = getDesiredVelocity(obj)
        %GETDESIREDVELOCITY Enables a graceful braking when the robot is in
        %proximity of the goal, and stops the robot when it has reached it.
        %
        % v = getDesiredVelocity(obj)
        %
        % OUTPUT
        % v - The desired velocity in m/s
            
            dist_braking = 4*obj.dist_arrival;
            v_braking_max = 0.7*obj.type.v_max;   %0.5
            v_braking_min = 0.15*obj.type.v_max;  %0.0
            
            if obj.dist_to_goal < dist_braking
                if obj.dist_to_goal < obj.dist_arrival  % arrived at goal
                    v = 0;
                else                        % breaking distance reached
                    % If the robot is not looking towards the desired
                    % orientation, the braking force should be even higher
                    if (abs( discontAngleDifference(obj.phi_ref, obj.phi) ) > pi/4)
                        v_braking_max = 0.5*v_braking_max;
                    end
                    
                    slope = ( v_braking_max - v_braking_min )/( dist_braking - obj.dist_arrival);
                    offset = ( v_braking_max - slope*dist_braking );
                    v = slope*obj.dist_to_goal + offset;
                end
            else
                % robot is relatively far away from the goal
                v = obj.type.v_max;
            end
        end % getDesiredVelocity
        
        function phi = getGoalOrientation(obj)
        %GETGOALORIENTATION Calculates the orientation at which the robot
        % would be pointed towards its goal.
        %
        %   phi = getGoalOrientation(obj)
        %
        % OUTPUT
        % phi - Goal orientation in rad
            phi = atan2( (obj.y_goal-obj.y), (obj.x_goal-obj.x) );
        end
        
        function distance_to_goal = updateGoalDistance(obj)
        %UPDATEGOALDISTANCE Calculates the distance to the robot's goal and
        %saves it in the designated class property.
        %
        %   distance_to_goal = updateGoalDistance(obj)
        %
        % OUTPUT
        % distance_to_goal - Distance to this robot's goal in meters
            
            distance_to_goal = sqrt( (obj.x_goal - obj.x)^2 + (obj.y_goal - obj.y)^2 );
            obj.dist_to_goal = distance_to_goal;
        end
        
        function reportToComm(obj, comm)
        %REPORTTOCOMM Reports some (possibly noise polluted) values to the
        %communication system the robots share.
        %
        %   obj.reportToComm(comm)
        %
        % INPUT
        % comm - Handle of the CommunicationSystem instance.
            comm.report(obj.id, obj.x, obj.y, obj.v, obj.a, obj.phi, obj.omega);
        end
        
        %% Logging
        
        function setLog(obj, log_file, log_extra)
        %SETLOG Sets the log file for this robot and which properties to
        %log additionally to the basics.
        %
        %   obj.setLog(log_file, log_extra)
        %
        % INPUT
        % log_file - File pointer to an opened txt-file
        % log_extra - A cell array containing the names of properties to log.
            
            obj.log_file = log_file;
            obj.log_extra = log_extra;
        end % setLog
        
        function log(obj, time)
        %LOG Adds a line to this robot's individual log file.
        %
        %   obj.log(time)
        %
        % INPUT
        % time - The current simulation time
        %
        % Some values (i.e. position and orientation are always logged, some
        % additional values can be logged too by setting the ROBOT_LOG_EXTRA
        % variable before the simulations starts. See RobotModel.setLog().
        % Also logs a string message, if it was set with setLogMsg() during
        % the simulation step.
            
            % Log the basics
            fprintf(obj.log_file, '\n%g;%g;%g;%g;', time, obj.x_gt, obj.y_gt, obj.phi_gt);
            
            % Now additional values
            for i = 1:numel(obj.log_extra)
                fprintf(obj.log_file, '%g;', obj.(obj.log_extra{i}));
            end
            
            % If set via setlogMsg, print a message and clear the buffer.
            if numel(obj.log_user_msg > 0)
                fprintf(obj.log_file, '%s', obj.log_user_msg);
                obj.log_user_msg = '';
            end
            fprintf(obj.log_file, ';');
        end
        
        function setLogMsg(obj, msg)
        %SETLOGMSG Temporarily stores a string message to log until the log
        %file is written in the log() function.
        %
        %   obj.setLogMsg(msg)
        %
        % INPUT
        % msg - The string message to log.
        %
        % Note that this function sanitize your input (i.e. remove
        % semicolons and line breaks) in order to keep the CSV structure of
        % the log file clean.
            msg = strrep(msg, ';', '|');
            msg = strrep(msg, '\n', ' ');
            obj.log_user_msg = msg;
        end
        
        function closeLog(obj)
        %CLOSELOG Closes the log file of this robot.
        %
        %   obj.closeLog()
            fclose(obj.log_file);
        end
        
    end % methods
    
end % class


