#include <stdio.h>
#include <math.h>
#include "mex.h"
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define TOL 0.000001

mxArray* getBeamMeasurements(double x_beam_start, double y_beam_start, double *beam_angles, int num_beams, double *range, double *obs_lines, int num_obs_lines) {
    
    /* result array */
    mxArray* rslt = mxCreateDoubleMatrix(num_beams,1, mxREAL);
    double* p_rslt = mxGetPr(rslt);
    
    /* for iterations */
    mwSize i_line, i_beam;
    /* for intersection-point calculation */
    double denominator = 0;
    /* coordinates of intersection point */
    double x_intersect, y_intersect;
    /* temporal distance storage */
    double dist_obs = 0;
    /* coordinates of a line to consider, for shorter notation */
    double x_line[2], y_line[2];
    /* coordinates of beam's ending point */
    double x_beam_end, y_beam_end;
    /* these are needed rather often an can be precalculated */
    double x_beam_min, y_beam_min, x_beam_max, y_beam_max;
    
    for (i_beam = 0; i_beam < num_beams; i_beam++) {
        
        /* initialize measurement with maxium possible reading */
        double* p_meas = p_rslt + i_beam;
        *p_meas = *(range + 1);
        
        /* Determine end point of beam */
        x_beam_end = x_beam_start + *(range+1)*cos(beam_angles[i_beam]);
        y_beam_end = y_beam_start + *(range+1)*sin(beam_angles[i_beam]);
        
        x_beam_min = min(x_beam_start, x_beam_end);
        y_beam_min = min(y_beam_start, y_beam_end);
        x_beam_max = max(x_beam_start, x_beam_end);
        y_beam_max = max(y_beam_start, y_beam_end);
        
        for (i_line = 0; i_line <num_obs_lines; i_line++) {
            
            /* get obstacle line coordinates */
            x_line[0] = *(obs_lines + i_line);
            x_line[1] = *(obs_lines + i_line + 1*num_obs_lines);
            y_line[0] = *(obs_lines + i_line + 2*num_obs_lines);
            y_line[1] = *(obs_lines + i_line + 3*num_obs_lines);
            
            /* put a rectangular window around the beam. If the obstacle line is completely outside the window, don't consider it */
            if (((x_line[0] < x_beam_min) && (x_line[1] < x_beam_min)) ||
                    ((x_line[0] > x_beam_max) && (x_line[1] > x_beam_max)) ||
                    ((y_line[0] < y_beam_min) && (y_line[1] < y_beam_min)) ||
                    ((y_line[0] > y_beam_max) && (y_line[1] > y_beam_max))) {
                continue;
            }
            
            denominator = ((x_beam_start-x_beam_end)*(y_line[0]-y_line[1])) - ((y_beam_start-y_beam_end)*(x_line[0]-x_line[1]));
            /* are the lines parallel? -> no intersection or another edge connected to this one will yield a result */
            /* TODO This should be dealt with more cleanly, we do have scenarios with single lines where this could be a problem */
            if (denominator == 0) {
                continue;
            }
            
            x_intersect = ((x_beam_start*y_beam_end-y_beam_start*x_beam_end)*(x_line[0]-x_line[1])-(x_beam_start-x_beam_end)*(x_line[0]*y_line[1]-y_line[0]*x_line[1]))/denominator;
            y_intersect = ((x_beam_start*y_beam_end-y_beam_start*x_beam_end)*(y_line[0]-y_line[1])-(y_beam_start-y_beam_end)*(x_line[0]*y_line[1]-y_line[0]*x_line[1]))/denominator;
            
            /* is the point within the boundaries of beam and edge (w/ floating point tolerance)? */
            if  ( (x_beam_min-x_intersect) > TOL || (x_beam_max-x_intersect) < -TOL
                    || (y_beam_min-y_intersect) > TOL || (y_beam_max-y_intersect) < -TOL
                    || (min(x_line[0], x_line[1])-x_intersect) > TOL || (max(x_line[0], x_line[1])-x_intersect) < -TOL
                    || (min(y_line[0], y_line[1])-y_intersect) > TOL || (max(y_line[0], y_line[1])-y_intersect) < -TOL
                    ) {
                continue;
            }
            
            /* if we didn't reach a 'continue' yet, the solution is valid */
            
            /* calculate the distance from the sensor */
            dist_obs = sqrt( pow(x_beam_start - x_intersect, 2) + pow(y_beam_start - y_intersect, 2));
            /* Is the new measurement shorter than the one from previous comparisons? */
            if (dist_obs < *p_meas) {
                *(p_rslt + i_beam) = dist_obs;
                
                /* smallen the window, so that subsequent obstacle lines can be dismissed or computed faster */
                x_beam_end = x_beam_start + *p_meas * cos(beam_angles[i_beam]);
                y_beam_end = y_beam_start + *p_meas * sin(beam_angles[i_beam]);
                x_beam_min = min(x_beam_start, x_beam_end);
                y_beam_min = min(y_beam_start, y_beam_end);
                x_beam_max = max(x_beam_start, x_beam_end);
                y_beam_max = max(y_beam_start, y_beam_end);
                
                /* consider the minimum range */
                if (*(p_rslt + i_beam) < *(range)) {
                    *(p_rslt + i_beam) = *(range);
                }
            }
            
        } /* for every obstacle line */
    }   /* for every beam */
    
    return rslt;
} /* end getBeamMeasurements */

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/*
 * nlhs	Number of output (left-side) arguments, or the size of the plhs array.
 * plhs	Array of output arguments.
 * nrhs	Number of input (right-side) arguments, or the size of the prhs array.
 * prhs	Array of input arguments.
 */
    
    unsigned int num_beams = 0;
    unsigned int num_obs_lines = 0;
    double* x_beam_start;
    double* y_beam_start;
    double* beam_angles;
    double* range;
    double* obs_lines;
    
    if(nrhs != 5) {
        mexErrMsgTxt("5 input arguments required: getBeamMeasurement(x_beam_start, y_beam_start, beam_angles, range, obs_lines)");
    }
    if(nlhs != 1) {
        mexErrMsgTxt("Only one output will be provided.");
    }
    
    /* make sure the first input argument is scalar */
    if( !mxIsDouble(prhs[0]) || mxIsComplex(prhs[0]) || mxGetNumberOfElements(prhs[0])!=1 ) {
        mexErrMsgTxt("x_beam_start must be a scalar.");
    }
    /* make sure the second input argument is scalar */
    if( !mxIsDouble(prhs[1]) || mxIsComplex(prhs[1]) || mxGetNumberOfElements(prhs[1])!=1 ) {
        mexErrMsgTxt("y_beam_start must be a scalar.");
    }
    /* make sure the third input argument is scalar */
    if( !mxIsDouble(prhs[2]) || mxIsComplex(prhs[2]) || mxGetNumberOfElements(prhs[2])<1 ) {
        mexErrMsgTxt("beam_angles must be a vector.");
    }
    /* make sure range is a 2-element row vector */
    if( !mxIsDouble(prhs[3]) || mxIsComplex(prhs[3]) || mxGetN(prhs[3])!=2 ) {
        mexErrMsgTxt("range must be a 2-element row vector of doubles: [rangeMin rangeMax]");
    }
    /* make sure obs_lines is in the correct format */
    if( !mxIsDouble(prhs[4]) || mxIsComplex(prhs[4]) || mxGetN(prhs[4])!=4 ) {
        mexErrMsgTxt("obs_lines should be a Mx4 matrix of M lines with coordinates X1 X2 Y1 Y2]");
    }
    
    x_beam_start = mxGetPr(prhs[0]);
    y_beam_start = mxGetPr(prhs[1]);
    beam_angles = mxGetPr(prhs[2]);
    num_beams = mxGetM(prhs[2]);
    range = mxGetPr(prhs[3]);
    obs_lines = mxGetPr(prhs[4]);
    num_obs_lines = mxGetM(prhs[4]);
    
    plhs[0] = getBeamMeasurements(*x_beam_start, *y_beam_start, beam_angles, num_beams, range, obs_lines, num_obs_lines);
    
    return;
} /* end Gateway-function */


