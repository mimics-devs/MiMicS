function [dists, beam_angles_rel, collision_detected] = ranger(robot, map, robots_edges)
%RANGER Simulates a robot-mounted laser ranger. Also used for collision checks.
%
%   [dists, beam_angles_rel, collision_detected] = ranger(robot, map, robots_edges)
%
% Given a robot's position, dimension and orientation, this function can
% calculate the distances of this robot to obstacles in a similar fashion
% a laser scanner on this robot would.
%       /   .
%      /  .  [-]
%     / .          /
%  [-] - - - -     |
%     \ .          /
%      \  .   [-]
%       \   .
% NOTE: This function uses a MEX-file for the subroutine
% "getBeamMeasurement". At the start of simulation, the system will checked
% if this file was compiled for your system and if not, compile it.
%
% INPUT
% robot - The robot for which to perform the scan, instance of RobotModel.
% map - An array of lines [X1 X2 Y1 Y2] representing the map.
% robots_edges - An array of lines similar to the map, but consisting of
% the edges of the other robots.
%
% OUTPUT
% dists - A vector consisting of one measured distance (in m) per beam,
%   ordered from left to right.
% beam_angles_rel - Beam angles relativ from the robot's orientation.
% collision_detected - 0 if there was no collision, 1 if the robot collided
% with a static obstacle, 2 if it collided with another robot
%
% See also ROBOTMODEL.

%% Configuration

% The minimum and maximum distances the scanner can detect in meters
range = robot.type.ranger_minmax;
% The field-of-view of this scanner in rad, one half spreading to the
% left and one half to the right of the direction the sensor is facing
fov = robot.type.ranger_fov*pi/180;
% The number of beams generated within the fov
beams = robot.type.ranger_num_beams;

%% Calculate beam angles

beam_angles_rel = linspace(fov/2, -fov/2, beams)';
beam_angles_abs = linspace(fov/2 + robot.phi_gt, -fov/2 + robot.phi_gt, beams)';
for i = 1:numel(beam_angles_abs)
    %beam_angles_abs(i) = limitAngleToPi(beam_angles_abs(i));
    if beam_angles_abs(i) > pi
        beam_angles_abs(i) = beam_angles_abs(i) - 2*pi;
    elseif beam_angles_abs(i) < -pi
        beam_angles_abs(i) = beam_angles_abs(i) + 2*pi;
    end
end

% For normal measurements, the map and robot lines (except the ones of
% this robot) can be combined
other_robot_edges = [cat(1, robots_edges{1:robot.id-1}); cat(1, robots_edges{robot.id+1:end})];
obs_lines = [map.lines; other_robot_edges];

% Safety check if there are actually obstacles
if ~numel(obs_lines)
    dists = repmat(range(2), beams, 1);
    collision_detected = 0;
    return;
end

%% Calculate the actual distances
dists = getBeamMeasurements(robot.x_gt, robot.y_gt, beam_angles_abs, range, obs_lines);

% add some gaussian noise
if robot.type.noise_ranger_mean ~= 0 || robot.type.noise_ranger_stddev > 0
    dists = dists + robot.type.noise_ranger_mean + sqrt(robot.type.noise_ranger_stddev)*randn(beams, 1);
end

%% Detect a collision: Simulate ranger beams along the robot's edges

collision_detected = 0;
range(1) = 0;
edges = robots_edges{robot.id};
for i = 1:size(edges,1)
    
    x_beam_start = edges(i,1);
    y_beam_start = edges(i,3);
    x_beam_end = edges(i,2);
    y_beam_end = edges(i,4);
    
    range(2) = sqrt((x_beam_end-x_beam_start)^2 + (y_beam_end-y_beam_start)^2);
    angle = atan2( (y_beam_end-y_beam_start), (x_beam_end-x_beam_start) );
    
    % Test against static obstacles
    if numel(map.lines)
        dist = getBeamMeasurements(x_beam_start, y_beam_start, angle, range, map.lines);
        if dist < range(2)
            collision_detected = 1;
            return
        end
    end
    
    % Test against other robots
    if numel(other_robot_edges)
        dist = getBeamMeasurements(x_beam_start, y_beam_start, angle, range, other_robot_edges);
        if dist < range(2)
            collision_detected = 2;
            return
        end
    end
end

end %function