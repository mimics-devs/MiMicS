function  rslt = MiMicS(varargin)
%
% Copyright (c) 2017, University of Duisburg-Essen, MiMicS All rights
% reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright notice,
% this
%    list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
% notice,
%    this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of swap-ferr nor the names of its
%    contributors may be used to endorse or promote products derived from
%    this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
% IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
% THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
% PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
% CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
% EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
% PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
% PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
% LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
% NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%
% @author Eduardo Ferrera
% @author Merlin Stampa
%
%MIMICS Main entry to the simulator. Does some basic checks and starts the
%main menu.
%
% MiMicS 
% Without arguments, the GUI is started. The following variants with
% arguments serve as shortcuts to functions also accessible via the GUI.
%
% rslt = MiMicS('simulate', list_of_algorithms, list_of_scenarios)
% Calls Simulator.performBatteryOfSimulations(). The result and arguments
% are explained in its own documentation.
%
% MiMicS('scenario_editor')
% Opens an editor to create or redesign scenarios.
%
% MiMicS('map_editor')
% Opens an editor for maps.
%
% rslt = MiMicS('getlog', 'my_alg', 'my_scen')
% Looks for a log folder in the results directory (configurable) and
% returns a struct containing the log data.
%
% rslt MiMicS('replay', 'my_alg', 'my_scen')
% Opens the simulation replayer for a given log from the results folder.
%
% MiMicS('config')
% Opens the configuration GUI.
%
% MiMicS('about')
% Opens a window with information about the developers and legal stuff.

    config = Config.getInstance();
    config.checkDirectories();
    
    % Check if used MEX-files are compiled for this system
    check_mex_compiled('getBeamMeasurements.c');
    
    rslt = 0;
    
    % Checking inputs
    if (nargin > 0)
        cmd = varargin{1};
        
        switch cmd
            case 'simulate'
                if nargin ~= 3
                    error('Please specify a list of algorithms and a list of scenarios. Example call: MiMicS(''simulate'', ''myalg1,myalg2'', ''myscen1,myscen2'')');
                end
                algs = varargin{2};
                scens = varargin{3};
                if ~isstr(algs) || ~isstr(scens)
                    error('Invalid arguments: Algorithms and scenarios must be comma-seperated lists. Example call: MiMicS(''simulate'', ''myalg1,myalg2'', ''myscen1,myscen2'')');
                end
                algs = strsplit(algs, ',');
                scens = strsplit(scens, ',');
                if numel(algs) == 0 || numel(scens) == 0
                    error('You need to select at least one algorithm and one scenario. Seperate multiple algorithms or scenarios with commas.')
                end

                simulator = Simulator.getInstance();
                rslt = simulator.performBatteryOfSimulations(algs, scens);
            case 'map_editor'
                mapEditor();
                rslt = 1;
            case 'scenario_editor'
                stageEditor();
                rslt = 1;
            case 'getlog'
                if nargin ~= 3
                    error ('Please select a result by providing algorithm and scenario name');
                end
                alg_name = varargin{2};
                scen_name = varargin{3};
                
                path_to_log = [config.dir_results filesep alg_name filesep scen_name];
                if ~isdir(path_to_log)
                    error('Log folder %s not found.', path_to_log);
                end
                rslt = readLog(path_to_log);
            case 'replay'
                if nargin ~= 3
                    error ('Please select a result by providing algorithm and scenario name');
                end
                alg_name = varargin{2};
                scen_name = varargin{3};
                
                path_to_log = [config.dir_results filesep alg_name filesep scen_name];
                if ~isdir(path_to_log)
                    error('Log folder %s not found.', path_to_log);
                end
                log = readLog(path_to_log);
                simReplayer(log);
                rslt = 1;
            case 'config'
                menuConfig();
            case 'about'
                about();
        end
    else
        % No arguments specified
        menuMain();
    end
    
end

