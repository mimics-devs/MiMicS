%Proyect: Distributed Navigation System simulator
%Responsable: Eduardo Ferrera 
%Director:    Jesus Capitan 
%Autor:       Eduardo Ferrera 
%Autor:       Merlin Stampa 
%Group:       NES_Robotics
%University: University Duisburg Essen

%Simulation name: AntipodalSquare_Robots_100_DR2R_4
%Number of robots: 100
%Date: 18/11/2015

map_name = 'FourSquares';


%Preallocating Stage
%initial_stage(i=1:N,:)=[x_o(i) y_o(i) phi_o(i) v_o(i) x_g(i) y_g(i)]
initial_stage = zeros(100,6);

initial_stage(1,:)=[20 20 -2.3562 0 -20 -20];
initial_stage(2,:)=[24 20 -2.4469 0 -24 -20];
initial_stage(3,:)=[28 20 -2.5213 0 -28 -20];
initial_stage(4,:)=[32 20 -2.583 0 -32 -20];
initial_stage(5,:)=[36 20 -2.6345 0 -36 -20];
initial_stage(6,:)=[20 24 -2.2655 0 -20 -24];
initial_stage(7,:)=[24 24 -2.3562 0 -24 -24];
initial_stage(8,:)=[28 24 -2.433 0 -28 -24];
initial_stage(9,:)=[32 24 -2.4981 0 -32 -24];
initial_stage(10,:)=[36 24 -2.5536 0 -36 -24];
initial_stage(11,:)=[20 28 -2.191 0 -20 -28];
initial_stage(12,:)=[24 28 -2.2794 0 -24 -28];
initial_stage(13,:)=[28 28 -2.3562 0 -28 -28];
initial_stage(14,:)=[32 28 -2.4228 0 -32 -28];
initial_stage(15,:)=[36 28 -2.4805 0 -36 -28];
initial_stage(16,:)=[20 32 -2.1294 0 -20 -32];
initial_stage(17,:)=[24 32 -2.2143 0 -24 -32];
initial_stage(18,:)=[28 32 -2.2896 0 -28 -32];
initial_stage(19,:)=[32 32 -2.3562 0 -32 -32];
initial_stage(20,:)=[36 32 -2.415 0 -36 -32];
initial_stage(21,:)=[20 36 -2.0779 0 -20 -36];
initial_stage(22,:)=[24 36 -2.1588 0 -24 -36];
initial_stage(23,:)=[28 36 -2.2318 0 -28 -36];
initial_stage(24,:)=[32 36 -2.2974 0 -32 -36];
initial_stage(25,:)=[36 36 -2.3562 0 -36 -36];
initial_stage(26,:)=[-36 20 -0.5071 0 36 -20];
initial_stage(27,:)=[-32 20 -0.5586 0 32 -20];
initial_stage(28,:)=[-28 20 -0.62025 0 28 -20];
initial_stage(29,:)=[-24 20 -0.69474 0 24 -20];
initial_stage(30,:)=[-20 20 -0.7854 0 20 -20];
initial_stage(31,:)=[-36 24 -0.588 0 36 -24];
initial_stage(32,:)=[-32 24 -0.6435 0 32 -24];
initial_stage(33,:)=[-28 24 -0.70863 0 28 -24];
initial_stage(34,:)=[-24 24 -0.7854 0 24 -24];
initial_stage(35,:)=[-20 24 -0.87606 0 20 -24];
initial_stage(36,:)=[-36 28 -0.66104 0 36 -28];
initial_stage(37,:)=[-32 28 -0.71883 0 32 -28];
initial_stage(38,:)=[-28 28 -0.7854 0 28 -28];
initial_stage(39,:)=[-24 28 -0.86217 0 24 -28];
initial_stage(40,:)=[-20 28 -0.95055 0 20 -28];
initial_stage(41,:)=[-36 32 -0.72664 0 36 -32];
initial_stage(42,:)=[-32 32 -0.7854 0 32 -32];
initial_stage(43,:)=[-28 32 -0.85197 0 28 -32];
initial_stage(44,:)=[-24 32 -0.9273 0 24 -32];
initial_stage(45,:)=[-20 32 -1.0122 0 20 -32];
initial_stage(46,:)=[-36 36 -0.7854 0 36 -36];
initial_stage(47,:)=[-32 36 -0.84415 0 32 -36];
initial_stage(48,:)=[-28 36 -0.90975 0 28 -36];
initial_stage(49,:)=[-24 36 -0.98279 0 24 -36];
initial_stage(50,:)=[-20 36 -1.0637 0 20 -36];
initial_stage(51,:)=[20 -36 2.0779 0 -20 36];
initial_stage(52,:)=[24 -36 2.1588 0 -24 36];
initial_stage(53,:)=[28 -36 2.2318 0 -28 36];
initial_stage(54,:)=[32 -36 2.2974 0 -32 36];
initial_stage(55,:)=[36 -36 2.3562 0 -36 36];
initial_stage(56,:)=[20 -32 2.1294 0 -20 32];
initial_stage(57,:)=[24 -32 2.2143 0 -24 32];
initial_stage(58,:)=[28 -32 2.2896 0 -28 32];
initial_stage(59,:)=[32 -32 2.3562 0 -32 32];
initial_stage(60,:)=[36 -32 2.415 0 -36 32];
initial_stage(61,:)=[20 -28 2.191 0 -20 28];
initial_stage(62,:)=[24 -28 2.2794 0 -24 28];
initial_stage(63,:)=[28 -28 2.3562 0 -28 28];
initial_stage(64,:)=[32 -28 2.4228 0 -32 28];
initial_stage(65,:)=[36 -28 2.4805 0 -36 28];
initial_stage(66,:)=[20 -24 2.2655 0 -20 24];
initial_stage(67,:)=[24 -24 2.3562 0 -24 24];
initial_stage(68,:)=[28 -24 2.433 0 -28 24];
initial_stage(69,:)=[32 -24 2.4981 0 -32 24];
initial_stage(70,:)=[36 -24 2.5536 0 -36 24];
initial_stage(71,:)=[20 -20 2.3562 0 -20 20];
initial_stage(72,:)=[24 -20 2.4469 0 -24 20];
initial_stage(73,:)=[28 -20 2.5213 0 -28 20];
initial_stage(74,:)=[32 -20 2.583 0 -32 20];
initial_stage(75,:)=[36 -20 2.6345 0 -36 20];
initial_stage(76,:)=[-36 -36 0.7854 0 36 36];
initial_stage(77,:)=[-32 -36 0.84415 0 32 36];
initial_stage(78,:)=[-28 -36 0.90975 0 28 36];
initial_stage(79,:)=[-24 -36 0.98279 0 24 36];
initial_stage(80,:)=[-20 -36 1.0637 0 20 36];
initial_stage(81,:)=[-36 -32 0.72664 0 36 32];
initial_stage(82,:)=[-32 -32 0.7854 0 32 32];
initial_stage(83,:)=[-28 -32 0.85197 0 28 32];
initial_stage(84,:)=[-24 -32 0.9273 0 24 32];
initial_stage(85,:)=[-20 -32 1.0122 0 20 32];
initial_stage(86,:)=[-36 -28 0.66104 0 36 28];
initial_stage(87,:)=[-32 -28 0.71883 0 32 28];
initial_stage(88,:)=[-28 -28 0.7854 0 28 28];
initial_stage(89,:)=[-24 -28 0.86217 0 24 28];
initial_stage(90,:)=[-20 -28 0.95055 0 20 28];
initial_stage(91,:)=[-36 -24 0.588 0 36 24];
initial_stage(92,:)=[-32 -24 0.6435 0 32 24];
initial_stage(93,:)=[-28 -24 0.70863 0 28 24];
initial_stage(94,:)=[-24 -24 0.7854 0 24 24];
initial_stage(95,:)=[-20 -24 0.87606 0 20 24];
initial_stage(96,:)=[-36 -20 0.5071 0 36 20];
initial_stage(97,:)=[-32 -20 0.5586 0 32 20];
initial_stage(98,:)=[-28 -20 0.62025 0 28 20];
initial_stage(99,:)=[-24 -20 0.69474 0 24 20];
initial_stage(100,:)=[-20 -20 0.7854 0 20 20];
