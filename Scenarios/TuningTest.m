% Defines initial robot positions, orientations, velocities and goals
initial_stage = zeros(2,6);
%Stage(i=1:N,:)=[x_o(i) y_o(i) phi_o(i) v_o(i) x_g(i) y_g(i)]
initial_stage(1,:) = [-3 0     0 0  6.0 0];
initial_stage(2,:) = [ 3 0 3.141 0 -6.0 0];

% Name of a map in the designated maps folder
map_name = 'TwoOpposingWalls';

% Cell array assigning robots to types. First cell in a row is the name of 
% the type, second is the list of robot's using that type. If no type is
% specified, the default is used.
robot_types = cell(1,2);
robot_types{1,1} = 'Pioneer3AT_noise_tuning';
robot_types{1,2} = [1 2];



