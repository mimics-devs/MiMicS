map_name = 'ParallelWalls';

n_robots        = 1; % Number of robots
d_wall_wall     = 6;   % Distance between walls
d_robot_robot   = 1;   % Distance between robots
robots_per_wall = 50;  % Number of robots per wall
x_offset = 1.0;
y_offset = 5.0;

initial_stage = zeros(n_robots,6);
for robot = 1:n_robots
    initial_stage(robot,:) = [x_offset y_offset -pi/2 0 0 0];
    
    % Preparing place for the next robot
    x_offset = x_offset + d_robot_robot;
    if ~mod(robot, robots_per_wall)
        x_offset = 1.0;
        y_offset = y_offset + d_wall_wall;
    end
end
