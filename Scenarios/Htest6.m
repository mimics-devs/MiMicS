%Proyect: Distributed Navigation System simulator
%Responsable: Eduardo Ferrera 
%Director:    Jesus Capitan 
%Autor:       Eduardo Ferrera 
%Autor:       Merlin Stampa 
%Group:       NES_Robotics
%University: University Duisburg Essen

%Number of robots: 6
%Date: 21/10/2015

map_name = 'LetterH';

%Preallocating Stage
%initial_stage(i=1:N,:)=[x_o(i) y_o(i) phi_o(i) v_o(i) x_g(i) y_g(i)]
initial_stage = zeros(6,6);

y_start = 10;
diff_dist = 2;

initial_stage(1,:)=[0.0 -y_start-0*diff_dist +pi/2 0 0.0 +y_start+0*diff_dist];
initial_stage(2,:)=[0.0 -y_start-1*diff_dist +pi/2 0 0.0 +y_start+1*diff_dist];
initial_stage(3,:)=[0.0 -y_start-2*diff_dist +pi/2 0 0.0 +y_start+2*diff_dist];

initial_stage(4,:)=[0.0 +y_start+0*diff_dist -pi/2 0 0.0 -y_start-0*diff_dist];
initial_stage(5,:)=[0.0 +y_start+1*diff_dist -pi/2 0 0.0 -y_start-1*diff_dist];
initial_stage(6,:)=[0.0 +y_start+2*diff_dist -pi/2 0 0.0 -y_start-2*diff_dist];