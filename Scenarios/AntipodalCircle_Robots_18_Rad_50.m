%Proyect: Distributed Navigation System simulator
%Responsable: Eduardo Ferrera 
%Director:    Jesus Capitan 
%Autor:       Eduardo Ferrera 
%Autor:       Merlin Stampa 
%Group:       NES_Robotics
%University: University Duisburg Essen

%Simulation name: AntipodalCircle_Robots_18_Rad_50
%Number of robots: 18
%Date: 1/6/2016

robot_types = cell(1,2);
robot_types{1,1} = 'Pioneer3AT_noise_tuning';
robot_types{1,2} = 1:18;

%Preallocating Stage
initial_stage = zeros(18,6);
%Stage(i=1:N,:)=[x_o(i) y_o(i) phi_o(i) v_o(i) x_g(i) y_g(i)]

initial_stage(1,:)=[46.9846 17.101 3.4907 0 -46.9846 -17.101];
initial_stage(2,:)=[38.3022 32.1394 3.8397 0 -38.3022 -32.1394];
initial_stage(3,:)=[25 43.3013 4.1888 0 -25 -43.3013];
initial_stage(4,:)=[8.6824 49.2404 4.5379 0 -8.6824 -49.2404];
initial_stage(5,:)=[-8.6824 49.2404 4.8869 0 8.6824 -49.2404];
initial_stage(6,:)=[-25 43.3013 5.236 0 25 -43.3013];
initial_stage(7,:)=[-38.3022 32.1394 5.5851 0 38.3022 -32.1394];
initial_stage(8,:)=[-46.9846 17.101 5.9341 0 46.9846 -17.101];
initial_stage(9,:)=[-50 6.1232e-15 6.2832 0 50 -1.2246e-14];
initial_stage(10,:)=[-46.9846 -17.101 6.6323 0 46.9846 17.101];
initial_stage(11,:)=[-38.3022 -32.1394 6.9813 0 38.3022 32.1394];
initial_stage(12,:)=[-25 -43.3013 7.3304 0 25 43.3013];
initial_stage(13,:)=[-8.6824 -49.2404 7.6794 0 8.6824 49.2404];
initial_stage(14,:)=[8.6824 -49.2404 8.0285 0 -8.6824 49.2404];
initial_stage(15,:)=[25 -43.3013 8.3776 0 -25 43.3013];
initial_stage(16,:)=[38.3022 -32.1394 8.7266 0 -38.3022 32.1394];
initial_stage(17,:)=[46.9846 -17.101 9.0757 0 -46.9846 17.101];
initial_stage(18,:)=[50 -1.2246e-14 9.4248 0 -50 1.837e-14];
