%Proyect: Distributed Navigation System simulator
%Responsable: Eduardo Ferrera 
%Director:    Jesus Capitan 
%Autor:       Merlin Stampa
%Group:       NES_Robotics
%University: University Duisburg Essen

map_name = 'Hospital';

% initial_stage consists of 2000 robots, load from this file
load('Benchmark_Hospital.mat');

robot_types = cell(1,2);
robot_types{1,1} = 'Pioneer3AT_Benchmark_Hospital';
robot_types{1,2} = 1:size(initial_stage,1);