map_lines = zeros(4*4,4);

sq_lateral = 12;
dist_sq_to_sq = 8 + sq_lateral;

% Defining a square as a reference
square = zeros(4,4);
square(1,:)=[-sq_lateral/2 -sq_lateral/2 -sq_lateral/2 +sq_lateral/2];
square(2,:)=[-sq_lateral/2 +sq_lateral/2 +sq_lateral/2 +sq_lateral/2];
square(3,:)=[+sq_lateral/2 +sq_lateral/2 -sq_lateral/2 +sq_lateral/2];
square(4,:)=[+sq_lateral/2 -sq_lateral/2 -sq_lateral/2 -sq_lateral/2];

map_lines( 1: 4,:) = square + [+dist_sq_to_sq*ones(4,1) +dist_sq_to_sq*ones(4,1) dist_sq_to_sq*ones(4,1) dist_sq_to_sq*ones(4,1)]/2;
map_lines( 5: 8,:) = square - [+dist_sq_to_sq*ones(4,1) +dist_sq_to_sq*ones(4,1) dist_sq_to_sq*ones(4,1) dist_sq_to_sq*ones(4,1)]/2;
map_lines( 9:12,:) = square + [-dist_sq_to_sq*ones(4,1) -dist_sq_to_sq*ones(4,1) dist_sq_to_sq*ones(4,1) dist_sq_to_sq*ones(4,1)]/2;
map_lines(13:16,:) = square - [-dist_sq_to_sq*ones(4,1) -dist_sq_to_sq*ones(4,1) dist_sq_to_sq*ones(4,1) dist_sq_to_sq*ones(4,1)]/2;