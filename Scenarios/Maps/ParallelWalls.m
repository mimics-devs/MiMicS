% Defining the benchmarking wall map

width        = 52; % Width
height       = 48; % Height
d_wall_wall  = 6;  % Distance between walls
n_hori_walls = 9;  % Number of horizontal walls

% Lateral walls
map_lines(1,:)=[    0     0    0 height];
map_lines(2,:)=[width width    0 height];

for i = 0:(n_hori_walls-1)
    map_lines(end + 1,:)=[0 width i*d_wall_wall i*d_wall_wall];
end