map_lines = zeros(6,4);
%Map(i=1:N,:)=[x_o(i) y_o(i) x_d(i) y_d(i)]

u_center = 2;
u_lateral = 4;

map_lines(1,:)=[-u_center/2 -u_center/2 -u_lateral/2 +u_lateral/2];
map_lines(2,:)=[-u_center/2 +u_center/2 +u_lateral/2 +u_lateral/2];
map_lines(3,:)=[+u_center/2 +u_center/2 -u_lateral/2 +u_lateral/2];

