map_lines = zeros(6,4);

h_center = 2;
h_lateral = 8;

map_lines(1,:)=[-h_center/2 -h_center/2 -h_lateral/2 +h_lateral/2];
map_lines(2,:)=[-h_center/2 +h_center/2 +0 +0];
map_lines(3,:)=[+h_center/2 +h_center/2 -h_lateral/2 +h_lateral/2];