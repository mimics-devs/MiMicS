% Creation date: 18 Jan 2017
% Authors: Eduardo Ferrera, Merlin Stampa
% Institution: University Duisburg Essen, NES Group

% The following scenario is prepared to work as a tunning scenario.
dist_between_walls  =  8;
wall_length         =  2; 
y_offset            =  0;

map_lines = zeros(2,4);
%map_lines(i=1:N,:)=[x_o(i) x_d(i) y_o(i) y_d(i)]
map_lines(1,:) = [+dist_between_walls/2 +dist_between_walls/2 +wall_length/2+y_offset -wall_length/2+y_offset];
map_lines(2,:) = [-dist_between_walls/2 -dist_between_walls/2 +wall_length/2+y_offset -wall_length/2+y_offset];