function [v_ref, phi_ref] = algorithm(robot, scan, comm, ~)
% Distributed propagation algorithm, based on the work:
%
% Vaughan, Richard. "Massively multi-robot simulation in stage." 
% Swarm intelligence 2.2 (2008): 189-208.
% 
% Simple propagation algorithm used in bechmarking scenarios to test the 
% performance of a simulator.

    if (robot.id == 1)
        % Only the robot number 1 measures the computational load
        saveComputationalLoad();
    end

    benchmark_time_limit = 600;                % seconds
    % Check if the benchmark time limit has been reached
    if comm.getTime() >= benchmark_time_limit
        fprintf('The benchmark time limit of %d seconds has been reached, deactivating robot %i.\n', ...
            benchmark_time_limit, robot.id);
        robot.deactivate('Benchmark time limit ended');
        v_ref = 0;
        phi_ref = robot.phi;
        return;
    end

    % Parameters
    v_speed = 0.3;                           % meters per second
    w_gain = 1.0;                            % turn speed gain
    safe_dist = 1;                           % meters (original: 0.5)
    safe_angle = 0.5;                        % radians

    ranger_dists = scan(:,1);
    beam_angles_rel = scan(:,2);
    
    dx = 0;
    dy = 0;
    
    sonar_meas = [100 100 100 100 100 100];  % measurements
    err_angle_abs = [3.14 3.14 3.14 3.14 3.14 3.14];
    sonar_angl = [-50 -30 -10 10 30 50]./180.*pi;   % radians
    
    for i = 1:numel(ranger_dists)
        dx = dx + ranger_dists(i) .* cos(beam_angles_rel(i)); 
        dy = dy + ranger_dists(i) .* sin(beam_angles_rel(i));
        
        % Filling the sonnar measurements
        for j = 1:6
            err = abs(beam_angles_rel(i) - sonar_angl(j));
            if err_angle_abs(j) > err
                err_angle_abs(j) = err;
                sonar_meas(j) = ranger_dists(i);
            end
        end
    end
    
    v_ref = 0;
    phi_ref = robot.phi;
    
    if dx ~= 0 || dy ~= 0
       resultant_angle = atan2(dy, dx);
       
       % Preparing the linear movement
       if abs(resultant_angle) < safe_angle && ...
           sonar_meas(1) > safe_dist/2.0 && ... % -50° original -> /4.0
           sonar_meas(2) > safe_dist/2.0 && ... % -30°
           sonar_meas(3) > safe_dist/1.0 && ... % -10°
           sonar_meas(4) > safe_dist/1.0 && ... % +10°
           sonar_meas(5) > safe_dist/2.0 && ... % +30°
           sonar_meas(6) > safe_dist/2.0        % +50° original -> /4.0
       
           v_ref = v_speed;
       else
           v_ref = 0;
       end
       
       % Preparing the angular movement
       phi_ref = robot.phi + robot.delta_t*w_gain*resultant_angle;
    end
   
end % function

function saveComputationalLoad()
% top -b -d1 -n1|grep -i "Cpu(s)"|head -c21|cut -d ' ' -f2|cut -d '%' -f1

% mpstat | awk '$12 ~ /[0-9.]+/ { print 100 - $12"%" }'
% mpstat -P ALL
    
end