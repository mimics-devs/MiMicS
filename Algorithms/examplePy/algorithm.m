function [v_ref, phi_ref] = algorithm(robot, ~, ~, ~)
% This algorithm uses a custom Python module to recreate the behaviour
% seen in the "example" and "exampleC" algorithms, i.e. letting the robot
% drive towards its goal without any real collision avoidance.
%
% Be aware that you will need to reload the Python module if you make any
% changes to it. How to do this is explained here:
% https://mathworks.com/help/matlab/matlab_external/call-modified-python-module.html
% If this does not help, try restarting MATLAB.
    
    % Add current directory to python path if necessary
    if count(py.sys.path, fileparts(mfilename('fullpath'))) == 0
        insert(py.sys.path,int32(0), fileparts(mfilename('fullpath')));
    end

    % Instantiate Python object in vars structure on first run
    if ~isfield(robot.vars, 'py_collAv')
        robot.vars.py_collAv = py.collAv.CollisionAvoidance;
        robot.vars.py_collAv.setGoal(robot.x_goal, robot.y_goal);
        robot.vars.py_collAv.setVMax(robot.type.v_max);
    end
    
    robot.vars.py_collAv.setPosition(robot.x, robot.y);
    
    v_ref = double(robot.vars.py_collAv.getVRef());
    phi_ref = double(robot.vars.py_collAv.getPhiRef());

    % This is how you can transform the scan data to Python lists
    %ranges = py.list(scan(:,1)');
    %beam_angles_rel = py.list(scan(:,2)');

end