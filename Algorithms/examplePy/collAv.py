import math

class CollisionAvoidance():
    
    # Constructor of the class
    def __init__(self):
        # Parameters
        self.dist_arrival = 0.25
        self.dist_braking = 4*self.dist_arrival;
        self.v_braking_max = 0.7
        self.v_braking_min = 0.15

    # Sets the robot's goal coordinates
    def setGoal(self, x_goal, y_goal):
        self.x_goal = x_goal
        self.y_goal = y_goal
    
    # Sets the maximum velocity
    def setVMax(self, v_max):
        self.v_max = v_max

    # Passes the robot's current coordinates
    def setPosition(self, x, y):
        self.x = x
        self.y = y

    # Get velocity reference (full speed normally, graceful braking near the goal
    def getVRef(self):
        
        distance_to_goal = math.sqrt( pow(self.x_goal - self.x, 2) + pow(self.y_goal-self.y, 2) )
        v_ref = 0

        if (distance_to_goal < self.dist_braking):
            if (distance_to_goal < self.dist_arrival):
                # Reached goal
                v_ref = 0
            else:
                # Approaching goal
                slope = (self.v_braking_max - self.v_braking_min) / (self.dist_braking - self.dist_arrival)
                offset = self.v_braking_max - slope*self.dist_braking
                v_ref = slope*distance_to_goal + offset
        else:
            # Further away
            v_ref = self.v_max

        return v_ref

    # Orients the robot towards the goal
    def getPhiRef(self):
        return math.atan2(self.y_goal - self.y, self.x_goal - self.x)

