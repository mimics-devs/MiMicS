#!/usr/bin/env python
#
# Copyright (c) 2017, University of Duisburg-Essen, multirobot-ferr
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of multirobot-ferr nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
#
# @file dummy_collision_avoidance.py
# @author Eduardo Ferrera
# @version 0.1
# @date    3/4/17
#
# @short: Node that measures the distance to the wall in front of the robot and
# if there is a risk of collision stops the robot.
#
# The scripts drives the robot forwards nomater the direction. If central measurement of the
# laser is shorter than d_stop, the node sends an stop command to the robot.
# If the robot collides with a wall, it sends a true signal throw the collisions topic.

# Deprecated # import rospy                                # Necessary to create a ros node
import math                                 # Necessary to execute ceil()
import sys                                  # Handling exceptions
from geometry_msgs.msg  import Twist        # Message to move the robot
from sensor_msgs.msg    import LaserScan    # Message of the laser
from std_msgs.msg       import Bool         # Message to determine number of collisions

t_loop = 0.1    # Time of the loop

class DummyCollisionAvoidance():

    # Constructor of the class
    def __init__(self):
        # Variables to command velocities to the robot
        # Deprecated # self.cmd_speed_pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
        self.cmd_speed = Twist()

        # Variables to inform from collisions
        # Deprecated # self.inform_collision = rospy.Publisher('/robot_collided', Bool, queue_size=1)
        self.collision = Bool()

        # Determining when to stop and if there was a collision
        # Deprecated # self.laser_sub = rospy.Subscriber('base_scan', LaserScan, self.CbRanger)
        self.stop_flag = False  # Determines if is necessary to stop the robot
        self.d2wall_last = None

        # Variable to determine when to stop
        self.d_stop = None
        if rospy.has_param("/d_stop"):
            self.d_stop = rospy.get_param("/d_stop")
            rospy.loginfo("The robot will stop at %.2fm of the wall", self.d_stop)
        else:
            rospy.logfatal("The value d_stop is not defined")
            sys.exit()

        # Variables to check and warn collisions
        self.collision_check_done = False
        self.d_collision = None
        if rospy.has_param("/d_collision"):
            self.d_collision = rospy.get_param("/d_collision")
        else:
            rospy.logfatal("The value d_collision is not defined")
            sys.exit()

        # Determining when to stop the node
        self.keep_alive_n_loops_more = 10



    # Moves the robot according to the requested speeds
    def MoveRobot(self, x_v, y_v, z_v, roll_v, pitch_v, yaw_v):
        # Preparing the message
        self.cmd_speed.linear.x = x_v
        self.cmd_speed.linear.y = y_v
        self.cmd_speed.linear.z = z_v
        self.cmd_speed.angular.x = roll_v
        self.cmd_speed.angular.y = pitch_v
        self.cmd_speed.angular.z = yaw_v

        # Publishing the message
        self.cmd_speed_pub.publish(self.cmd_speed)
        return

    # Moves the robot unless there is a risk of collision in front of it
    def MoveIfPossible(self):
        if self.stop_flag:
            self.MoveRobot( 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0)
        else:
            self.MoveRobot(10.0, 0.0, 0.0,
                           0.0, 0.0, 0.0)
        return

    # Callback to read the laser and determine when is the robot too close to a wall
    # (It only pays attention to the beam closer to the x axis)
    def CbRanger(self, ranger_msg):
        print ranger_msg.ranges

        # Sometimes the program is not fast enoght to determine the value of d_stop
        if self.d_stop != None:
            # Computing the index closer to the x axis (assuming that is between angle_max and angle_min)
            x_idx = (0.0 - ranger_msg.angle_min)/ranger_msg.angle_increment
            x_idx = int(math.ceil(x_idx))

            d2wall = ranger_msg.ranges[x_idx]

            if (not self.stop_flag):
                # The robot is moving
                if d2wall <= self.d_stop:
                    self.stop_flag = True
            else:
                # The robot is trying to stop
                if self.d2wall_last == d2wall:
                    self.CheckCollision(d2wall)

            self.d2wall_last = d2wall


        # Wasting time with the laser
        for i in range(1000):
            tmp = sum(ranger_msg.ranges)

        return

    # Checks for collisions
    def CheckCollision(self, d2wall):
        if not self.collision_check_done:
            if d2wall <= self.d_collision:
                rospy.logwarn(rospy.get_namespace() + " collided with the wall (wall at %.3fm)", d2wall)
                self.collision.data = True
            else:
                rospy.loginfo(rospy.get_namespace() + " stoped safelly at %.3fm of the wall", d2wall)
                self.collision.data = False

            self.inform_collision.publish(self.collision)

            self.collision_check_done = True

    # Returns true while the system still working
    def Running(self):
        if self.collision_check_done:
            if self.keep_alive_n_loops_more > 0:
                self.keep_alive_n_loops_more -= 1
            else:
                return False
        return True

# Main loop of the node
def MainLoop():
    global t_loop

    dummy_ca = DummyCollisionAvoidance()
    rate = rospy.Rate(1/t_loop) # 10hz
    while not rospy.is_shutdown() and dummy_ca.Running():
        dummy_ca.MoveIfPossible()
        rate.sleep()


if __name__ == '__main__':
    rospy.init_node('dummy_collision_avoidance')
    try:
        MainLoop()
    except rospy.ROSInterruptException:
        pass
