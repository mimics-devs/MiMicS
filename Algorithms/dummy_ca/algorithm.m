function [v_ref, phi_ref] = algorithm(robot, scan, ~, ~)
% Copyright (c) 2017, University of Duisburg-Essen, multirobot-ferr
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% 1. Redistributions of source code must retain the above copyright notice, this
%    list of conditions and the following disclaimer.
% 
% 2. Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution.
% 
% 3. Neither the name of multirobot-ferr nor the names of its
%    contributors may be used to endorse or promote products derived from
%    this software without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
%
% @file algorithm
% @author Eduardo Ferrera
% @version 0.5
% @date    8/4/17
% 
% @short: Node that measures the distance to the wall in front of the robot and
% if there is a risk of collision stops the robot.
% 
% The scripts drives the robot forwards nomater the direction. If central measurement of the
% laser is shorter than d_stop, the node sends an stop command to the robot.
% If the robot collides with a wall, it sends a true signal throw the collisions topic.

    if ~isfield(robot.vars, 'd_stop')
        % Initializing the system
        
        % Standart robot is the Pioneer3AT 0.5m x 0.7m
        % The laser is on the middle so we can set as in stage d_collision
        % to 0.201m
        
        % The laser is in the middle, so we should set d_collision to 0.35
        % but in order to respect the same restrictions of stage we will 
        % set it to 0.351
        % This robot is able to stop in 0.2m so we will set the distance to
        % stop to 0.21+0.35
        robot.vars.d_stop = 0.56;
        robot.vars.d_collision = 0.201;
        
        robot.vars.stop_flag = false;
        robot.vars.d2wall_last = NaN;
        robot.vars.collision_check_done = false;
        robot.vars.keep_alive_n_loops_more = 10;
    end
    
    if ~Running(robot)
        robot.deactivate('Stop requested by the user');
    end

    % Calling the laser callback
    LaserCb(robot, scan)
    
    if robot.vars.stop_flag
        v_ref   = 0.0;
    else
        v_ref   = 1.0;
        robot.vars.last_speed = robot.y;
    end
    phi_ref = robot.phi;
    
end % function


function LaserCb(robot, scan)
% Callback to read the laser and determine when is the robot too close to a wall
% (It only pays attention to the beam closer to the x axis)

    % Computing the index closer to the x axis (assuming that is between angle_max and angle_min)
    x_idx = ceil((0.0 - scan(1,2))/(scan(2,2) - scan(1,2)));   % (0.0 - angle_min)/angle_increment

    d2wall = scan(x_idx,1);

    if ~robot.vars.stop_flag
        % The robot is moving
        if d2wall <= robot.vars.d_stop
            disp(['Stopping the robot at ' num2str(d2wall) 'm of the wall'])
            robot.vars.stop_flag = true;
        end
    else
        % The robot is trying to stop
        if abs(robot.vars.d2wall_last - d2wall) < 0.001     % The robot already stoped
            CheckCollision(robot, d2wall)
        end
        robot.vars.d2wall_last = d2wall;
    end

    % Wasting time with the laser
    for i = [1:1000]
        tmp = sum(scan(:,1));
    end
end

function CheckCollision(robot, d2wall)
% Checks for collisions

    if ~robot.vars.collision_check_done
        if d2wall <= robot.vars.d_collision
            msg = ['robot ' num2str(robot.id) ' collided with the wall (wall at ' num2str(d2wall) 'm)'];
        else
            msg = ['robot ' num2str(robot.id) ' stopped safelly at ' num2str(d2wall) 'm of the wall'];
        end

        % Saving the information in the log file
        disp(msg)
        robot.setLogMsg(msg)

        robot.vars.collision_check_done = true;
    end
end


function Run = Running(robot)
% Returns true while the system still working
    Run = true;
    if robot.vars.collision_check_done
        if robot.vars.keep_alive_n_loops_more > 0
            robot.vars.keep_alive_n_loops_more = robot.vars.keep_alive_n_loops_more -1;
        else
            Run = false;
        end
    end
end
