% Simple scenario generator to test MiMicS under heavy load.

% Parameters
num_scenarios = 98;
map_name = 'Box105x50';
folder = [pwd filesep 'Scenarios' filesep 'dummy_ca' filesep];
robot_init_y = 20;
robot_init_phi = -pi/2;
first_x = -49;             % Starting X-coordinate of the first robots. All others will be placed to the right of this.

% Folder and scenario creation
mkdir(folder);
for i_scenario = 1:num_scenarios
   % Open file
   full_path = [folder 'dummy_ca' sprintf('%03d', i_scenario) '.m'];
   file = fopen(full_path, 'w');
   if file < 0
       error(['Error while trying to open file: ' full_path]);
   end
   
   fprintf(file, 'map_name = ''%s'';\n\n', map_name);
   fprintf(file, 'initial_stage = zeros(%d,6);\n', i_scenario);
   
   % The number of robots within a scenario is equal to the number of the scenario.
   for i_robot = 1:i_scenario
      robot_init_x = first_x + (i_robot-1);
      fprintf(file, 'initial_stage(%d,:) = [%g %g %g %g %g %g];\n', i_robot, robot_init_x, robot_init_y, robot_init_phi, 0, 0, 0);
   end
   
   fclose(file);
   
end
