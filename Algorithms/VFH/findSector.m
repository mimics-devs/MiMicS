%  Calculates in which laserscan sector an angle is contained
%
% currPos: current position of the vehicle
% laPoint: carrot point
%
function sector = findSector(angle, alpha, n)

    sector = round(n/2 +0.5 - angle/alpha);
    
    if sector > n
        sector = n;
    elseif sector < 1
        sector = 1;
    end
end

