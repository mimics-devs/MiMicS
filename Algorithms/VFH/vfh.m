% [angle, stop, no_obstacles] = vfh(laPoint, handles, simMode)
% 
% Makes a vehicle avoid obstacles
%
% laPoint: the goal point [x,y]
% handles: handles to the GUI components
% simMode: 'sim' if simultion, 'mod' if model, 'real' if real vehicle
%
% Returns:
% phi_ref: the angle of the best direction of travel in radians
% stop: 1 if the vehicle gets stuck, else 0
% no_obstacles: 1 if there's no obstacles towards the carrot point, else 0

function [phi_ref, v_ref, stop, no_obstacles] = vfh(robot, ranger_dists, beam_angles_rel)

    % Define parameters
    ws = 4;                         % window size (in meters)
    n = 54;                         % number of sectors
    dmax = sqrt(2)*(ws/2);          % maximum distance of an active cell
    b = 1;                          % magnitude calculation parameter 
    a = b*dmax;                     % magnitude calculation parameter
    
    width_valley_wide = 12;       	% number of sectors a valley must span to be considered wide
    width_valley_narrow = 4;          % width of a narrow valley
    threshold = 6;                  % sectors with magnitude underneath this valley are considered obstacle-free (original: 43)
    mag_fullstop = 15;              % if the smoothed POD in travelling direction exceeds this direction, the robot should make a full stop
    
    fov = robot.type.ranger_fov/180*pi;   % Field of view in radians
    alpha = fov/n;                          % angular difference between two sector borders
    
    % last orientation reference
    persistent prev_angle;
    if isempty(prev_angle)
        prev_angle = robot.phi;
    end
    
    % Initialise output
    stop = 0;
    no_obstacles = 0;
    
    num_beams = length(ranger_dists);
    
    % find the angle between the current orientation and the goal
    la_angle = limitAngleToPi(robot.getGoalOrientation() - robot.phi);
    % get the sector this angle is contained in
    la_sector = findSector(la_angle, alpha, n);
    
    % prepare histogram
    h = zeros(1,n);
    
    % Normally, one would update the histogram cell grid here. 
    % However, we are dealing with static as well as moving obstacles (other robots). 
    % Hence, a constructed grid is useless with every new iteration. 
    % If a laserbeam reports an obstacle within the sliding window, the 
    % calculated cell magnitude is added directly to the polar histogram.
    
    ws_half = ws/2;
    for i = 1:num_beams
        y_delta_dist = ranger_dists(i) * sin(beam_angles_rel(i));
        x_delta_dist = ranger_dists(i) * cos(beam_angles_rel(i));
        
        % is the hit point of the beam inside the sliding window?
        if abs(y_delta_dist) < ws_half && abs(x_delta_dist) < ws_half
            
            % determine the sector
            k = findSector(beam_angles_rel(i), alpha, n);

            % calculate magnitude m_ij = (c_ij)^2 * (a-bd);
            % NOTE: Since we implicitly recontruct the grid from scratch 
            % for every iteration, c (certainty value) is always 1. 
            % d would normally be the distance between the center point of 
            % the cell and the vehicle center point. However, the distance 
            % measured by the laserbeam is just as good if not even more 
            % accurate
            mag = (a - b*ranger_dists(i));
            % add this magnitude to the polar histogram
            h(k) = h(k) + mag;
        end    
    end

    % smooth POD
    hs = hSmooth(h,n);

    % find candidates for travelling direction
    c = [];             % stores indices of candidate sectors
    j = 1;              % counter for c
    k = 1;              % iterator for the beams
    while k <= n
        if(hs(k) < threshold)   % is this sector obstacle-free?
            
            kl = k;             % right border of the valley
            while(k <= n && hs(k) < threshold)
                kr = k;         % left border of a valley 
                k = k + 1;
            end
            
            if(kr-kl > width_valley_wide)       % wide valley
                
                c(j)   = round(kl + width_valley_narrow); % towards the left side
                c(j+1) = round(kr - width_valley_narrow); % towards the right side
                j = j + 2;
                if(la_sector > c(j-2) && la_sector < c(j-1)) % direction to goal is contained in this valley
                    c(j) = la_sector;
                    j = j + 1;
                end		
            elseif(kr-kl > width_valley_narrow) % narrow valley 
                c(j) = round((kr+kl)/2);      % direction in the middle
                j = j + 1;
            end
        else
            k = k + 1;
        end
    end
    
    % "Since there are usually several candidate-valleys, the algorithm 
    % selects the one that most closely matches the direction to the target"       
    
    g = NaN(length(c), 1);          % stores costs of each candiate
    for i=1:length(c)
        % abs(sek1 - sek2) works if field-of-view < 360° (first and last sector are not direct neighbors)
        g(i) =  abs(c(i) - la_sector);
    end
    [~, i] = min(g); 
    
    phi_ref = robot.phi + fov/2 - (c(i)-0.5)*alpha;
 
    if isempty(g) % All directions blocked!
        phi_ref = prev_angle;
        stop = 1;
        v_ref = 0;
    elseif(c(i) == la_sector)
        no_obstacles = 1;
    end
    
    if ~stop
        % get the smoothed magnitude in the direction of travelling when number
        % of beams is even
        if mod(n,2) == 0 
            mag_phi = (h(n/2) + h(n/2 + 1))/2;
            
        else
            mag_phi = h(round(n/2));
        end
        % Reduce speed if there is an obstalce ahead of the robot (large and/or
        % near) and if the steering rate is high
        v_ref = robot.type.v_max * (1 - min(mag_phi, mag_fullstop)/mag_fullstop);
        v_ref = v_ref*max(0, (1-abs(discontAngleDifference(phi_ref,robot.phi))/robot.type.omega_max));
    end
    
    prev_angle = phi_ref;
     
    % DEBUG
%     if robot.id == 1
%         figure(2);
%         subplot(2,1,1);
%         bar(h);
%         title('Polar Obstacle Density');
%         xlim([0 n]);
% 
%         subplot(2,1,2);
%         bar(hs);
%         line([0 n], [threshold threshold], 'Color', 'magenta');
%         line([0 n], [mag_fullstop mag_fullstop], 'Color', 'magenta');
%         line([n/2+0.5 n/2+0.5], [threshold mag_fullstop], 'Color', 'magenta');
%         line([la_sector la_sector], [0 threshold], 'Color', 'cyan');
%         for i=1:length(c)
%             line([c(i) c(i)], [0 threshold], 'Color', 'green', 'LineStyle', '--');
%             text(c(i), (threshold+1), num2str(g(i)));
%         end
%         title('Smoothed POD'); 
%         xlim([0 n]);
%         %disp('DEBUG');
%         pause(1);
%     end

end % function


