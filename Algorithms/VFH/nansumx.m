function y = nansumx(x,d)
%---------------------------------------------------------------------------------

% Removes NaN and computes sum(x,d)
% Set to NaN if ALL elements along d are NaN:
%---------------------------------------------------------------------------------

% Written 1998. Last modified July 23 1999
% Thomas Hellstrom Umea Sweden. Email: thomash@cs.umu.se

if nargin<2
   if size(x,1)>1
      d=1;
   else
      d=2;
   end
end

% NaN->0:
nans = isnan(x);
i = find(nans);
x(i) = zeros(size(i));

% Sum along dimension d:
y = sum(x,d);

% Set sum to 0 if ALL element along d are NaN:
i = find(all(nans,d));
y(i) =  NaN;

return

