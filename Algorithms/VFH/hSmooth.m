% calculates smoothed polar obstacle density 
% h: polar obsticle density
% n: number of sectors
function smooth = hSmooth(h,n)
    % larger l -> more smoothing
    l = 7;
    w=[l l:-1:1 l:-1:1]./(2*l+1); % weight vector

    % Creates a matrix full of NaN
    s = NaN(length(w), n);

    % EDU: Example
    % l= 3, w= [3 3 2 1 3 2 1]/7 n=5    %Note, I will ignore the /7
    % h = [2 1 0 0 0]
    % s=nan*ones(7,5)
    % [ NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN];

    s(1,1:n) = h(1:n).*w(1);
    % Fills the first lines with real values time l
    % [   6   3   0   0   0;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN];

    for i = 2:l+1  
        s(i,i:n) = h(1:n-i+1)*w(i);
    end

    % For i=2,3,4
    % Represents rows but also columns
    % [   6   3   0   0   0;
    %   NaN   x   x   x   x;
    %   NaN NaN   x   x   x;
    %   NaN NaN NaN   x   x;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN;
    %   NaN NaN NaN NaN NaN];
    %
    % h(1:n-i+1) - h = [2 1 0 0 0]
    %       i=2 - h(1:4) - [2 1 0 0]
    %       i=3 - h(1:3) - [2 1 0]
    %       i=4 - h(1:2) - [2 1]


    for i=1:l
        s(l+i+1, 1:n-i) = h(i+1:n).*w(l+i+1);
    end

    smooth = nansumx(s);

end
