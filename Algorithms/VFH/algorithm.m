function [v_ref, phi_ref] = algorithm(robot, scan, ~, ~)
 
    % seperate ranger data
    ranger_dists = scan(:,1);
    beam_angles_rel = scan(:,2);
    
    % Run VFH algorithm, set orientation and speed reference
    [phi_ref, v_ref] = vfh(robot, ranger_dists, beam_angles_rel);
    
    % slower speed if within breaking/arrival distance
    v_ref = min(v_ref, robot.getDesiredVelocity());

end % function