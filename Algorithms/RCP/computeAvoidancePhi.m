function phi_avoidance = computeAvoidancePhi(conflict_angle, robot_phi, ...
    d2conflict,rotation_distance,P_rotation)
% Computes a orientation that allows to surround the obstacle keeping a
% rotation_distance between the robot and the obstacle itself

% Computing the avoidance angle
phi_avoidance_local = conflict_angle - pi/2;

% Computing a P actuation to try to keep a rotation distance
% The system measures the diference between the desired distance and the
% current distance and tryes to minimize such error.
act = P_rotation*(d2conflict-rotation_distance);

% Adding such actuation to the avoidance angle
phi_avoidance_local = phi_avoidance_local + act;

% The following avoidance angle is an increment of the current angle.
phi_avoidance = phi_avoidance_local + robot_phi;

end