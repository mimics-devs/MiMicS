function [conflict_idx, conflicts_detected] = discriminateConflicts(Rrd, polar_obst)
% The system receives a the polar histogram with the distances to the
% sourrounding obstacles and the Rrd, and detects all the obstacles that
% produces a conflict.
%
% Note: To produce a clonflict the obstacle should have in tuch one part of
% his Rrd with other obstacle's Rrd.
% Note: We assume that all robots are equal, so Rrd are the same for all of
% them.

% One of the Rrd for the robot, and one to inflate the other obstacles.
polar_obst=polar_obst-2*Rrd;

% Every negative value is a conflict
conflict_idx = find(polar_obst <= 0);

% Checking if there is at least one conflict detected
if (isempty(conflict_idx))
    conflicts_detected=false;
else
    conflicts_detected=true;
end

end