function [v_ref, phi_ref] = algorithm(robot, ~, comm, ~)
    
    % Constants of the orientation system
    pos_noise = 0; 
            % Maximum posible noise on the positioning system
    alpha_sd = 1.05;
            % Number larger than 1 that allows to include some more safety 
            % space in the Safety disk
    
    radius_safety = alpha_sd * (robot.type.safety_radius + pos_noise);
            % Rsd: Safety disk, area where the robot should be located
            % Note: safety_radius = sqrt(sum[(robot.type.vehicle_width/2)^2 (robot.type.vehicle_length/2)^2])  
            
            % A stoping simulation showed the follow results
            % Starting the stop at (5.3173,1.9951) Vmax=1
            % Finalizing the stop at (5.8114,19951) V=0.002
            %   Stopping distance: 0.4941
            % 
            % The parameters of the robot are 
            % Speed/Traction dynamic (a transfer function)
            % sptr_dyn = ss(tf(1, [0.5 1 0]))
            % Steering dynamic (a transfer function)
            % steer_dyn = ss(tf(1, [0.2 1]))
            % Parameters [Kp Ki Kd] for PID-control of v
            % Values computed with pidtool(sys,'pid') [Response time=0.08899s Transient behavior=0.9 ]
            % v_pid = [22.4749 7.0996 11.2515]
            % Parameters [Kp Ki Kd] for PID-control of phi
            % phi_pid = [20 0 3];
            % maximum translational velocity in m/s
            % v_max = 1
            % maximum rotational velocity in rad/s
            % omega_max = 1
    stop_distance = 0.4941;           %[m]
    loop_period = 0.1;                %[s] = 1/[Hz]
    speed_max = robot.type.v_max;      %[m/s]
    
    speed_rendezvous = 0.6*speed_max; %[m/s]
    % Note: If the speed is too fast, the system switches fast between
    % rendesvouz and rencontre, forcing a 011001010101010 undesired
    % behavior.
    
    
    radius_reserved = speed_max*loop_period + stop_distance + radius_safety;
            % Rrd: Reserved disk, area where the robot should be able to
            % stop.
            
            % To stop the robot requires the reaction time and the stop
            % distance. Rrd should be a ring that surrounds the Safety disk
            % Note that the selected parameters should not be increased
            % since the robot will not only stop but also turn at the same
            % time in most of the cases (only changing from free to blocked 
            % can force this).
            
    alpha_s = 4;
            % Number larger than 2. Allows to define how big should Rs be
            % in comparaison with Rrd. A large number forces to compute
            % obstacles that are far away from the robot. 
    
    radius_sense = alpha_s*radius_reserved;  
            % Sensing radius: Obstacles that are farer away from the 
            % robot than this value are not properly sensed or ignored
    
    
    num_divisions = 360;  % Number of divisions of the polar_obst histogram. 
              % (Larger values perform better accuracies but increases the computational load)
              
    phi_max_err = pi/15;
            % Maximum error allowed while turning in the RENCONTRE state
            
    %%% IMPROVEMENT %%%
    rotation_distance = radius_reserved - radius_safety;
            % The system will try to keep this distance while sourrounding
            % other obstacles
    p_rotation = 0.2;
            % Acts as a P controler to try to keep the distance while
            % sourrounding.
            
    angle_of_view_for_ignore = pi/2;
            % Instead of ussing all navigable area to look for the goal in
            % order to define the obstacle ignorable or not, looks only on
            % the angle that starts on the avoidable orientation and
            % increases in a clockwise direction until the mentioned value.
            % This allows to deal with the wall-convex conflict.
            
            % Note: to return to the old system, the values should be set
            % to P_rotation= 0; angle_of_view_for_ignore = pi;
    %%%%%%%%%%%%%%%%%%%

    % Constants of the speed system
    global dist_arrival;
            % Once the robot is at less than this distance, it considers
            % itself on the goal.
    dist_braking = 3*dist_arrival;
            % At this distance, the robot starts to slowdown its speed.
    MaxVbreaking = 0.7*speed_max;
    MinVbreaking = 0.0*speed_max;

                   
    % Defining all possible states of the state machine for the orientation
    STATE_MACHINE_ORIENTATION = struct('FREE',0,'RENDEZVOUS',1,'RENCONTRE',2,'BLOCKED',3);
            % FREE: There are no obstacles on the system, or can be ignored
            % RENDEZVOUS: A non ignorable obstacle is meet. The robot
            % should stop and surround it in a counterclockwise direction.
            % RENCONTRE: The robot is prepared to afford the obstacle.
            % Start to sorround it.
            % BLOCKED: There are many obstacles sourrounding the robot. It
            % has to keep his position until new order.
            
    % Defining all possible states of the state machine for the speed
    STATE_MACHINE_SPEED = struct('TRAVELING2GOAL_FREE',0,'ARRIVING2GOAL',1,'ONGOAL',2);
            % State machine transitions controller
            % The state machine is composed by three states controled by the
            % distance to the goal
            %
            % |----TRAVELING2GOAL_FREE----+----ARRIVING2GOAL----+---ONGOAL----+
            %                         Dbreaking             dist_arrival        Goal
            %       Max Speed                   Smooth speed        Stop
    
    
    % Computing the necessary reference to arrive to the goal without care
    % about conflicts
    phi_free_ref = robot.getGoalOrientation();
    v_ref = speed_max;    
    
    % Remembering the last state of the state machine
    if (isfield(robot.vars,'status_orientation'))
        status_orientation = robot.vars.status_orientation;
    else
        status_orientation = STATE_MACHINE_ORIENTATION.FREE;
    end

    
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % PREPARING THE WORK FOR THE STATE MACHINE IN ORIENTATION (Start)

    % Prepares the polar_obst histogram
    theta_incr = 2*pi/num_divisions;
    theta = pi:-theta_incr:-pi;
    polar_obst = radius_sense*ones(size(theta));
    
    % Filling the polar_obst histogram with the communications information
    polar_obst = communicationsObstacleDetection(radius_sense, polar_obst, theta, ...
        robot.x, robot.y, robot.phi, comm);
    
    % Debug
    % if (robot.id==1)
    %    plot(theta,polar_obst)
    % end
    
    % TODO: INCLUDE THE INFORMATION OF THE LASER
    
    % TODO: FILTER THE INFORMATION WITH OLD SCANS
    
    [conflicts_idx, conflicts_detected] = discriminateConflicts(radius_reserved, polar_obst);  
    
    % Debug
    % if (robot.id==1 && conflicts_detected)
    %     plot(theta,binary_polar_conflicts);
    % end

    if (conflicts_detected)
        % Checking if there is a solution available
        [solution_available, conflict_l2surround_idx, confligt_r2surround_idx] = ...
            checkForSolutions(conflicts_idx, theta, robot.id);
        
        if (solution_available)
            % Checks if the conflict is ignorable
            if (status_orientation == STATE_MACHINE_ORIENTATION.FREE)
                conflict_ignorable = checkForIgnorableConflicts( ...
                    theta(conflict_l2surround_idx), theta(confligt_r2surround_idx), ...
                    phi_free_ref - robot.phi, phi_max_err, 2*pi, robot.id);
            else
                conflict_ignorable=checkForIgnorableConflicts( ...
                    theta(conflict_l2surround_idx), theta(confligt_r2surround_idx), ...
                     phi_free_ref - robot.phi, phi_max_err, angle_of_view_for_ignore, robot.id);
            end
        
            if (~conflict_ignorable)
                % Computes the phi_avoidance that allows the robot to surround
                % the obstacle.
                phi_avoidance = computeAvoidancePhi( ...
                    theta(conflict_l2surround_idx), robot.phi, ... 
                    polar_obst(conflict_l2surround_idx), rotation_distance, p_rotation);
            end
        end
    end
    
    
    % PREPARING THE WORK FOR THE STATE MACHINE IN ORIENTATION (End)
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    
    % % % % % % % % % % % % % % % % % % % % % % % 
    % COMPUTING TRANSITIONS IN ORIENTATION (Start)
    
    % Defining the default status as free
    status_orientation = STATE_MACHINE_ORIENTATION.FREE;
    
    % Changing the status of the state machine according to the situation
    if (conflicts_detected)
        % The state machine has to transit
        
        if (solution_available)
            % Exists a solution for the conflict
            if (conflict_ignorable)
                % The solution is ignorate the conflict
                status_orientation = STATE_MACHINE_ORIENTATION.FREE;
            else
                % The solution is surround the conflict
                status_orientation = STATE_MACHINE_ORIENTATION.RENCONTRE;
                
                if ( abs(angleDifference(robot.phi, phi_avoidance)) < phi_max_err)
                    % The robot is prepared to surround the conflict
                    status_orientation = STATE_MACHINE_ORIENTATION.RENDEZVOUS;
                end
            end
        else
            % There is a conflict without solution
            status_orientation = STATE_MACHINE_ORIENTATION.BLOCKED;
        end
    end
    
    % Saving the current status
    robot.vars.status_orientation = status_orientation;
    
    % COMPUTING TRANSITIONS IN ORIENTATION (End) 
    % % % % % % % % % % % % % % % % % % % % % % 
    

    % % % % % % % % % % % % % % % % % % % % % % 
    % COMPUTING ACTUATION IN ORIENTATION (Start)
    % Note: The actuation in speed should be already computed
    
    switch status_orientation
        case STATE_MACHINE_ORIENTATION.FREE
            % The system can travel at following the free reference to the
            % goal
            v_ref = min([speed_max robot.getDesiredVelocity()]);
            phi_ref=phi_free_ref;
            
        case STATE_MACHINE_ORIENTATION.RENCONTRE
            % The system should stop and turn in the specified reboundant
            % orientation
            v_ref = 0;
            phi_ref = phi_avoidance;
            
        case STATE_MACHINE_ORIENTATION.RENDEZVOUS
            % The system should turn while keeps the specified reboundant
            % orientation
            
            % Uses as a reference on speed the lowest reference possible.
            if (speed_rendezvous < v_ref)            
                v_ref = speed_rendezvous;
            end
            
            phi_ref = phi_avoidance;
            
        case STATE_MACHINE_ORIENTATION.BLOCKED
            % The system should keep its position and orientation until new
            % order.
            phi_ref = robot.phi;
            v_ref = 0;
    end
    
    % COMPUTING ACTUATION IN ORIENTATION (End)
    % % % % % % % % % % % % % % % % % % % % % 
    
%     % debug
%     if (robot.id == 3)
%         DataRepresentation(vRef, 'vref');
%     end
%     
%     SegmentedSpaceRepresentation(robot, conflicts_idx, theta, polar_obst);
%     
%     
%     % debug
%     % Plots the a circle arround the robot with colors according to his
%     % state machine.
%     debug_plot_circle(robot.x,robot.y,Rsd,'b',12);
%     switch status_orientation
%         case STATE_MACHINE_ORIENTATION.FREE
%             debug_plot_circle(robot.x,robot.y,Rrd,'g',12);
%         case STATE_MACHINE_ORIENTATION.RENCONTRE
%             debug_plot_circle(robot.x,robot.y,Rrd,'c',12);
%         case STATE_MACHINE_ORIENTATION.RENDEZVOUS
%             debug_plot_circle(robot.x,robot.y,Rrd,'b',12);
%         case STATE_MACHINE_ORIENTATION.BLOCKED
%             debug_plot_circle(robot.x,robot.y,Rrd,'m',12);
%     end
%     % debug_plot_circle(robot.x,robot.y,Rs,'y',12);
    

end % function


function debug_plot_circle(x, y, radius, color, points)
% This functions draws a circle arround the point (x,y) with the size
% radius. The circle is an approximation of points.
% It is used to draw while debuging the Rsafety disk and the Rreserved disk

% Changing to plot in the main window
subplot(5,5,[1:4 6 11 16 21])
hold on

ang = -pi:(2*pi)/points:pi;

x_cir = radius*cos(ang) + x;
y_cir = radius*sin(ang) + y;

plot(x_cir, y_cir, color)
hold off

% Allowing other graphs to work
subplot(5,5,[20 25]);
drawnow;

end


function DataRepresentation(var,title_str)
% Represents a data in a plot

MaxData = 150;

persistent Data;
persistent i;

if isempty(i)
    if (isvector(var))
        Data = zeros(MaxData,length(var));
        i=1;
    else
        error('Data representation can not afford a matrix of information');
    end
end

if i >= MaxData
    i = 0;
end

for j=1:length(var)
    Data(i,j)=var(j);
end
i=i+1;

plot(Data)
title(title_str)

end


function SegmentedSpaceRepresentation(robot, conflicts_idx, theta, polar_obst)
    % Changing to plot in the main window
    subplot(5,5,[1:4 6 11 16 21])
    hold on
    
    for i=1:length(conflicts_idx)
        % Checking for the contact point
        ang = theta(conflicts_idx(i)) + robot.phi;
        d = polar_obst(conflicts_idx(i)) / 2;
        
        xc = robot.x + d*cos(ang);
        yc = robot.y + d*sin(ang);
        
        rx = xc;
        ry = yc;
        
        % Increasing points to form a rect
        for j=-10:0.1:10
            rx = [rx xc+j*cos(ang+pi/2)];
            ry = [ry yc+j*sin(ang+pi/2)];
        end
        
        plot(rx,ry,'r');
    end
    
end