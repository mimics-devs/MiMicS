function [v_ref, phi_ref] = algorithm(robot, scan, comm, map)

% This instance of the algorithm function serves as a basic example about
% the usage of C/C++ code in MATLAB.
%
% A MEX file lets you call a C function from MATLAB. The full documentation
% can be found at:
% https://mathworks.com/help/matlab/write-cc-mex-files.html
%
% In short, your C source needs to include the mex.h header, and a
% function called mexFunction(), which serves as the entry point or gateway 
% into your program instead of the common main(). It can pass arguments
% between MATLAB and your C code.
% How to write such a C source MEX file is explained in
% https://mathworks.com/help/matlab/matlab_external/standalone-example.html
%
% After it is written, the source must be compiled to a MEX file.
%
% In this example, we will use C code to make the robot move towards the
% goal without any collision avoidance, just as in the other example.

    % This will check if the source was properly compiled for your system
    % and also detects if you updated the source since the last
    % compilation.
    check_mex_compiled('getRef.c');
    
    % Calls mexFunction() inside the compiled getRef.mex*
    [v_ref, phi_ref] = getRef(robot.x, robot.y, [robot.x_goal robot.y_goal]);

end % function