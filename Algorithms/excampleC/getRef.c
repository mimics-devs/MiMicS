#include <stdio.h>
#include <math.h>
#include "mex.h"

/* Own function to calculate velocity reference */
double getVRef(double x, double y, double* goal) {
    
    double x_goal = *(goal);
    double y_goal = *(goal+1);
    
    double distance_to_goal = sqrt( pow(x_goal - x, 2) + pow(y_goal - y, 2));
    double dist_arrival = 0.25;
    double dist_braking = 4*dist_arrival;
    double v_braking_max = 0.7;
    double v_braking_min = 0.15;
    double v_ref = 0;
    double offset;
    double slope;
    
    if (distance_to_goal < dist_braking) {
        if (distance_to_goal < dist_arrival) {
            /* Reached goal */
            v_ref = 0;
        } else {
            /* Approaching goal */
            slope = ( v_braking_max - v_braking_min )/( dist_braking - dist_arrival);
            offset = ( v_braking_max - slope*dist_braking );
            v_ref = slope*distance_to_goal + offset;
        }
    } else {
        /* further away */
        v_ref = 1;
    }
            
    return v_ref;
}

/* Own function to calculate orientation reference */
double getPhiRef(double x, double y, double* goal) {
    
    double x_goal = *(goal);
    double y_goal = *(goal+1);
    
    return atan2( (y_goal-y), (x_goal-x) );
}

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/*
nlhs	Number of output (left-side) arguments, or the size of the plhs array.
plhs	Array of output arguments.
nrhs	Number of input (right-side) arguments, or the size of the prhs array.
prhs	Array of input arguments.
*/
    
  double* v_ref;
  double* phi_ref;
  double* x;
  double* y;
  double* goal;

  /* Validate number of input/output arguments */
  if(nrhs != 3) {
    mexErrMsgTxt("3 input arguments required: getRef(x, y, goal)");
  }
  if(nlhs != 2) {
  	mexErrMsgTxt("Two output arguments (v_ref, phi_ref) will be provided.");
  }
  
  /* Validate input */
  if( !mxIsDouble(prhs[0]) || mxIsComplex(prhs[0]) || mxGetNumberOfElements(prhs[0])!=1 ) {
    mexErrMsgTxt("x must be a scalar.");
  }
  if( !mxIsDouble(prhs[1]) || mxIsComplex(prhs[1]) || mxGetNumberOfElements(prhs[1])!=1 ) {
    mexErrMsgTxt("y must be a scalar.");
  }
  if( !mxIsDouble(prhs[2]) || mxIsComplex(prhs[2]) || mxGetN(prhs[2])!=2 ) {
    mexErrMsgTxt("goal must be a 2-element row vector of doubles: [x_goal y_goal]");
  }
  
  /* Assign input */
  x = mxGetPr(prhs[0]);
  y = mxGetPr(prhs[1]);
  goal = mxGetPr(prhs[2]);
  
  /* Prepare output */
  plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
  plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);
  v_ref = mxGetPr(plhs[0]);
  phi_ref = mxGetPr(plhs[1]); 
  
  /* Do the actual calculations */
  *v_ref = getVRef(*x, *y, goal);
  *phi_ref = getPhiRef(*x, *y, goal);
  
  return;
} /* end Gateway-function */


