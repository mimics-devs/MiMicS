function [v_ref, phi_ref] = algorithm(robot, scan, ~, ~)
    
    % seperate ranger data
    ranger_dists = scan(:,1);
    beam_angles_rel = scan(:,2);

    % Run VFH+ algorithm, set orientation and speed reference
    [phi_ref, v_ref] = vfhplus(robot, ranger_dists, beam_angles_rel);
    
    % NOTE: The cost calculation part of VFH+ only works properly if the
    % field of view of the ranger is <360°. This is the case for the robot
    % type we tested this algorithm with.
    
    % slower speed if within breaking/arrival distance
    v_ref = min(v_ref, robot.getDesiredVelocity());
    
    % For practical applications, at this point the robot could
    % determine a more precise phi_ref if the robot is within breaking
    % distance to the gaol and no obstacles are present. 
    % The VFH only outputs approximate angles to the goal 
    % (the sectors are discrete).
    % %phi_ref = atan2((y_goal-y_current),(x_goal-x_current));
    % This is not done here in order to stay more accurate to the
    % paper, which didn't mention such a correction.
    
end % function