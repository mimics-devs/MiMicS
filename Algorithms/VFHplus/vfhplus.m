% [phi_ref, v_ref, stop, noObstacles] = vfhplus(robot, laserscan, beam_angles)
% 
% Makes a vehicle avoid obstacles
%
% robot: The current vehicle
% laserscan: Measured distances of each beam
% bean_angles: The (absolute) angles in which the laserbeams are facing
%
% Returns:
% phi_ref: the angle of the best direction of travel in radians
% v_ref: The velocity reference
% stop: 1 if the vehicle gets stuck, else 0
% noObstacles: 1 if there's no obstacles towards the carrot point, else 0

function [phi_ref, v_ref] = vfhplus(robot, laserscan, beam_angles_rel)

    % Define parameters
    ws = 4;                                     % window size in meters 
    n = 54;                                     % number of sectors
    b = 1;                                      % magnitude calculation parameter         
    a = 1 + b*((ws/2)^2);                       % magnitude calculation parameter 
    width_valley_wide = 12;                     % number of sectors a valley must span to be considered wide
    width_valley_narrow = 5;                    % width of a narrow valley
    threshold_low = 10;                         % mag below this: sector free
    threshold_high = 20;                        % mag above: sector blocked
    mag_fullstop = 35;                          % if the smoothed POD in travelling direction exceeds this direction, the robot should make a full stop
    r_min_steer_radius = robot.type.min_steer_radius(robot.v);  % minimum steering radius
    
    % cost constants
    my1 = 5; 
    my2 = 2; 
    my3 = 2;
    
    % to factor in the enlargement of obstacles for the masked polar
    % histogram, simply enlarge the minimum steering radius
    r_min_steer_radius = r_min_steer_radius + robot.type.safety_radius;
    
    fov = robot.type.ranger_fov/180*pi;   % Field of view in radians. NOTE: This implementation assumes fov < 360° at some points
    alpha = fov/n;                              % angular difference between two sector borders.        
    
    % last orientation reference
    persistent prev_sector;
    if isempty(prev_sector)
        prev_sector = n/2;
    end
    
    num_beams = length(laserscan);
    
    % find the angle between the current orientation and the goal
    la_angle = limitAngleToPi(robot.getGoalOrientation() - robot.phi);
    % get the sector this angle is contained in
    la_sector = findSector(la_angle, alpha, n);

    % prepare polar obstacle density histogram (POD)
    h = zeros(1,n);
    
    ws_half = ws/2;
    % For the masked polar histogram: 
    % Consider the left trajectory center first
    x_delta_center = -r_min_steer_radius * sin(robot.phi);
    y_delta_center = r_min_steer_radius * cos(robot.phi); 
    left_tc = 1;
    kl = 0;
    kr = n+1;
    
    for i = 1:num_beams
        
        delta_dist_x = laserscan(i) * cos(beam_angles_rel(i) - robot.phi);
        delta_dist_y = laserscan(i) * sin(beam_angles_rel(i) - robot.phi);
        
        % is the hit point of the beam inside the sliding window?
        if abs(delta_dist_y) < ws_half && abs(delta_dist_x) < ws_half
            
            % new for the VFH+ : get enlargement angle
            gamma = asin(robot.type.safety_radius/laserscan(i));
            
            for k = 1:n % for every sector
                % relative angle of the sector
                angle = fov/2 - (k-1)*alpha;
                if angle > beam_angles_rel(i)-gamma && angle < beam_angles_rel(i)+gamma
                    mag = (a - b*laserscan(i)^2);
                    % add this magnitude to the polar histogram
                    h(k) = h(k) + mag;
                end
            end   
        end
        
        % if we have moved over to the right-hand side...
        if i > num_beams/2 && left_tc
            % then consider the right trajectory center
            x_delta_center = r_min_steer_radius * sin(robot.phi);
            y_delta_center = -r_min_steer_radius * cos(robot.phi);
            left_tc = 0;
        end
        
        % is the hitpoint within the steering circle?
        if (delta_dist_x - x_delta_center)^2 + (delta_dist_y - y_delta_center)^2 <= r_min_steer_radius^2
            
            % if so find determine the sector and remember
            k = findSector(beam_angles_rel(i), alpha, n);
            if left_tc
                kl = k;
            elseif kr == -1
                kr = k;
            end
        end
    end
    
    %update binary polar histogram
    if ~isfield(robot.vars, 'binary_polar_histogram')
        robot.vars.binary_polar_histogram = zeros(n, 1);
    end
    for k = 1:n
        if h(k) > threshold_high
            robot.vars.binary_polar_histogram(k) = 1;    % blocked
        elseif h(k) < threshold_low
            robot.vars.binary_polar_histogram(k) = 0;    % free
        end
    end
    
    % create the masked polar histogram (MPH)
    hm = robot.vars.binary_polar_histogram;
    hm(1:kl) = 1;
    hm(kr:n) = 1;
        
    % collect orientation candidates
    c = [];             % storage for orientation candidates
    j = 1;              % counter for c
    k = 1;              % counter for the beams
    while k <= n
        
        if(hm(k) == 0)   % is this sector obstacle-free?
            
            kl = k;             % left border of the valley
            while(k <= n && hm(k) == 0)
                kr = k;         % right border of a valley 
                k = k + 1;
            end
            
            % calculate candidate directions:
            if(kr-kl > width_valley_wide)
                c(j)   = round(kl + width_valley_wide/2); % towards the left side
                c(j+1) = round(kr - width_valley_wide/2); % towards the right side
                j = j + 2;
                if(la_sector > c(j-2) && la_sector < c(j-1)) % direction to goal is contained in this valley
                    c(j) = la_sector;
                    j = j + 1;
                end		
            elseif(kr-kl > width_valley_narrow) % narrow valley 
                c(j) = round((kr+kl)/2);      % direction in the middle
                j = j + 1;
            end
        else
            k = k + 1;
        end
    end
             
    g = NaN(length(c), 1);          % stores costs of each candiate

    % calculate costs
    for i=1:j-1 
                % abs(sek1 - sek2) works if field-of-view < 360° (first and last sector are not direct neighbors)
        g(i) =    my1*abs(c(i) - la_sector) ...                       % deviation from goal direction
                + my2*abs(c(i) - (n/2+0.5)) ...                       % deviation from current orientation
                + my3*abs(c(i) - prev_sector);                        % deviation from last orientation reference   
    end
    [~, i] = min(g); % Select the valley nearest to the goal direction
    
    phi_ref = robot.phi + fov/2 - (c(i)-0.5)*alpha;
    prev_sector = c(i);
    
    stop = 0;
    if isempty(g) % All directions blocked!
        phi_ref = robot.phi;
        stop = 1;
        v_ref = 0;
    end
    
    if ~stop
        % get the smoothed magnitude in the direction of travelling when number
        % of beams is even
        if mod(n,2) == 0 
            mag_phi = (h(n/2) + h(n/2 + 1))/2;
        else
            mag_phi = h(n/2);
        end
        % Reduce speed if there is an obstalce ahead of the robot (large and/or
        % near) and if the steering rate is high
        v_ref = robot.type.v_max * (1 - min(mag_phi, mag_fullstop)/mag_fullstop);
        v_ref = v_ref*max(0, (1-abs(discontAngleDifference(phi_ref,robot.phi))/robot.type.omega_max));
    end
    
    %prev_angle = phi_ref;
    
    % DEBUG
%     if robot.id == 1
%         figure(2);
%         subplot(3,1,1);
%         bar(h);
%         title('Polar Obstacle Density');
%         line([0 n], [threshold_low threshold_low], 'Color', 'magenta');
%         line([0 n], [threshold_high threshold_high], 'Color', 'magenta');
%         line([0 n], [mag_fullstop mag_fullstop], 'Color', 'red');
%         line([n/2+0.5 n/2+0.5], [threshold_low mag_fullstop], 'Color', 'magenta');
%         xlim([0 n]);
% 
%         subplot(3,1,2);
%         bar(robot.vars.binary_polar_histogram);
%         title('Binary Polar Histogram'); 
%         xlim([0 n]);
%         
%         subplot(3,1,3);
%         bar(hm);
%         for i=1:length(c)
%             line([c(i) c(i)], [0 1], 'Color', 'green', 'LineStyle', '--');
%             text(c(i), 1.1, num2str(g(i)));
%         end
%         line([la_sector la_sector], [0 1], 'Color', 'cyan');
%         title('Masked Polar Histogram');
%         xlim([0 n]);
%         
%         pause(1);
%     end
    
end % function


