function conflict_ignorable = checkForIgnorableConflicts( ...
    left_conflict_angle, right_conflict_angle, goal_orientation, ...
    phi_max_err, angle_of_view_for_ignore, robot_id)
% The system checks according to the angles the following things:
%   if the robot can skip safely the in case that the conflict is ignorable
%       That happens only if the robot is looking on the navigable area.
%   if the goal is on the navigable area
%   if the goal is visible according to the angle_of_view_for_ignore of the
%   robot.
%
%   If all conditions are reached, the function returnes true.
%
% Note: goal_orientation shold be referntiated to the origin of coordinates
% of the robot, and not to the map.
%   Positive values means that the goal is on the left of the robot.
% Note: The robot_id is used only for debug purposes

% Assumes by default that the conflict is not ignorable
conflict_ignorable = false;

left_conflict_avoidance_angle = left_conflict_angle - pi/2;
right_conflict_avoidance_angle = right_conflict_angle - pi/2;

% Notes: 
% if angle_difference(0,left_conflict_avoidance_angle) < 0 
%   The robot has the left_obstacle on his left side
% end
% if angle_difference(0,left_conflict_avoidance_angle) < 0 &&
%   angle_difference(right_conflict_avoidance_angle,0) > 0
%   The robot is looking on the direction of the free zone
% end

if angleDifference(0, left_conflict_avoidance_angle) < +phi_max_err && ...
    angleDifference(right_conflict_avoidance_angle, 0) > -phi_max_err
    % If the scape exists, the robot can take it without risks.
    robot_can_ignore = true;
else
    % If the robot tries to move free using this direction, it will crash
    robot_can_ignore = false;
end

% Notes:
% if angle_difference(goal_orientation, left_conflict_avoidance_angle) < 0
%   The goal is in a clockwise position from the orientation that allows to
%   afford the obstacle on the left
% end
% if angle_difference(right_conflict_avoidance_angle, goal_orientation) > 0
%   The goal is in a counterclockwise position from the orientation that 
%   allows to afford the obstacle on the right
% end

if angleDifference(goal_orientation, left_conflict_avoidance_angle) < 0 && ...
   angleDifference(right_conflict_avoidance_angle, goal_orientation) > 0
    % The goal in on the free area
    goal_on_free=true;
else
    goal_on_free=false;
end

% if angle_difference(goal_orientation, left_conflict_avoidance_angle) < 0
%   The goal is in a clockwise position from the orientation that allows to
%   afford the obstacle on the left
% end

if angleDifference(goal_orientation, left_conflict_avoidance_angle) < 0 && ...
   angleDifference(left_conflict_avoidance_angle + angle_of_view_for_ignore , goal_orientation) > 0
    % The goal in on the area of vision
    goal_visible=true;
else
    goal_visible=false;
end

if (robot_can_ignore && goal_on_free && goal_visible)
    % The conflict is ignorable
    conflict_ignorable = true;
end

% Debug
% if robot_id==1
%     disp(['Ignorable=' num2str(robot_can_ignore) ' OnFree=' num2str(goal_on_free) ' Visible=' num2str(goal_visible)])
%     pause(1)
% end
% 

end

