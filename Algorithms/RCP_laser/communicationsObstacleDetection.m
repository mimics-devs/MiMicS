function polar_obst = communicationsObstacleDetection(Rs, polar_obst, ...
    theta, x_robot, y_robot, phi_robot, comm)
% This system receives information from all the moving obstacles (robots)
% that are sharing its possitions and returns an actualization of the
% polar_obst histogram with the distances to all surrounding obstacles.
% Obstacles farer away from Rs [m] are ignored.

% Checks the number of neigbours of the system
N_Neighb = size(comm.matrix, 1);

for i=1:N_Neighb
    % Transforming from global coordinate system to local coordenate system
    [x_other, y_other] = comm.getPosition(i);
    x_diff = x_other - x_robot;
    y_diff = y_other - y_robot;
    
    % Checking if the object is inside the Rs sliding window
    if (abs(x_diff) < Rs || abs(y_diff) < Rs)
        
        % Checking if the robot is checking its own position
        if (x_diff || y_diff)
            
            % Rotating from global coordinate system to local coordinate
            % system
            % Note: This operation should be performed before the
            % translation, but the translation gives enoght information to
            % discriminate some of the measurements
            x_diff_rot = x_diff*cos(-phi_robot) - y_diff*sin(-phi_robot);
            y_diff_rot = x_diff*sin(-phi_robot) + y_diff*cos(-phi_robot);
            
            r = sqrt(x_diff_rot^2 + y_diff_rot^2);
            ang = atan2(y_diff_rot, x_diff_rot);
            
            % The obstacle is inside the Rs disk. We have to include it on
            % the polar_obst histogram
            
            % Checking where is the more suitable position to include this
            % distance
            [~,idx]=min(abs(theta-ang));
            
            if (polar_obst(idx) > r)
                polar_obst(idx) = r;
            end

            % Debug
            % disp(['Object detected at ' num2str(r) 'm and ' num2str(ang*180/pi) 'deg'])
        end
        
    end
end

end

