function [solution_available, conflictleft2sorround_idx, conflictright2sorround_idx] = ...
    checkForSolutions(conflicts_idx, theta, robot_id)
% Divides the working space in navigable and nonnavigable area.
% Checks if the navigable area is an open space (where the robot can scape
% or not).
% In case of possible scape, returns the id of the conflict to sorround (
% the one that allows to leave the conflict on the left side ), and the id
% of the conflict that should be not sorrounded ( the one that allows to
% leave the conflic on the right side).
%
% Note: phi_free_ref should be referentiated to the center of the robot and
% not to the map
% Note: Robot_id is only used for debuging purposes

% We asume by default that there is no solution available.
solution_available = false;
conflictleft2sorround_idx = nan;
conflictright2sorround_idx = nan;

% Solving taking into account the number of conflicts
switch length(conflicts_idx)
    case 1
        % If there is only one conflict, there is going to be always a
        % solution
        solution_available=true;
        conflictleft2sorround_idx=conflicts_idx;
        conflictright2sorround_idx=conflicts_idx;
        
    otherwise
        % If the angle between conflicts is larger than pi, means that
        % there is an infinite navigable area.
        
        % Note: angle_difference returnes negatives values if the
        % difference is larger than pi. This happens because the error is
        % shorter if you measure it in a counterclockwise way instead in a
        % clockwise way.
        
        for i=2:length(conflicts_idx)
            if (angleDifference(theta(conflicts_idx(i-1)),theta(conflicts_idx(i))) < 0)
                conflictleft2sorround_idx = conflicts_idx(i-1);
                conflictright2sorround_idx = conflicts_idx(i);
                solution_available = true;
            end
        end
        
        % It is necessary to take into account the -pi/pi discontinuity
        if (angleDifference(theta(conflicts_idx(length(conflicts_idx))),theta(conflicts_idx(1))) < 0)
            conflictleft2sorround_idx = conflicts_idx(length(conflicts_idx));
            conflictright2sorround_idx = conflicts_idx(1);
            solution_available = true;        
        end
        

%         for i=2:length(conflicts_idx)
%             difference = angle_difference(theta(conflicts_idx(i-1)),theta(conflicts_idx(i)));
%             if (difference < 0)
%                 % It is returning the error in a counterclockwise way
%                 % instead of in a clockwise way (Check the performance in debug)
%                 difference = difference + 2*pi;
%             end
%                 
%             if  (difference >= pi)
%                 conflictleft2sorround_idx = conflicts_idx(i-1);
%                 conflictright2sorround_idx = conflicts_idx(i);
%                 solution_available = true;
%             end
%         end
%         
%         % It is necessary to take into account the -pi/pi discontinuity
%         difference = angle_difference(theta(conflicts_idx(length(conflicts_idx))),theta(conflicts_idx(1))) 
%         if (difference < 0)
%         	% It is returning the error in a counterclockwise way
%         	% instead of in a clockwise way (Check the performance in debug)
%         	difference = difference + 2*pi;
%         end
%         if (difference >= pi)
%             conflictleft2sorround_idx = conflicts_idx(length(conflicts_idx));
%             conflictright2sorround_idx = conflicts_idx(1);
%             solution_available = true;        
%         end
        
%         if (robot_id == 2)
%             debug_tool(conflicts_idx, theta);
%         end
end

end


function debug_tool(conflicts_idx, theta)
% Prints in the command line the reasoning followed by the system in case
% of more than one robot

errors= [];

for i=2:length(conflicts_idx)
    ang = angle_difference(theta(conflicts_idx(i-1)),theta(conflicts_idx(i)))*180/pi;
    if (ang < 0)
        ang = ang + 360;
    end
    errors = [errors ang];
end
        
% It is necessary to take into account the -pi/pi discontinuity
ang = angle_difference(theta(conflicts_idx(length(conflicts_idx))),theta(conflicts_idx(1)))*180/pi;
if (ang < 0)
    ang = ang + 360;
end
errors = [errors ang];

disp(['Conflicts at:   ' num2str( theta(conflicts_idx)*180/pi )])
disp(['errors:               ' num2str(errors)])
disp(' ')


end