function [v_ref, phi_ref] = algorithm(robot, scan, comm, ~)
    
    % Constants of the orientation system
    pos_noise = 0; 
            % Maximum posible noise on the positioning system
    alpha_sd = 1.05;
            % Number larger than 1 that allows to include some more safety 
            % space in the Safety disk

    radius_safety = alpha_sd * (robot.type.safety_radius +pos_noise);
            % radius_safety: Safety disk, area where the robot should be located
            % Note: safety_radius = sqrt(sum[(robot.type.vehicle_width/2)^2 (robot.type.vehicle_length/2)^2])  
            
            % A stoping simulation showed the follow results
            % Starting the stop at (5.3173,1.9951) Vmax=1
            % Finalizing the stop at (5.8114,19951) V=0.002
            %   Stopping distance: 0.4941
            % 
            % The parameters of the robot are 
            % Speed/Traction dynamic (a transfer function)
            % sptr_dyn = ss(tf(1, [0.5 1 0]))
            % Steering dynamic (a transfer function)
            % steer_dyn = ss(tf(1, [0.2 1]))
            % Parameters [Kp Ki Kd] for PID-control of v
            % Values computed with pidtool(sys,'pid') [Response time=0.08899s Transient behavior=0.9 ]
            % v_pid = [22.4749 7.0996 11.2515]
            % Parameters [Kp Ki Kd] for PID-control of phi
            % phi_pid = [20 0 3];
            % maximum translational velocity in m/s
            % v_max = 1
            % maximum rotational velocity in rad/s
            % omega_max = 1
    stop_distance = 0.48;             %[m]
    loop_period = 0.1;                %[s] = 1/[Hz]
    speed_max = robot.type.v_max;      %[m/s]
    
    speed_rendezvous = 0.5 * speed_max; %[m/s]
    % Note: If the speed is too fast, the system switches fast between
    % rendesvouz and rencontre, forcing a 011001010101010 undesired
    % behavior.
    
    alpha_rd = 1.0;
            % Number larger than 1 that allows to include some more safety 
            % space in the Safety disk
    
    radius_reserved = alpha_rd * (speed_max*loop_period + stop_distance) + radius_safety;
            % Rrd: Reserved disk, area where the robot should be able to
            % stop.
            
            % To stop the robot requires the reaction time and the stop
            % distance. Rrd should be a ring that surrounds the Safety disk
            % Note that the selected parameters should not be increased
            % since the robot will not only stop but also turn at the same
            % time in most of the cases (only changing from free to blocked 
            % can force this).
            
    alpha_s = 3;
            % Number larger than 2. Allows to define how big should radius_sense be
            % in comparaison with Rrd. A large number forces to compute
            % obstacles that are far away from the robot. 
    
    radius_sense = alpha_s * radius_reserved;  
            % Sensing radius: Obstacles that are farer away from the 
            % robot than this value are not properly sensed or ignored
    
    
    N = 360;  % Number of divisions of the polar_obst histogram. 
              % (Larger values perform better accuracies but increases the computational load)
              
    phi_max_err = pi/15;
            % Maximum error allowed while turning in the RENCONTRE state
            
    %%% IMPROVEMENT %%%
    rotation_distance = radius_reserved - radius_safety;
            % The system will try to keep this distance while sourrounding
            % other obstacles
    p_rotation = 0.6;
            % Acts as a P controler to try to keep the distance while
            % sourrounding.
            
    angle_of_view_for_ignore = pi/2;
            % Instead of ussing all navigable area to look for the goal in
            % order to define the obstacle ignorable or not, looks only on
            % the angle that starts on the avoidable orientation and
            % increases in a clockwise direction until the mentioned value.
            % This allows to deal with the wall-convex conflict.
            
            % Note: to return to the old system, the values should be set
            % to P_rotation= 0; angle_of_view_for_ignore = pi;
            
    rotation_behaviour = +1;
            % +1 - Rotate allways counterclockwise (default)
            %  0 - Rotate to the goal
            % -1 - Rotate allways clockwise
    %%%%%%%%%%%%%%%%%%%

    % Constants of the speed system
    global dist_arrival;
            % Once the robot is at less than this distance, it considers
            % itself on the goal.
    dist_braking = 3*dist_arrival;
            % At this distance, the robot starts to slowdown its speed.

            % If the robot is at dist_braking but its orientation is larger
            % than PhiBreaking, stops its linear speed.
    phi_braking = 0.8*pi;   % [0,pi]
    k_braking  = 0.1;       % [1,0)
            
              
    % Defining all possible states of the state machine for the orientation
    STATE_MACHINE_ORIENTATION = struct('FREE',0,'RENDEZVOUS',1,'RENCONTRE',2,'BLOCKED',3);
            % FREE: There are no obstacles on the system, or can be ignored
            % RENDEZVOUS: A non ignorable obstacle is meet. The robot
            % should stop and surround it in a counterclockwise direction.
            % RENCONTRE: The robot is prepared to afford the obstacle.
            % Start to sorround it.
            % BLOCKED: There are many obstacles sourrounding the robot. It
            % has to keep his position until new order.
            
    % Defining all possible states of the state machine for the speed
    STATE_MACHINE_SPEED = struct('TRAVELING2GOAL_FREE',0,'ARRIVING2GOAL',1,'ONGOAL',2);
            % State machine transitions controller
            % The state machine is composed by three states controled by the
            % distance to the goal
            %
            % |----TRAVELING2GOAL_FREE----+----ARRIVING2GOAL----+---ONGOAL----+
            %                         Dbreaking             dist_arrival        Goal
            %       Max Speed                   Smooth speed        Stop
            
            
    % Note: We could detect if the obstacles are static or dynamic with the
    % laser. The robots have corners, and it could be diferentiated from
    % the walls and so on. (Chairs and tables can  be treated as dynamic
    % obstacles)
    
    % Computing the necessary reference to arrive to the goal without care
    % about conflicts
    phi_free_ref = robot.getGoalOrientation();
    v_ref = speed_max;    
    
    % Remembering the last state of the state machine
    if (isfield(robot.vars, 'status_orientation'))
        status_orientation = robot.vars.status_orientation;
    else
        status_orientation = STATE_MACHINE_ORIENTATION.FREE;
    end

    
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    % PREPARING THE WORK FOR THE STATE MACHINE IN ORIENTATION (Start)

    % Prepares the polar_obst histogram
    theta_incr = 2*pi/N;
    theta = pi:-theta_incr:-pi;
    polar_obst = radius_sense * ones(size(theta));
    
    % Filling the polar_obst histogram with the communications information
    polar_obst_comm = communicationsObstacleDetection(radius_sense, polar_obst, theta, robot.x, robot.y, robot.phi, comm);
    
    % Filling the polar_obst histogram with the laser information
    polar_obst_laser = laserObstacleDetection(radius_sense, polar_obst, theta, scan(:,1),scan(:,2), false); % robot.id == 2);
    
    % Discriminating the conflicts according to it's origin
    [polar_obst, conflicts_idx, conflicts_detected] = discriminateConflicts(radius_reserved, polar_obst_comm, polar_obst_laser);  

    if (conflicts_detected)
        % Checking if there is a solution available
        [solution_available, conflict_l2surround_idx, conflict_r2surround_idx] = ...
            checkForSolutions(conflicts_idx, theta, robot.id);
        
        if (solution_available)
            % Checks if the conflict is ignorable
            if (status_orientation == STATE_MACHINE_ORIENTATION.FREE)
                conflict_ignorable = checkForIgnorableConflicts( ...
                    theta(conflict_l2surround_idx), theta(conflict_r2surround_idx), ...
                    phi_free_ref - robot.phi, phi_max_err, 2*pi, robot.id);
            else
                conflict_ignorable=checkForIgnorableConflicts(theta( ...
                    conflict_l2surround_idx), theta(conflict_r2surround_idx), ...
                    phi_free_ref - robot.phi, phi_max_err, angle_of_view_for_ignore, robot.id);
            end
        
            if (~conflict_ignorable)
                % Computes the phi_avoidance that allows the robot to surround
                % the obstacle.
                phi_avoidance = computeAvoidancePhi( ...
                    theta(conflict_l2surround_idx), phi_free_ref, robot.phi, ... 
                    polar_obst(conflict_l2surround_idx), rotation_distance, p_rotation, rotation_behaviour);
            end
        end
    end
    
    
    % PREPARING THE WORK FOR THE STATE MACHINE IN ORIENTATION (End)
    % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
    
    % % % % % % % % % % % % % % % % % % % % % % % 
    % COMPUTING TRANSITIONS IN ORIENTATION (Start)
    
    % Defining the default status as free
    status_orientation=STATE_MACHINE_ORIENTATION.FREE;
    
    % Changing the status of the state machine according to the situation
    if (conflicts_detected)
        % The state machine has to transit
        
        if (solution_available)
            % Exists a solution for the conflict
            if (conflict_ignorable)
                % The solution is to ignore the conflict
                status_orientation = STATE_MACHINE_ORIENTATION.FREE;
            else
                % The solution is to surround the conflict
                status_orientation = STATE_MACHINE_ORIENTATION.RENCONTRE;
                
                if ( abs(angleDifference(robot.phi,phi_avoidance)) < phi_max_err)
                    % The robot is prepared to surround the conflict
                    status_orientation = STATE_MACHINE_ORIENTATION.RENDEZVOUS;
                end
            end
        else
            % There is a conflict without solution
            status_orientation = STATE_MACHINE_ORIENTATION.BLOCKED;
        end
    end
    
    % Saving the current status
    robot.vars.status_orientation = status_orientation;
    
    % COMPUTING TRANSITIONS IN ORIENTATION (End) 
    % % % % % % % % % % % % % % % % % % % % % % 
    
    
    % % % % % % % % % % % % % % % % % % % % 
    % COMPUTING ACTUATION IN SPEED (Start)
    

    % The system multipyes the desired speed with a constant k_turn that 
    % depends on how far away is the current orientation of the desired one
    
    % k_turn depends on the variable PhiBreaking on the following form:
    % 
    %              |
    %            1 + 
    %              |    \
    %   k_braking  +        +--------
    %            0 +--------+------------+
    %                   phi_braking   ang_err
    ang_err = abs( angleDifference(phi_free_ref, robot.phi));
    
    if (ang_err > phi_braking)
        k_turn = k_braking;
    else
        a = (k_braking-1)/phi_braking;
        b = 1;

        k_turn = a*ang_err + b;
    end

    v_ref_free = k_turn * min([speed_max robot.getDesiredVelocity()]);
    
    % COMPUTING ACTUATION IN SPEED (End)
    % % % % % % % % % % % % % % % % % % 
    
     
    % % % % % % % % % % % % % % % % % % % % % % 
    % COMPUTING ACTUATION IN ORIENTATION (Start)
    % Note: The actuation in speed should be already computed
    
    switch status_orientation
        case STATE_MACHINE_ORIENTATION.FREE
            % The system can travel at following the free reference to the
            % goal
            v_ref = v_ref_free;  % Keeps the speed as it is
            phi_ref = phi_free_ref;
            
        case STATE_MACHINE_ORIENTATION.RENCONTRE
            % The system should stop and turn in the specified reboundant
            % orientation
            v_ref = 0;
            phi_ref = phi_avoidance;
            
        case STATE_MACHINE_ORIENTATION.RENDEZVOUS
            % The system should turn while keeps the specified reboundant
            % orientation
            
            % Uses as a reference on speed the lowest reference possible.
            v_ref = min([robot.getDesiredVelocity() speed_rendezvous]);
            phi_ref = phi_avoidance;
            
        case STATE_MACHINE_ORIENTATION.BLOCKED
            % The system should keep its position and orientation until new
            % order.
            phi_ref = robot.phi;
            v_ref = 0;
    end
    
    % COMPUTING ACTUATION IN ORIENTATION (End)
    % % % % % % % % % % % % % % % % % % % % % 
    
    % Debug systems
    
%     if (robot.id == 1)
%         %debug_plot_state(STATE_MACHINE_ORIENTATION,status_orientation)
%         
%         SegmentedSpaceRepresentation(robot, conflicts_idx, theta, polar_obst, scan(:,1), scan(:,2));
%         
%     end
%     
%     debug_plot_circles(robot,radius_safety,Rrd,status_orientation,STATE_MACHINE_ORIENTATION);
    
%     % debug
%     if (robot.id == 3)
%         DataRepresentation(vRef,'vref');
%     end
%     
%     SegmentedSpaceRepresentation(robot, conflicts_idx, theta, polar_obst);
%     
%     

    

end % function

function debug_plot_state(STATE_MACHINE_ORIENTATION,status_orientation)
% Plots in the screen the different possible status
    switch status_orientation
        case STATE_MACHINE_ORIENTATION.FREE
            disp('State: Free')         
        case STATE_MACHINE_ORIENTATION.RENCONTRE
            disp('State: Rencontre')
        case STATE_MACHINE_ORIENTATION.RENDEZVOUS
            disp('State: Rendezvouz')
        case STATE_MACHINE_ORIENTATION.BLOCKED
            disp('State: Blocked')
    end
end

function debug_plot_circles(robot,radius_safety,Rrd,status_orientation,STATE_MACHINE_ORIENTATION)
% Plots in the main screen all the circles corresponding to all the robots.

% Changing to plot in the main window
figure(1)
subplot(5,5,[1:4 6 11 16 21])
hold on

% Deleating the circles handler (if exists)
if isfield(robot.vars,'circles_handler')
	delete(robot.vars.circles_handler);
end

h = debug_plot_circle(robot.x,robot.y,radius_safety,'b',12);

% Plots the a circle arround the robot with colors according to his
% state machine.
switch status_orientation
	case STATE_MACHINE_ORIENTATION.FREE
        h = [h debug_plot_circle(robot.x,robot.y,Rrd,'g',12)];
	case STATE_MACHINE_ORIENTATION.RENCONTRE
        h = [h debug_plot_circle(robot.x,robot.y,Rrd,'c',12)];
	case STATE_MACHINE_ORIENTATION.RENDEZVOUS
        h = [h debug_plot_circle(robot.x,robot.y,Rrd,'b',12)];
	case STATE_MACHINE_ORIENTATION.BLOCKED
        h = [h debug_plot_circle(robot.x,robot.y,Rrd,'m',12)];
end
% % % %     % debug_plot_circle(robot.x,robot.y,radius_sense,'y',12);

% Storing the handler to deleate the ploted circles.
robot.vars.circles_handler = h;

% Allowing other graps to work
subplot(5,5,[20 25]);

end


function h = debug_plot_circle(x,y,radius,color,points)
% This functions draws a circle arround the point (x,y) with the size
% radius. The circle is an approximation of points.
% It is used to draw while debuging the Rsafety disk and the Rreserved disk

ang=-pi:(2*pi)/points:pi;

x_cir=radius*cos(ang)+x;
y_cir=radius*sin(ang)+y;

h = plot(x_cir,y_cir,color);
drawnow

end


function DataRepresentation(var,title_str)
% Represents a data in a plot

MaxData=150;

persistent Data;
persistent i;

if isempty(i)
    if (isvector(var))
        Data = zeros(MaxData,length(var));
        i = 1;
    else
        error('Data representation can not afford a matrix of information');
    end
end

if i >= MaxData
    i=1;
end

for j=1:length(var)
    Data(i,j) = var(j);
end
i = i + 1;

plot(Data)
title(title_str)

end


function SegmentedSpaceRepresentation(robot, conflicts_idx, theta, polar_obst, scanner_distance, scanner_angle)
% The system plots the segmentation of the space of the robot. Tryes to not
% overplot the image only ploting the lines till the interceptions

    
    if isfield(robot.vars,'handler')
        delete(robot.vars.handler);
    end

    % Changing to plot in the main window
    figure(1)
    subplot(5,5,[1:4 6 11 16 21])
    hold on
    h = [];
    
    % Ploting the scanner view
    xs = scanner_distance.* cos(scanner_angle + robot.phi) + robot.x;
    ys = scanner_distance.* sin(scanner_angle + robot.phi) + robot.y;
    h = plot(xs,ys,'y-.');
    
    % Reserving enoght space to store all lines represented by two points
    xc1 = zeros(size(conflicts_idx));
    yc1 = zeros(size(conflicts_idx));
    xc2 = zeros(size(conflicts_idx));   % Contact point
    yc2 = zeros(size(conflicts_idx));
    xc3 = zeros(size(conflicts_idx));
    yc3 = zeros(size(conflicts_idx));
    
    % Preparing a simple representation as lines of all the conflicts
    for i=1:length(conflicts_idx)
        % Checking for the contact point
        ang = theta(conflicts_idx(i)) + robot.phi;
        d = polar_obst(conflicts_idx(i)) ;    
        
        % Computing the contact point
        xc2(i) = robot.x + d*cos(ang);
        yc2(i) = robot.y + d*sin(ang);
        
        % Computing the line
        xc1(i) = xc2(i) + 50*cos(ang + pi/2); 
        yc1(i) = yc2(i) + 50*sin(ang + pi/2); 
        
        xc3(i) = xc2(i) - 50*cos(ang + pi/2); 
        yc3(i) = yc2(i) - 50*sin(ang + pi/2); 
    end
    
%     % Conflicts lines that cuts with other conflict lines should be cropped
%     if (length(conflicts_idx) >= 2)
%         i = 1;
%         while( i <= length(conflicts_idx))
%             if (i == 1)
%                 i_prev = length(conflicts_idx);
%             else
%                 i_prev = i - 1;
%             end
%             
%             diff = angle_difference(theta(conflicts_idx(i)), theta(conflicts_idx(i_prev)));
%             
%             if (abs(diff < pi))
%                 % The two lines cutts in a position
%                 a = (yc3(i) - yc1(i)) / (xc3(i) - xc1(i));
%                 b = yc1(i)  - a*xc1(i);
%                 r = ['y ==' num2str(a) '*x +(' num2str(b) ')'];
%                 
%                 a_prev = (yc3(i_prev) - yc1(i_prev)) / (xc3(i_prev) - xc1(i_prev));
%                 b_prev = yc1(i_prev)  - a*xc1(i_prev);
%                 r_prev = ['y ==' num2str(a_prev) '*x +(' num2str(b_prev) ')'];
%                 
%                 [x,y] = solve(r,r_prev,'x','y')
%                 
%                 plot(x,y,'*')
%                 
%                 xc1(i) = x;
%                 yc1(i) = y;
%                 
%                 xc3(i_prev)=x;
%                 yc3(i_prev)=y;
%             end
% 
%             i = i + 1;
%         end
%     end
    
    % Ploting everything
    for i=1:length(conflicts_idx)
        % Ploting contact points
        h = [h plot([xc2(i) robot.x],[yc2(i) robot.y],'g')];
        
        % Ploting the segment points
        h = [h plot([xc1(i) xc3(i)],[yc1(i) yc3(i)],'r')];
    end
    
    % Storing the handler to delete the plotted lines later
    robot.vars.handler = h;
    
end