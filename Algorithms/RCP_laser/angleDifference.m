function diff = angleDifference(ang1,ang2)
% Computes the difference between two angles taking into account the -pi/pi
% discontinuity.

while(ang1 > +2*pi)
    ang1 = ang1-2*pi;
end

while(ang1 < -2*pi)
    ang1 = ang1+2*pi;
end

while(ang2 > +2*pi)
    ang2 = ang2-2*pi;
end

while(ang2 < -2*pi)
    ang2 = ang2+2*pi;
end

% Replicates ang1 in different 2*pi spaces
%
%   E.g: 
%        ang1-2*pi    ang1       ang1+2*pi
%     +------*---+------*---+------*--+
%   -2*pi       -pi         pi      2*pi

ang1=[ang1-2*pi ang1 ang1+2*pi];

% Checking the difference to all of them
%
%   E.g:
%    diff(1)      |----------------|
%    diff(2)      |-----|
%    diff(3) |----|
%     +------*---+------*---+------*--+
%   -2*pi       -pi         pi      2*pi

diff=ang1-ang2;

% Checking from the differences the shortest one
[~,idx]=min(abs(diff));

% Returning only the shortest difference to the system
diff=diff(idx);

end

