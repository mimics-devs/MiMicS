function phi_avoidance = computeAvoidancePhi(conflict_angle, goal_angle, ...
    robot_phi, d2conflict,rotation_distance,P_rotation, rotation_behaviour)
% Computes a orientation that allows to surround the obstacle keeping a
% rotation_distance between the robot and the obstacle itself.
% The rotation will be clockwise, to the goal or counterclockwise according
% to the parameter "rotation_behaviour"
            % +1 - Rotate allways counterclockwise (default)
            %  0 - Rotate to the goal
            % -1 - Rotate allways clockwise

% Computing the avoidance angle
switch rotation_behaviour
    case 1
        phi_avoidance_local = conflict_angle - pi/2;
    case -1
        phi_avoidance_local = conflict_angle + pi/2;
    case 0
        disp('THIS PART DOES NOT WORK!')
        if angle_difference(goal_angle, robot_phi) < 0
            phi_avoidance_local = conflict_angle + pi/2
        else
            phi_avoidance_local = conflict_angle - pi/2
        end
end

% Computing a P actuation to try to keep a rotation distance
% The system measures the diference between the desired distance and the
% current distance and tryes to minimize such error.
act = P_rotation*(d2conflict-rotation_distance);

% Adding such actuation to the avoidance angle
phi_avoidance_local = phi_avoidance_local + act;

% The following avoidance angle is an increment of the current angle.
phi_avoidance = phi_avoidance_local + robot_phi;

end