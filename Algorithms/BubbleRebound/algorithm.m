function [v_ref, phi_ref] = algorithm(robot, scan, ~, ~)
    
    % seperate ranger data
    ranger_dists = scan(:,1);
    beam_angles_rel = scan(:,2);

    STATE_MACHINE = struct('TRAVELING2GOAL_FREE',0, 'REBOUND_MODE',1);
    
    v_pref = robot.getDesiredVelocity();
    
    % The paper used 180° sensor scans. 
    % => Drop all readings below -90° or above +90°
    [left_idx, right_idx] = limitFOV(beam_angles_rel);
    beam_angles_rel = beam_angles_rel(left_idx:right_idx);
    ranger_dists = ranger_dists(left_idx:right_idx);
    % Small modification (also in the C++ code):
    % Limit distance readings to save some computational load
    limit_dist = 2.5;
    for i = 1:length(ranger_dists)
        ranger_dists(i) = min(ranger_dists(i), limit_dist);
    end 
    
    % is an obstacle within the robot's velocity-dependent bubble?
    is_obs_in_bubble = isObstacleInBubble(ranger_dists, beam_angles_rel, robot, limit_dist);
    
    if is_obs_in_bubble && robot.dist_to_goal > robot.dist_arrival
        robot.vars.br_status = STATE_MACHINE.REBOUND_MODE;
    end
    
    switch robot.vars.br_status
        
        case STATE_MACHINE.TRAVELING2GOAL_FREE
            phi_ref = robot.getGoalOrientation();
            v_ref = v_pref;
            
        case STATE_MACHINE.REBOUND_MODE
            % Default: Continue motion
            phi_ref = robot.phi_ref;
            v_ref = v_pref * 0.4;
            
            % but if a new obstacle is encountered
            if is_obs_in_bubble
                % calculate new rebound angle
                phi_ref = getReboundAngle(ranger_dists, beam_angles_rel);
                % function returns a value relative to robot, add the
                % current orientation to make it absolute
                phi_ref = limitAngleToPi(phi_ref + robot.phi);
                % make the robot almost stop for the turn
                % (difference between noised orientation and desired > 5°)
                if abs(discontAngleDifference(phi_ref, robot.phi)) > 0.0873
                    v_ref = 0.0;
                end
            elseif isGoalFree(robot, limit_dist, ranger_dists, beam_angles_rel)
                % -> exit rebound mode
                robot.vars.br_status = STATE_MACHINE.TRAVELING2GOAL_FREE;
                phi_ref = robot.getGoalOrientation();
            end
             
    end 

end % function


function [iLeft, iRight] = limitFOV(beam_angles_rel)
% LIMITFOV Limits the field-of-view to 180° as used by the BR-paper.
%
% [ranger_dists, beam_angles] = limitFOV(ranger_dists, beam_angles_rel)
%
% IN:
% beam_angles_rel - laser beam angles relative to the orientation
%
% OUT:
% iLeft - Left index value
% iRight - Right index value
    n = length(beam_angles_rel);
    iLeft = 1;
    iRight = n;
    for i = 1:n
        % beam_angles are absolute, transform to relative
        if beam_angles_rel(i) > pi/2
            iLeft = i+1;
        elseif beam_angles_rel(i) < -pi/2
            iRight = i-1;
            return;
        end
    end 
end

function phi_ref = getReboundAngle(ranger_dists, beam_angles_rel)
% GETREBOUNDANGLE Calculates a new orientation reference after an obstacle 
% has entered the robot's bubble.

    sum_dist = 0;
    sum_dist_times_angle = 0;
    
    for i = 1:length(ranger_dists)
        sum_dist = sum_dist + ranger_dists(i);
        sum_dist_times_angle = sum_dist_times_angle + ranger_dists(i)*beam_angles_rel(i);
    end
    
    phi_ref = sum_dist_times_angle/sum_dist;
    
    % special case: Symmetrical distribution of obstacles (Fig. 11 in paper)
    if (abs(phi_ref) < 0.001)
        phi_ref = pi/2;
    end
end

function rslt = isObstacleInBubble(ranger_dists, beam_angles_rel, robot, limit_dist)
% ISOBSTACLEINBUBBLE Checks for each angle if the measured value is lower
% than the bubble boundary.
    rslt = false;
    
    for i = 1:length(ranger_dists)
        if ranger_dists(i) < limit_dist
            if ranger_dists(i) < getBubbleDist(beam_angles_rel(i), robot)
                rslt = true;
                return;
            end
        end
    end
end

function dist = getBubbleDist(angle, robot)
% GETBUBBLEDIST - Calculates the bubble distance of an angle
%
% dist = getBubbleDist(angle)
%
% In theory we can get angles ranging from -3.14 to +3.14 (in radiant). The
% sensor actually does not do a 360° scan, so it will be less . 0 will be 
% the the angle pointing directly in front of the robot. The basic idea of 
% the following calcuation is to make the bubble bigger in front of the 
% robot (angles close to 0) and then reduce the sensitivity at the sides 
% (and having no sensitivity bubble "behind" the robot)

	angle = abs(angle);
    % pi/2 is orthogonal to the robot velocity. What is behind the robot 
    % should not trigger the bubble.
    if (angle > pi/2)
        dist = 0;
        return;
    end

    % base value of the robot size
    dist = 1.5*sqrt(robot.type.vehicle_width^2 + robot.type.vehicle_length^2);
    
    % Ki * V * delta_t 
    Ki =14;
    %dist = dist + Ki*robot.type.v_max*robot.steptime;
    dist = dist + Ki*robot.v*robot.delta_t;
end

function rslt = isGoalFree(robot, limit_dist, ranger_dists, beam_angles_rel)
% ISGOALFREE - Determines if there is a free line of sight between the
% robot and its goal.

    rslt = false;

    % find relative angle from robot to goal
    goal_angle_rel = limitAngleToPi(robot.getGoalOrientation() - robot.phi);
    
    % if the goal is outside of our field-of-view, a free line of sight is assumed
    if abs(goal_angle_rel) > pi/2
        rslt = true;
        return;
    end
    
    % Limit the goal distance to the max laser distance
	% This solves for situations where the goal is outside of the laser capacity
	% (which would always return false as there is no given line of sight to the goal)
    dist_g = min(robot.dist_to_goal, limit_dist);
    
    % Try to find the two laser-reading entries between which the goal lies
    for i = 1:length(ranger_dists)-1
        if beam_angles_rel(i) >= goal_angle_rel && goal_angle_rel >= beam_angles_rel(i+1)
            % Check if the goal is within free line of sight
            if ranger_dists(i) >= dist_g && ranger_dists(i+1) >= dist_g
                rslt = true;
                return;
            end
        end
    end

end