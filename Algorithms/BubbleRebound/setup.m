function rslt = setup(robots, ~)
%SETUP Does some preparatory work for the robots before the simulation
%starts.
%
% INPUT
% robots - The robots (an array of RobotModel instances)
% map - The map the simulation uses
%
% OUTPUT
% rslt - True if the preparations were successful, false otherwise.

STATE_MACHINE = struct('TRAVELING2GOAL_FREE',0, 'REBOUND_MODE',1);
for i = 1:numel(robots)
   robots(i).vars.br_status = STATE_MACHINE.TRAVELING2GOAL_FREE; 
end

rslt = true;

end

