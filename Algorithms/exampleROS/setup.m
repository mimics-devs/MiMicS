function rslt = setup(robots, map )
%SETUP Does some preparatory work for the robots before the simulation
%starts.
%
% INPUT
% robots - The robots (an array of RobotModel instances)
% map - The map the simulation uses
%
% OUTPUT
% rslt - True if the preparations were successful, false otherwise.

if ~license('test', 'robotics_system_toolbox')
    error('You need to have the Robotics Toolbox installed to use the exampleROS algorithm.');
end

rosinit('127.0.0.1');

occ_grid = map.convertToOccupancyGrid(0.1);

%Publish map metadata
map_meta_pub = rospublisher('/map_metadata', 'nav_msgs/MapMetaData', 'IsLatching', true);
metadata_msg = rosmessage(map_meta_pub);
curr_time = rostime('now');
metadata_msg.MapLoadTime.Sec = curr_time.Sec;
metadata_msg.MapLoadTime.Nsec = curr_time.Nsec;
metadata_msg.Resolution = occ_grid.info.res;
metadata_msg.Width = occ_grid.info.num_cols;
metadata_msg.Height = occ_grid.info.num_rows;
metadata_msg.Origin.Position.X = occ_grid.info.origin(1);
metadata_msg.Origin.Position.Y = occ_grid.info.origin(2);
send(map_meta_pub, metadata_msg);

% Publish map
map_pub = rospublisher('/map', 'nav_msgs/OccupancyGrid', 'IsLatching', true);
map_msg = rosmessage(map_pub);
map_msg.Header.Stamp = curr_time;
map_msg.Header.FrameId = 'world';
map_msg.Info = metadata_msg;
map_msg.Data = reshape(occ_grid.data' .* 100, [], 1);     % -1: unknown 0: free 100: blocked. Reshape to 1D-Array
send(map_pub, map_msg);
disp('Published occupancy grid map');

global tftree;
tftree = rostf;

for i = 1:numel(robots)
   name = sprintf('robot_%d', i);
   robots(i).vars.ros_name = name;
   
   % Publish goal 
   robots(i).vars.goal_pub = rospublisher(['/' name '/goal'], 'geometry_msgs/Pose', 'IsLatching', true);
   goal_msg = rosmessage('geometry_msgs/Pose');
   goal_msg.Position.X = robots(i).x_goal;
   goal_msg.Position.Y = robots(i).y_goal;
   send(robots(i).vars.goal_pub, goal_msg);
   
   % Set up  other publishers
   robots(i).vars.pose_pub = rospublisher(['/' name '/pose'], 'geometry_msgs/Pose');
   robots(i).vars.scan_pub = rospublisher(['/' name '/scan'], 'sensor_msgs/LaserScan');
   
   % Set up cmd_vel subscriber
   robots(i).vars.cmdvel_sub = rossubscriber(['/' name '/cmd_vel'], 'geometry_msgs/Twist');
   
   % Send initial pose
   pose_msg = rosmessage('geometry_msgs/Pose');
   pose_msg.Position.X = robots(i).x;
   pose_msg.Position.Y = robots(i).y;
   quat = axang2quat([0 0 1 robots(i).phi]);
   pose_msg.Orientation.W = quat(1);
   pose_msg.Orientation.X = quat(2);
   pose_msg.Orientation.Y = quat(3);
   pose_msg.Orientation.Z = quat(4);
   send(robots(i).vars.pose_pub, pose_msg);
end
disp('Created pose publishers and cmd_vel subscribers.');

rslt = true;

end

