function shutdown( )
%SHUTDOWN to be executed after the simulation ends.
    clearvars tftree;
    rosshutdown;

end

