#include "mimros.h"
#include "string.h"
#include <math.h>

#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>

int main(int argc, char **argv) {
    // name remapping etc
    ros::init(argc, argv, "mimros");
    MimROS mimros;
    ros::spin();
    return 0;
}

MimROS::MimROS() {
    ownName_ = ros::this_node::getName();
    goalReceived_ = false;
    
    stopDistance_ = 0.48;
    speedMax_ = 1;

    distArrival_ = 0.25;
    distBraking_ = 3*distArrival_;
    brakingSpeedMax_ = 0.7*speedMax_;
    brakingSpeedMin_ = 0.15*speedMax_;
    brakingPhi_ = 0.8*M_PI;
    brakingK_ = 0.1;
    
    cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>((ownName_ + std::string("/cmd_vel")).c_str(),2,true);
    laser_sub_ = nh_.subscribe((ownName_ + std::string("/scan")).c_str(), 1, &MimROS::LaserReceived, this);
    goal_sub_ = nh_.subscribe((ownName_ + std::string("/goal")).c_str(), 1, &MimROS::GoalReceived, this);
    map_sub_ = nh_.subscribe(std::string("/map").c_str(), 1, &MimROS::MapReceived, this);
    
    ROS_INFO("%s : Init complete", ownName_.c_str());
}


void MimROS::GoalReceived(const geometry_msgs::PoseConstPtr& msg) {
	
    goalX_ = msg->position.x;
    goalY_ = msg->position.y;

    // until the first goal is received, the robot shouldn't move
    if (!goalReceived_) {
        goalReceived_ = true;
        pose_sub_ = nh_.subscribe((ownName_ + std::string("/pose")).c_str(), 1, &MimROS::PoseReceived, this); 
    }
    ROS_INFO("%s received new goal %f|%f", ownName_.c_str(), goalX_, goalY_);
}

void MimROS::PoseReceived(const geometry_msgs::PoseConstPtr& msg) {

    robotX_ = msg->position.x;
    robotY_ = msg->position.y;

    // Convert quaternion to roll-pitch-yaw
    tf2::Quaternion q(msg->orientation.x, msg->orientation.y, msg->orientation.z, msg->orientation.w);
    tf2::Matrix3x3 m(q);
    double roll, pitch;
    m.getRPY(roll, pitch, robotPhi_);

     // calculate distance to goal
    goalDist_ = sqrt( pow((goalX_ - robotX_), 2) + pow((goalY_ - robotY_), 2));

    // get absolute angle towards goal
    double goalAngleAbs = atan2((goalY_- robotY_),(goalX_ - robotX_));
    // get relative angle
    goalAngle_ = AngleDifference(goalAngleAbs, robotPhi_);

    // prepare cmd_vel message
    geometry_msgs::Twist twist;
    twist.angular.x = 0;
    twist.angular.y = 0;
    twist.linear.y = 0;
    twist.linear.z = 0;
    
    twist.angular.z = goalAngleAbs;		// phi_ref
    twist.linear.x = GetDesiredVelocity(goalDist_, goalAngle_);		// v_ref

    // publish new command velocity
    cmd_vel_pub_.publish(twist);
    
    // tf2 broadcast
    geometry_msgs::TransformStamped transformStamped;
	transformStamped.header.stamp = ros::Time::now();
	transformStamped.header.frame_id = "world";
	transformStamped.child_frame_id = ownName_.c_str();
	transformStamped.transform.translation.x = robotX_;
	transformStamped.transform.translation.y = robotY_;
	transformStamped.transform.translation.z = 0.0;
	transformStamped.transform.rotation.x = q.x();
	transformStamped.transform.rotation.y = q.y();
	transformStamped.transform.rotation.z = q.z();
	transformStamped.transform.rotation.w = q.w();
	tf_br_.sendTransform(transformStamped);
}

void MimROS::LaserReceived(const sensor_msgs::LaserScanConstPtr& laser_scan) {
	//ROS_INFO("Received laser scan! angle_min=%f, angle_max=%f, num_beams=%d", laser_scan->angle_min, laser_scan->angle_max, (int) laser_scan->ranges.size()); 
}

void MimROS::MapReceived(const nav_msgs::OccupancyGridConstPtr& msg) {
	ROS_INFO("%s received map", ownName_.c_str());
}

double MimROS::AngleDifference(double ang1, double ang2) {
    // translate both angles to (-2*pi, +2*pi) range
    while (ang1 > 2*M_PI) ang1 -= 2*M_PI;
    while (ang1 < -2*M_PI) ang1 += 2*M_PI;
    while (ang2 > 2*M_PI) ang2 -= 2*M_PI;
    while (ang2 < -2*M_PI) ang2 += 2*M_PI;

    // duplicate first angle 2 times
    double angleRep[] = {ang1-2*M_PI, ang1, ang1+2*M_PI};

    // calculate the 3 errors and get the minimum
    double diff = 2*M_PI;
    double diffAux;
    for(unsigned int i = 0; i < 3; i++) {
        diffAux = angleRep[i] - ang2;
        if (abs(diffAux) < abs(diff)) diff = diffAux;
    }
    return diff;
}

double MimROS::GetDesiredVelocity(double& goalDist, double& goalAngle) {
	double turnK = 1;
    double vel = 0;
	
    if (goalDist < distBraking_) {
        if (goalDist < distArrival_) {
            vel = 0;        // robot is on the goal
        }
        else {
            // robot is approaching the goal and should gracefully brake.
            double slope = (brakingSpeedMax_ - brakingSpeedMin_) / (distBraking_ - distArrival_);
            double offset = brakingSpeedMax_ - slope*distBraking_;
            vel =  slope*goalDist + offset;
        }
    }
    else { // far away
        vel = speedMax_;
    }

    /* turnK depends on the variable brakingPhi_ in the following fashion
                  |
                1 +
                  |    \
       brakingK_  +        +--------
                0 +--------+------------+
                       PhiBreaking    ang_err
    */
    turnK =  (abs(goalAngle) > brakingPhi_) ? brakingK_ : ((brakingK_ -1) / brakingPhi_)*abs(goalAngle) + 1;
	
    return turnK * vel;
}

