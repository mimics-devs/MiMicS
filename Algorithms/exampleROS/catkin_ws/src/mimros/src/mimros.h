#ifndef MIMROS_H
#define MIMROS_H

#include "ros/ros.h"
#include <tf2_ros/static_transform_broadcaster.h>

// messages needed
#include "sensor_msgs/LaserScan.h"
#include "geometry_msgs/Twist.h"
#include "nav_msgs/OccupancyGrid.h"
#include <geometry_msgs/TransformStamped.h>

/**
 * Simple algorithm to work in MiMicS.
 *
 * \author Merlin Stampa
 * \date Jul 13, 2017
 */
class MimROS {
	
public:
        /** Default constructor */
        MimROS();

protected:
private:
    // variables
    ros::NodeHandle nh_;
    
    ros::Subscriber pose_sub_;                          //!< Receives pose (position and orientation) estimates
    ros::Subscriber goal_sub_;                          //!< Receives goal poses
    ros::Subscriber laser_sub_;                         //!< Receives laser scans to derive the polar obstacle diagram (pod) from
    ros::Subscriber map_sub_;							//!< Receives map
    ros::Publisher cmd_vel_pub_;                        //!< Publishes new command velocity
    tf2_ros::StaticTransformBroadcaster tf_br_;			//!< Broadcasts local robot frame
    
    std::string ownName_;                          //!< Simply stores it's own name

    double goalX_, goalY_, goalDist_, goalAngle_, robotX_, robotY_, robotPhi_;
    
    double stopDistance_;           //!< Maximum distance needed for a full stop in meters.
    double speedMax_;               //!< Maximum speed of the robot in m/s
    double distArrival_;            //!< If the robot's distance to the goal is smaller than this distance, the robot considers itself on the goal.
    double distBraking_;            //!< The distance to goal at which the robot starts to reduce its speed for a smooth arrival.
    double brakingSpeedMax_;        //!< Maximum speed while in braking distance.
    double brakingSpeedMin_;        //!< Minimum speed while in braking distance.
    double brakingPhi_;             //!< [0,pi] If the robot is within braking distance but its orientation error is larger than this value, it tries to stop first in order to allow the rotation to happen.
    double brakingK_;               //!< 
    bool goalReceived_;				//!< Flag that is set to true if the first goal was received

    /** Callback for goals.
     * \param msg The message
     */
    void GoalReceived(const geometry_msgs::PoseConstPtr& msg);

    /** Callback for own pose estimates.
     * \param msg The message
     */
    void PoseReceived(const geometry_msgs::PoseConstPtr& msg);

    /** Callback for laser scan data.
     * \param laser_scan The laser scan message
     */
    void LaserReceived(const sensor_msgs::LaserScanConstPtr& laser_scan);
    
    /** Callback for the map.
     *  \param The message.
     */
    void MapReceived(const nav_msgs::OccupancyGridConstPtr& msg);
    
    /** Utility functio to get the difference between two angles (in
     * rad) from the [-pi;pi] format.
     * \param ang1 The first angle
     * \param ang2 The second angle
     */
    double AngleDifference(double ang1, double ang2);
    
    /** Calculates the velocity the robot would like to have ignoring
     * obstacles.
     * \param goalDist Distance to the goal (m)
     * \param goalAngle Relative angle towards the goal (rad)
     */
    double GetDesiredVelocity(double& goalDist, double& goalAngle);
};

#endif // MIMROS_H
