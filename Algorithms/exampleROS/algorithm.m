function [v_ref, phi_ref] = algorithm(robot, scan, ~, ~)
%ALGORITHM Tells A robot where to go.
%
% This specific variant is a simple example to work with ROS.
    
    % Publish current pose
    pose_msg = rosmessage('geometry_msgs/Pose');
    pose_msg.Position.X = robot.x;
    pose_msg.Position.Y = robot.y;
    quat = axang2quat([0 0 1 robot.phi]);
    pose_msg.Orientation.W = quat(1);
    pose_msg.Orientation.X = quat(2);
    pose_msg.Orientation.Y = quat(3);
    pose_msg.Orientation.Z = quat(4);
    send(robot.vars.pose_pub, pose_msg);
    
    % Publish scan
    dists = scan(:,1);
    beam_angles_rel = scan(:,2);
    scan_msg = rosmessage('sensor_msgs/LaserScan');
    scan_msg.Header.FrameId = robot.vars.ros_name;
    scan_msg.Header.Stamp = rostime('now');
    scan_msg.AngleMin = beam_angles_rel(end);
    scan_msg.AngleMax = beam_angles_rel(1);
    scan_msg.AngleIncrement = beam_angles_rel(1) - beam_angles_rel(2);
    scan_msg.RangeMin = robot.type.ranger_minmax(1);
    scan_msg.RangeMax = robot.type.ranger_minmax(2);
    scan_msg.Ranges = flip(dists);
    send(robot.vars.scan_pub, scan_msg);
    
    % Receive cmd_vel
    while numel(robot.vars.cmdvel_sub.LatestMessage) <= 0
        pause(0.01);
    end
    cmd_vel = robot.vars.cmdvel_sub.LatestMessage;
    
    v_ref = cmd_vel.Linear.X;
    phi_ref = cmd_vel.Angular.Z;
    
    % Tf2 demonstration
    global tftree;
    if robot.id == 2
        robToRob = getTransform(tftree, 'robot_1', 'robot_2', 'Timeout', 5);
        translation = robToRob.Transform.Translation;
        quat = robToRob.Transform.Rotation;
        axang = quat2axang([quat.W quat.X quat.Y quat.Z]);
        fprintf('Robot 1 to 2 tf: x %g, y %g, phi %g\n', translation.X, translation.Y, axang(4));
    end

end % function