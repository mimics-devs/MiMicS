function [ Fr_x, Fr_y ] = adjustedRepulsiveForce(w,Fr_x,Fr_y,cos_theta)
% Computes one special adjustement of the repulsive force of the system to
% reduce oscilations while affornting linear obstacles.

Fr_x=w*Fr_x+(1-w)*Fr_x*(-cos_theta);
Fr_y=w*Fr_y+(1-w)*Fr_y*(-cos_theta);

end