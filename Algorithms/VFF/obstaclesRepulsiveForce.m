function [ Fr_x, Fr_y ] = obstacleRepulsiveForce(Fer,Nx_cells,Ny_cells,cell_size,laserscan,beam_angles)
% The function receives the measurements of the laser and computes the
% repulsive forces of the system.

% Note: We have to satisfy the function (1) of the paper, thus we define
% x_0=0 and y_0=0 as well as x_t=d*sin(ang) y_t=d*cos(ang).

C=1;  % Asumes that in the histogram there was allways 1 hit when a robot is hit.

Fr_x=0;
Fr_y=0;

% Change of reference system. The laser is on the (0,0)
x_0=0;
y_0=0;

% Active window parameters
W_x=cell_size*Nx_cells/2;
W_y=cell_size*Ny_cells/2;

% Checking every measurement of the laser
for i=1:length(laserscan)
    x_t=laserscan(i)*cos(beam_angles(i));
    y_t=laserscan(i)*sin(beam_angles(i));
    
    % Checking if the target is on the active window
    if (abs(x_t-x_0) <= W_x)    % Inside of the x part of the window
        if (abs(y_t-y_0) <= W_x)    % Inside of the y part of the window
            Fr_x=Fr_x+Fer*C*(x_t-x_0)/(laserscan(i)^3);
            Fr_y=Fr_y+Fer*C*(y_t-y_0)/(laserscan(i)^3);
        end
    end
end

end