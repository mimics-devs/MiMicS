function cos_theta = cosComputation(Fr_x,Fr_y,V_linear,phi)
% This function computes one special cosinus that is used in some parts of
% the algorithm as a specific variable.

Fr_norm=norm([Fr_x Fr_y]);

if (Fr_norm && V_linear)
    % Exists a repulsion force
    v_x=V_linear*cos(phi);
    v_y=V_linear*sin(phi);
    
    cos_theta=(v_x*Fr_x+v_y*Fr_y)/(Fr_norm*V_linear);
    %cos_theta=cos_theta*180/pi;
else
    % Without repulsion force the value should be zero
    cos_theta=0;
end
    
end

