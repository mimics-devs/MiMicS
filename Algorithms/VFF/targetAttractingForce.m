function [ Ft_x, Ft_y ] = targetAttractingForce(Fct,x_current,y_current,x_target,y_target)
% The function receives the current position of the robot and the target,
% and computes the target-attracting force.

d_t=sqrt((y_target-y_current)^2+(x_target-x_current)^2);

Ft_x=Fct*(x_target-x_current)/d_t;
Ft_y=Fct*(y_target-y_current)/d_t;

end