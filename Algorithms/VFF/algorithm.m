function [v_ref, phi_ref] = algorithm(robot, scan, ~, ~)

    v_max = robot.type.v_max;
    
    % The following are defined by the example algorithm itself
    STATE_MACHINE = struct('TRAVELING',0,'ONGOAL',2);
    
    % Necessary constants for the system                                              
    Fct = +0.1;             % Target atracting force //original 5 // current 1
    Fer = -0.012;           % Object repulsive force //original -1 // -0.1
    w = 0.01;               % Helps while reducing the oscilation //original 0.25 // 0.01
    
    % Active window parameters
    num_x_cells = 60;    %[cells]
    num_y_cells = 60;    %[cells]
    cell_size = 0.1;     %[m/cell]
    
    % State machine transitions controller
    % The state machine is composed by two states controled by the
    % distance to the goal
    %
    % |----TRAVELING----+---ONGOAL----+
    %              dist_arrival      Goal
    if (robot.dist_to_goal > robot.dist_arrival)
        Status=STATE_MACHINE.TRAVELING;
    else 
        Status=STATE_MACHINE.ONGOAL;
    end
    
    % Depending on the state the system uses the VFF algorithm at the
    % desired speed or the same one at speed zero.
    %
    %              |
    %        V_VFF +-------+
    %              |       +  
    %            0 |       +-------+
    %              |-------+-------+
    %                  Darrival  Goal
    
    % Computing the atracting forces
    [Ft_x,Ft_y] = targetAttractingForce(Fct, robot.x, robot.y, robot.x_goal, robot.y_goal);

    % Computing the repulsive forces
    ranger_dists = scan(:,1);
    beam_angles_rel = scan(:,2);
    [Fr_x, Fr_y] = obstaclesRepulsiveForce(Fer, ...
        num_x_cells, num_y_cells, cell_size, ranger_dists, beam_angles_rel);
    
    % Computing cos_theta
    cos_theta = cosComputation(Fr_x, Fr_y, robot.v, robot.phi);
    
    % Adjusting repulsive forces
    [Fr_x, Fr_y] = adjustedRepulsiveForce(w, Fr_x, Fr_y, cos_theta);
    
    % Adding all the forces to the system
    R = [Ft_x+Fr_x, Ft_y+Fr_y];
    
    % Normalizing such vector
    R = R/norm(R);
    
    % Computing orientation
    delta = atan2(R(2),R(1));
    
    % Computing speed (Note: if Fr is zero, cos_theta will be zero)
    v = v_max*(1-abs(cos_theta));
   
    phi_ref=delta;
        
    switch Status
        case STATE_MACHINE.TRAVELING
            v_ref = min([v robot.getDesiredVelocity()]);
        case STATE_MACHINE.ONGOAL
            v_ref = 0;
    end
     
    % Debuging sytem
%     if (robot.id==1)
%         if (~isfield(robot.vars,'plot_vars'))
%             robot.vars.plot_vars=R;
%         else
%             robot.vars.plot_vars = DataRepresentation(R,'Phiref', robot.vars.plot_vars);
%         end
%     end
        
end % function


function new_storage_cache = DataRepresentation(var, title_str, storage_cache)
% Represents a data in a plot

MaxData = 50;

if (size(storage_cache,1) < MaxData)
    new_storage_cache = [storage_cache ; var];
else
    new_storage_cache = [storage_cache(2:size(storage_cache,1),:); var];
end

plot(new_storage_cache)
title(title_str)

end