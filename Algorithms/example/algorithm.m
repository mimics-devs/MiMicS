function [v_ref, phi_ref] = algorithm(robot, scan, comm, map)
%ALGORITHM Tells A robot where to go.
%
%   [v_ref, phi_ref] = algorithm(robot, scan, comm, map)
%
% This function should perform calculations based on sensor readings and
% knowledge about the robot's status, to calculate a velocity and an
% orientation reference, guiding the robot towards its goal and arround
% obstacles. The system allows to choose between multiple algorithms, the
% only thing they have to have in common is the function declaration.
%
% This specific variant is a simple example designed to explain the basic
% tools you have available when designing your algorithm.
%
% IONPUT
% robot - Instance of RobotModel
% scanner - Data from ranger-function
% comm - The inter-robot communication system
% map - List of static obstacle lines (implicitly measured by scanner)
%
% Note that if you do not need some or more of the provided arguments, for
% example the map, you can replace them with '~' in the function 
% declaration to increase performance a bit.
%
% OUTPUT
% v_ref - Velocity reference in m/s
% phi_ref - Orientation reference in rad
%
% See also ROBOTMODEL, RANGER, COMMUNICATIONSYSTEM.
    
    %% How to get the robot's data
    
    % Note that many properties of RobotModel, especially regarding
    % position, orientation, velocity and acceleration are read-only, i.e.
    % there will be an error if you try robot.x = 10
    % Some of the data can be corrupted by simulated noise.
    % To see an overview of the properties, type "doc RobotModel".
%     x_current = robot.x;
%     y_current = robot.y;
%     x_goal = robot.x_goal;
%     y_goal = robot.y_goal;
    
    %% User-defined data
    
    % The RobotModel class has a struct member named vars. You can use it
    % to save arbitrary data.
    
%     robot.vars.ca_status = 0;
%     robot.vars.string = 'I am a robot'; 
    
    %% Laser ranger scan
    
    % A simulated laser ranger scan will be passed to this function. It is
    % represented as a matrix. The first column represents the measured
    % distances, while the corresponding value in the second column 
    % indicates the angle of the laser beam relative to the robot's 
    % orientation.
    % Type "help ranger" for more information.
    
%     measurements = scan(:,1);
%     beam_angles_rel = scan(:,2);
    
    %% Communication system
    
    % This function is passed a reference to a system which represents a
    % very simple communication mechanism. It's a class which stores the
    % last known (but possibly noise-corrupted) position, orientation and
    % velocity of every robot. You can access this data through 
    
    % The 'comm' argument of this function is the handle of an object
    % which allows the robots to exchange some information about their
    % current position, velocity etc. The robots update their information
    % at the end of a simulation loop. 
    % For more information, type "doc CommunicationSystem".
    
%     id_other_robot = 1;
% 	[x_other, y_other] = comm.getPosition(id_other_robot);
    
    %% Logging
    % During a simulation, each robot keeps its individual log file,
    % updated after each simulation step or if the robot deactivates. Some
    % values are always logged, i.e. position and orientation, but you can
    % configure MiMicS to log more values.
    
    % If you want to log a string message, use the setLogMsg() function as
    % displayed in the following example. The buffer will be cleared before
    % the next simulation step begins.
    % NOTE: You should not use line breaks or semicolons.
    
    % For this example to work, also uncomment the lines in the previous
    % code block.
    
%     msg = sprintf('Reported pos of robot %i: (%g|%g)', id_other_robot, x_other, y_other);
%     robot.setLogMsg(msg);
    
    %% Debug Plotting
    
    % If you want to display some information graphically while the
    % simulation is running, you can use MATLAB's built-in plotting
    % abilities.
    
    % The following example traces the position of the first robot in the
    % main simulation plot.
    
%     if robot.id == 1
%         figure(1);          % main simulation plot
%         hold on;
%         plot(robot.x_gt, robot.y_gt, 'x', 'Color', 'k', 'MarkerSize', 2);
%     end

    %% Example Algorithm
    
    % This algorithm does nothing more than guiding the robot towards the
    % goal, without any form of obstacle avoidance. 
    phi_ref = robot.getGoalOrientation();
    v_ref = robot.getDesiredVelocity(); 

end % function