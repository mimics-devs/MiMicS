function rslt = setup(robots, map )
%SETUP Does some preparatory work for the robots before the simulation
%starts.
%
% INPUT
% robots - The robots (an array of RobotModel instances)
% map - The map the simulation uses
%
% OUTPUT
% rslt - True if the preparations were successful, false otherwise.

for i = 1:numel(robots)
   robots(i).vars.string = 'I am a robot'; 
   robots(i).vars.ca_status = 0;
end

rslt = true;

end

