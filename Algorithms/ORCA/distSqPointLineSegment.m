function distSq = distSqPointLineSegment(a, b, c)
%DISTSQPOINTLINESEGMENT Computes the squared distance from a line segment
%with the specified endpoints to a specified point.
%
% a: The first endpoint of the line segment.
% b: The second endpoint of the line segment.
% c: The point to which the squared distance is to be calculated.
%
% distSq: The squared distance from the line segment to the point.

    r = dot((c - a), (b - a)) / (norm(b - a))^2;

    if r < 0.0
        distSq = norm(c - a)^2;

    elseif r > 1.0
        distSq = norm(c - b)^2;

    else
        distSq = norm(c - (a + r * (b - a)))^2;
    end

end

