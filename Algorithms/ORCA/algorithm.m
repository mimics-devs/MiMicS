function [v_ref, phi_ref] = algorithm(robot, ~, comm, map)
% This specific variant uses the ORCA algorithm. All other functions
% and classes in this folder are adaptations of the ones found in the
% RVO2 library (http://gamma.cs.unc.edu/RVO2/).
    
    % For every simulation step, the agent tree must be rebuild since the
    % robot positions have most likely changed.
    if robot.vars.agent.kdTree.last_update ~= comm.getTime()
        robot.vars.agent.kdTree.last_update = comm.getTime();
        robot.vars.agent.kdTree.buildAgentTree(comm);
    end
    
    % calculate preferred orientation
    phi_pref = robot.getGoalOrientation();
    
    % Copy robot properties
    robot.vars.agent.position = [robot.x robot.y];
    robot.vars.agent.velocity = robot.v .* [cos(robot.phi) sin(robot.phi)];      
    robot.vars.agent.velocity_pref = robot.getDesiredVelocity() .* [cos(phi_pref) sin(phi_pref)]; 
    
    % Get obstacle and agent neighbors from Kd-Tree
    robot.vars.agent.computeNeighbors();
    
    % Calculate new reference velocity and orientation.
    [v_ref, phi_ref] = robot.vars.agent.computeNewVelocity();

end % function