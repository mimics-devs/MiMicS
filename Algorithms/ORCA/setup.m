function rslt = setup(robots, map )
%SETUP Does some preparatory work for the robots before the simulation
%starts.
%
% INPUT
% robots - The robots (an array of RobotModel instances)
% map - The map the simulation uses
%
% OUTPUT
% rslt - True if the preparations were successful, false otherwise.

% Fast queries for the neighbors of an agent are possible with a Kd-Tree.
persistent kdTree;
kdTree = KdTree();
obstacles = Obstacle.empty();
for i = 1:size(map.lines,1)
    [~, obstacles] = addObstacle(obstacles, [map.lines(i,1) map.lines(i,3); map.lines(i,2) map.lines(i,4)]);
end
kdTree.buildObstacleTree(obstacles);
kdTree.last_update = CommunicationSystem.getInstance().getTime();

% Each robot gets an instance of the Agent class doing the core work.
for i = 1:numel(robots)
    robots(i).vars.agent = Agent();
    robots(i).vars.agent.kdTree = kdTree;
    robots(i).vars.agent.id = robots(i).id;
    robots(i).vars.agent.comm = CommunicationSystem.getInstance();
    robots(i).vars.agent.delta_t = robots(i).delta_t;
    robots(i).vars.agent.max_speed = robots(i).type.v_max;
    % Increasing the safety radius to be more scared about what is around
    robots(i).vars.agent.radius = 1.15*robots(i).type.safety_radius;
end

rslt = true;

end
