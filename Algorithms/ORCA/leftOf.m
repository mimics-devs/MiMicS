function rslt = leftOf(a, b, c)
%LEFTOF - Computes signed distance from a line connecting the specified
%points to a specified point.
%
% a: The first point on the line
% b: The second point on the line.
% c: The point to which the signed distance is to be calculated.
% rslt: Positive when the point c lies to the left of the line ab.
    rslt = det([a - c; b - a]);
    return;
end