classdef AgentTreeNode < handle
    %AGENTTREENODE Defines an agent kd-tree node.
    
    properties
        % The beginning node number.
        begin;
        % The ending node number.
        n_end;
        % The left node number.
        left;
        % The maximum x-coordinate.
        x_max;
        % The maximum y-coordinate.
        y_max;
        % The minimum x-coordinate.
        x_min;
        % The minimum y-coordinate.
        y_min;
        % The right node number.
        right;
    end
    
    methods
    end
    
end

