classdef KdTree < handle
    %KDTREE KdTree class.
    %   Enables efficient queries for agent and obstacle neighbors.
    
    properties
        % ObstacleTreeNode
        obstacle_tree = [];
        % vector of AgentTreeNode
        agent_tree = [];
        % list of agents
        agents = [];
        
        % timestamp of the last rebuilding of the agent tree
        last_update = 0;
    end    
    
    properties (Constant)
        RVO_EPSILON = 0.00001;
        MAX_LEAF_SIZE = 10;
    end
    
    methods
        function obj = KdTree()
            % Constructs a k-d-tree instance.
        end
        
        function buildAgentTree(obj,comm)
            % BUILDAGENTTREE Builds an agent kd-tree.
            %
            % comm: Instance of the CommunicationSystem. Used to get
            % x,y,v and phi of all the other robots.
            
            % First lines of code in OI are to check if new agents were
            % added between simulation steps. We do not need to do that.
            
            [x,y] = comm.getAllPositions(); 
            obj.agents = [x,y];
            obj.agent_tree = AgentTreeNode.empty(2*size(obj.agents,1) - 1, 0);
            
            obj.buildAgentTreeRecursive(1, size(obj.agents,1), 1);
        end % function buildAgentTree
        
        function buildAgentTreeRecursive(obj, begin, n_end, node)

            obj.agent_tree(node).begin = begin;
            obj.agent_tree(node).n_end = n_end;
            obj.agent_tree(node).x_min = obj.agents(begin,1);       
            obj.agent_tree(node).x_max = obj.agents(begin,1);
            obj.agent_tree(node).y_min = obj.agents(begin,2);        
            obj.agent_tree(node).y_max = obj.agents(begin,2);
            
            for i =(begin+1) : (n_end-1)
                obj.agent_tree(node).x_max = max(obj.agent_tree(node).x_max, obj.agents(i,1));
                obj.agent_tree(node).x_min = min(obj.agent_tree(node).x_min, obj.agents(i,1));
                obj.agent_tree(node).y_max = max(obj.agent_tree(node).y_max, obj.agents(i,2));
                obj.agent_tree(node).y_min = min(obj.agent_tree(node).y_min, obj.agents(i,2));
            end
            
            if (n_end - begin > obj.MAX_LEAF_SIZE)
                % No leaf node.
                is_vertical = (obj.agent_tree(node).x_max - obj.agent_tree(node).x_min) > (obj.agent_tree(node).y_max - obj.agent_tree(node).y_min);
                if is_vertical
                    split_value = 0.5 * (obj.agent_tree(node).x_max + obj.agent_tree(node).x_min);
                else
                    split_value = 0.5 * (obj.agent_tree(node).y_max + obj.agent_tree(node).y_min);
                end
                
                left = begin;
                right = n_end; 
                
                while left < right
                    if is_vertical
                        while (left < right) && (obj.agents(left,1) < split_value)
                            left = left + 1;
                        end
                    else
                        while (left < right) && (obj.agents(left,2) < split_value)
                            left = left + 1;
                        end
                    end
                    
                    if is_vertical
                        while (right > left) && (obj.agents(right-1,1) >= split_value)
                            right = right - 1;
                        end
                    else
                        while (right > left) && (obj.agents(right-1, 2) >= split_value)
                            right = right - 1;
                        end
                    end
                    
                    if (left < right)
                        % std::swap in OI
                        tmp_agent = obj.agents(right-1,:);
                        obj.agents(right-1,:) = obj.agents(left,:);
                        obj.agents(left,:) = tmp_agent(:);
                        left = left + 1;
                        right = right - 1;
                    end
                end % while left < right
                
                if left == begin
                    left = left + 1;
                    right = right + 1;
                end
                
                obj.agent_tree(node).left = node + 1;
                obj.agent_tree(node).right = node + 2*(left - begin);
                
                obj.buildAgentTreeRecursive(begin, left, obj.agent_tree(node).left);
                obj.buildAgentTreeRecursive(left, n_end, obj.agent_tree(node).right);
                
            end % if (end-begin > MAX_LEAF_SIZE)
        end % function buildAgentTreeRecursive
        
        function obstacles = buildObstacleTree(obj, obstacles)
            % Builds an obstacle k-d-tree.
            
            % First lines of code in OI are just to get obstacles from
            % simulator. We can't make much use of references here and
            % don't have sim instance, we just pass the obstacles back and
            % forth.
            
            [node, obstacles] = buildObstacleTreeRecursive(obj, obstacles);
            obj.obstacle_tree = node;
            
        end % function buildObstacleTree
        
        function [node, obstacles] = buildObstacleTreeRecursive(obj, obstacles)
            
            if numel(obstacles) == 0
                node = [];
                return;
            else
                node = ObstacleTreeNode();

                optimal_split = 0;
                min_left = numel(obstacles);
                min_right = numel(obstacles);

                for i = 1:numel(obstacles)
                    left_size = 0;
                    right_size = 0;

                    obstacle_I1 = obstacles(i);
                    obstacle_I2 = obstacle_I1.next_obstacle;

                    % Compute optimal split node.
                    for j = 1:numel(obstacles)
                        if i == j
                            continue;
                        end

                        obstacle_J1 = obstacles(j);
                        obstacle_J2 = obstacle_J1.next_obstacle;

                        J1_leftOf_I = leftOf(obstacle_I1.point, obstacle_I2.point, obstacle_J1.point);
                        J2_leftOf_I = leftOf(obstacle_I1.point, obstacle_I2.point, obstacle_J2.point);

                        if (J1_leftOf_I >= -obj.RVO_EPSILON && J2_leftOf_I >= -obj.RVO_EPSILON)
                            left_size = left_size + 1;
                        elseif (J1_leftOf_I <= obj.RVO_EPSILON && J2_leftOf_I <= obj.RVO_EPSILON) 
                            right_size = right_size + 1;
                        else 
                            left_size = left_size + 1;
                            right_size = right_size + 1;
                        end
                        
                        size_pair = [max(left_size, right_size), min(left_size, right_size)];
                        min_pair = [max(min_left, min_right), min(min_left, min_right)];
                        % OI does a comparison of pairs here, decided to copy the actual implementation of <= operator in c++ here
                        if ~(size_pair(1) < min_pair(1) || (~(min_pair(1) < size_pair(1)) && size_pair(2) < min_pair(2)))
                            break;
                        end
                    end % for every obstacle j

                    size_pair = [max(left_size, right_size), min(left_size, right_size)];
                    min_pair = [max(min_left, min_right), min(min_left, min_right)];
                    if size_pair(1) < min_pair(1) || (~(min_pair(1) < size_pair(1)) && size_pair(2) < min_pair(2))
                        min_left = left_size;
                        min_right = right_size;
                        optimal_split = i;
                    end
                end % for every obstacle i

                % Build split node.
                left_obstacles = Obstacle.empty(min_left, 0);
                right_obstacles = Obstacle.empty(min_right, 0);

                % used as indices, start with 1 instead of 0 in OI
                left_counter = 1;    
                right_counter = 1;
                i = optimal_split;

                obstacle_I1 = obstacles(i);
                obstacle_I2 = obstacle_I1.next_obstacle;

                for j = 1:numel(obstacles)
                    if i == j
                        continue;
                    end

                    obstacle_J1 = obstacles(j);
                    obstacle_J2 = obstacle_J1.next_obstacle;

                    J1_leftOf_I = leftOf(obstacle_I1.point, obstacle_I2.point, obstacle_J1.point);
                    J2_leftOf_I = leftOf(obstacle_I1.point, obstacle_I2.point, obstacle_J2.point);

                    if (J1_leftOf_I >= -obj.RVO_EPSILON && J2_leftOf_I >= -obj.RVO_EPSILON) 
                        left_obstacles(left_counter) = obstacles(j);
                        left_counter = left_counter + 1;
                    
                    elseif J1_leftOf_I <= obj.RVO_EPSILON && J2_leftOf_I <= obj.RVO_EPSILON
                        right_obstacles(right_counter) = obstacles(j);
                        right_counter = right_counter + 1;
                    
                    else
                        % Split obstacle j
                        t = det(obstacle_I2.point - obstacle_I1.point, obstacle_J1.point - obstacle_I1.point) ...
                          / det(obstacle_I2.point - obstacle_I1.point, obstacle_J1.point - obstacle_J2.point);

                        splitpoint = obstacle_J1.point + t * (obstacle_J2.point - obstacle_J1.point);

                        new_obstacle = Obstacle();
                        new_obstacle.point = splitpoint;
                        new_obstacle.prev_obstacle = obstacle_J1;
                        new_obstacle.next_obstacle = obstacle_J2;
                        new_obstacle.isConvex = true;
                        new_obstacle.unitDir = obstacle_J1.unitDir;

                        new_obstacle.id = numel(obstacles);

                        % OI uses push_back into attribute obstacles_ of sim instance here
                        obstacles = [obstacles, new_obstacle];

                        obstacle_J1.next_obstacle = new_obstacle;
                        obstacle_J2.prev_obstacle = new_obstacle;

                        if J1_leftOf_I > 0
                            left_obstacles(left_counter) = obstacle_J1;
                            left_counter = left_counter + 1;
                            right_obstacles(right_counter) = new_obstacle;
                        else
                            right_obstacles(right_counter) = obstacle_J1;
                            right_counter = right_counter + 1;
                            left_obstacles(left_counter) = new_obstacle;
                        end
                    end
                end

                node.obstacle = obstacle_I1;
                node.left = obj.buildObstacleTreeRecursive(left_obstacles);
                node.right = obj.buildObstacleTreeRecursive(right_obstacles);
                return;
            end % else obstacles not empty
        end % function buildObstacleTreeRecursive
        
        function computeAgentNeighbors(obj, agent, range_sq)
            % COMPUTEAGENTNEIGHBORS - Computes the agent neighbors of the
            % specified agent.
            %
            % agent: The agent for which agent neighbors are to be computed.
            % range_sq: The squared range around the agent.
            
            obj.queryAgentTreeRecursive(agent, range_sq, 1);
        end % function computeAgentNeighbors
        
        function computeObstacleNeighbors(obj, agent, range_sq)
            % COMPUTEOBSTACLENEIGHBORS - Computes the obstacle neighbors of 
            % the specified agent.
            %
            % agent: The agent for which obstacles are to be computed.
            % range_sq: The squared range around the agent.
            
            obj.queryObstacleTreeRecursive(agent, range_sq, obj.obstacle_tree);
        end % function computeObstacleNeighbors
        
        function range_sq = queryAgentTreeRecursive(obj, agent, range_sq, node) 
            
            if (obj.agent_tree(node).n_end - obj.agent_tree(node).begin <= obj.MAX_LEAF_SIZE)
                for i = obj.agent_tree(node).begin : obj.agent_tree(node).n_end
                    range_sq = agent.insertAgentNeighbor(i, range_sq);
                end
            else
                x_agent = agent.position(1);
                y_agent = agent.position(2);
                dist_sq_left = (max(0, obj.agent_tree(obj.agent_tree(node).left).x_min - x_agent))^2 + ...
                               (max(0, x_agent - obj.agent_tree(obj.agent_tree(node).left).x_max))^2 + ...
                               (max(0, obj.agent_tree(obj.agent_tree(node).left).y_min - y_agent))^2 + ...
                               (max(0, y_agent - obj.agent_tree(obj.agent_tree(node).left).y_max))^2;

                dist_sq_right = (max(0, obj.agent_tree(obj.agent_tree(node).right).x_min - x_agent))^2 + ...
                                (max(0, x_agent - obj.agent_tree(obj.agent_tree(node).right).x_max))^2 + ...
                                (max(0, obj.agent_tree(obj.agent_tree(node).right).y_min - y_agent))^2 + ...
                                (max(0, y_agent - obj.agent_tree(obj.agent_tree(node).right).y_max))^2;

                if (dist_sq_left < dist_sq_right)
                    if (dist_sq_left < range_sq)
                        obj.queryAgentTreeRecursive(agent, range_sq, obj.agent_tree(node).left);

                        if (dist_sq_right < range_sq)
                            obj.queryAgentTreeRecursive(agent, range_sq, obj.agent_tree(node).right);
                        end
                    end
                else
                    if (dist_sq_right < range_sq)
                        range_sq = obj.queryAgentTreeRecursive(agent, range_sq, obj.agent_tree(node).right);

                        if (dist_sq_left < range_sq)
                            range_sq = obj.queryAgentTreeRecursive(agent, range_sq, obj.agent_tree(node).left);
                        end
                    end
                end % if dist_sq_left < dist_sq_right

            end % if MAX_LEAF_SIZE reached
        end % function queryAgentTreeRecursive
        
        function range_sq = queryObstacleTreeRecursive(obj, agent, range_sq, node)
            if numel(node) == 0
                return;
            else
                obstacle1 = node.obstacle;
                obstacle2 = obstacle1.next_obstacle;

                agent_leftOf_line = leftOf(obstacle1.point, obstacle2.point, agent.position);

                if agent_leftOf_line >= 0
                    obj.queryObstacleTreeRecursive(agent, range_sq, node.left);
                else
                    obj.queryObstacleTreeRecursive(agent, range_sq, node.right);
                end

                dist_sq_line = agent_leftOf_line^2 / norm(obstacle2.point - obstacle1.point)^2;

                if dist_sq_line < range_sq
                    if agent_leftOf_line < 0
                        % Try obstacle at this node only if agent is on right side of obstacle (and can see obstacle).
                        agent.insertObstacleNeighbor(node.obstacle, range_sq);
                    end
                    %Try other side of line.
                    if agent_leftOf_line >= 0
                        range_sq = obj.queryObstacleTreeRecursive(agent, range_sq, node.right);
                    else
                        range_sq = obj.queryObstacleTreeRecursive(agent, range_sq, node.left);
                    end

                end % if distSQLine < range_sq
            end % node not null
        end % function queryObstacleTreeRecursive
        
    end % methods
    
end % class

