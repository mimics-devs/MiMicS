classdef Obstacle < handle
    %OBSTACLE Contains the obstacle class.
    %   RVO2 uses polygons as obstacles. Vertices should be specified in
    %   counterclockwise order, unless it is a "negative" obstacle (i.e. a
    %   wall around the environment).
    %   This class actually does not contain an entire polygon, but only a
    %   single vertex with pointers to the next and previous obstacles.
    %
    %   Adding of such an obstacle to the simulation is done with the
    %   "addObstacle" function.
    
    properties
        id              
        point               % coordinates of this vertex
        next_obstacle
        prev_obstacle
        unit_dir            % vector to next vertex of unit length
        is_convex           % 1 if convex, 0 otherwise
    end
    
    methods
        function obj = Obstacle()
            % Constructor
            %
            % Creates an instance of this class
            %
            id = 0;
            is_convex = 0;
            next_obstacle = [];
            prev_obstacle = [];
        end
    end
    
end

