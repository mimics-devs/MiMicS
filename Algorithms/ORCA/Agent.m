classdef Agent < handle
% Agent class. Performsthe actual computations to find a new reference
% velocity, given the current preferred (desired) velocity, current
% velocity and position and obstacles. Some attributes are copied from our
% RobotModel.

% functions the original implementation (OI) uses
% abs(vector) - computes length of a 2d-vector
% absSq(vector) - computes squared vector length
% det(vector1, vector2) - computes determined of 2x2 matrix, with ROWS consisting of the the specified 2d-vecotrs
% normalize(vector) - normalizes vector
% sqr(x) - squares x
% vector*vector refers to the scalar (dot) product of the two vectors
      
    properties
        kdTree;                             % Reference to the KdTree
        comm;                             % Reference to the CommunicationSystem (necessary for insertAgentNeighbor etc)
        delta_t = 0.1;
        
        max_num_neighbors = 100;            % The maximum number of other agents the agent takes into account in the navigation. The larger this number, the longer the running time of the simulation. If the number is too low, the simulation will not be safe.
        max_dist_neighbor = 15;             % The maximum distance (center point to center point) to other agents the agent takes into account in the navigation. The larger this number, the longer the running time of the simulation. If the number is too low, the simulation will not be safe. Must be non-negative.
        radius;                             % The radius of the agent. Must be non-negative. NOTE: We will work with this for the time being, maybe later adapt the algorithm to non-circular robots.
        time_horizon = 5;                   % The minimal amount of time for which the agent's velocities that are computed by the simulation are safe with respect to other agents. The larger this number, the sooner this agent will respond to the presence of other agents, but the less freedom the agent has in choosing its velocities. Must be positive.
        time_horizon_obst = 5;              % The minimal amount of time for which the agent's velocities that are computed by the simulation are safe with respect to obstacles. The larger this number, the sooner this agent will respond to the presence of obstacles, but the less freedom the agent has in choosing its velocities. Must be positive.
        
        obstacle_neighbors = [];            % cell array, one row per {dist, obstacle}-pair
        agent_neighbors = [];               % cell array, one row per {dist, agent}-pair
        orca_lines = [];                    % cell array, one row per {point, direction}-pair
        
        % these attributes are shared with the class RobotModel, although
        % the velocities are represented as 2d cartesian vectors instead of
        % angle (phi) plus magnitude (v)
        id;
        position;
        velocity;
        velocity_pref;
        max_speed;
    end % properties
    
    properties (Constant)
        RVO_EPSILON = 0.00001;
    end % Constants
    
    methods
        
        function obj = Agent()
            % Constructor
        end
        
        function computeNeighbors(obj)
            % COMPUTENEIGHBORS Computes the neighbors (obstacles and other agents) of this agent.
            
            obj.obstacle_neighbors = [];
            range_sq = (obj.time_horizon_obst * obj.max_speed + obj.radius)^2;
            obj.kdTree.computeObstacleNeighbors(obj, range_sq);
            
            obj.agent_neighbors = [];
            if obj.max_num_neighbors > 0
                range_sq = obj.max_dist_neighbor^2;
                obj.kdTree.computeAgentNeighbors(obj, range_sq);
            end
        end % function computeNeighbors
        
        function [v_ref, phi_ref] = computeNewVelocity(obj)
            % COMPUTENEWVELOCITY Computes the new velocity for this agent
            % (core part of the algorithm).
            % v_ref, phi_ref: Follow the preferred velocity as closely as 
            % possible, but at the same time guarantee collision avoidance
            % for a fixed time window.
               
            obj.orca_lines = [];
            
            % Create obstacle ORCA lines
            inv_time_horizon_obst = 1 / obj.time_horizon_obst;
            
            for i= 1:size(obj.obstacle_neighbors,1)
                obstacle1 = obj.obstacle_neighbors{i,2};
                obstacle2 = obstacle1.next_obstacle;
                
                relative_pos_1 = obstacle1.point - obj.position;
                relative_pos_2 = obstacle2.point - obj.position;
                
                % Check if velocity obstacle of obstacle is already taken care of
                % by previously constructed obstacle ORCA lines.
                is_already_covered = false;
                
                for j = 1:size(obj.orca_lines, 1)
                    point = obj.orca_lines{j,1};
                    dir = obj.orca_lines{j,2};
                    
                    if (det([(inv_time_horizon_obst * relative_pos_1 - point); dir]) - inv_time_horizon_obst*obj.radius >= -obj.RVO_EPSILON ...
                            && det([(inv_time_horizon_obst * relative_pos_2 - point); dir]) - inv_time_horizon_obst*obj.radius >= -obj.RVO_EPSILON)
                        is_already_covered = true;
                        break;
                    end
                end % for every previous ORCA line
                
                if is_already_covered
                    continue;
                end
                
                % Not yet covered. Check for collisions.
                
                dist_sq_1 = norm(relative_pos_1)^2;
                dist_sq_2 = norm(relative_pos_2)^2;
                
                radius_sq = obj.radius^2;
                
                obstacle_vector = obstacle2.point - obstacle1.point;
                s = dot(-relative_pos_1, obstacle_vector) / norm(obstacle_vector)^2;
                dist_sq_line = norm(-relative_pos_1 - s * obstacle_vector)^2;
                
                if (s < 0 && dist_sq_1 <= radius_sq)
                    % Collision with left vertex. Ignore if non-convex
                    if obstacle1.is_convex
                        line_point = [0 0];
                        line_direction = [-relative_pos_1(2) relative_pos_1(1)];
                        line_direction = line_direction/norm(line_direction);
                        obj.orca_lines = [obj.orca_lines; {line_point, line_direction}];
                    end
                    
                    continue;
                    
                elseif (s > 1 && dist_sq_2 <= radius_sq)
                    % Collision with right vertex. Ignore if non-convex or if it
                    % will be taken care of by neighboring obstacle
                    
                    if obstacle2.is_convex && det([relative_pos_2; obstacle2.unit_dir]) >= 0       % obstacle2->is_convex && ...
                        line_point = [0 0];
                        line_direction = [-relative_pos_2(2) relative_pos_2(1)];
                        line_direction = line_direction/norm(line_direction);
                        obj.orca_lines = [obj.orca_lines; {line_point, line_direction}];
                    end
                    
                    continue;
                    
                elseif (s >= 0 && s < 1 && dist_sq_line <= radius_sq)
                    % Collision with obstacle segment
                    line_point = [0 0];
                    line_direction = -obstacle1.unit_dir;
                    obj.orca_lines = [obj.orca_lines; {line_point, line_direction}];
                    
                    continue;
                end
                
                % No collision.
                % Compute legs. When obliquely viewed, both legs can come from a
                % single vertex. Legs extend cut-off line when nonconx vertex.
                
                left_leg_direction = [0 0];
                right_leg_direction = [0 0];
                
                if (s < 0 && dist_sq_line <= radius_sq)
                    % Obstacle viewed obliquely so that left vertex defines
                    % velocity obstacle.
                    
                    if ~obstacle1.is_convex
                        % Ignore obstacle.
                        continue;
                    end
                    
                    obstacle2 = obstacle1;
                    
                    leg1 = sqrt(dist_sq_1 - radius_sq);
                    left_leg_direction = [relative_pos_1(1)*leg1 - relative_pos_1(2)*obj.radius, relative_pos_1(1)*obj.radius + relative_pos_1(2)*leg1] / dist_sq_1;
                    right_leg_direction = [relative_pos_1(1)*leg1 + relative_pos_1(2)*obj.radius, -relative_pos_1(1)*obj.radius + relative_pos_1(2)*leg1] / dist_sq_1;
                    
                elseif (s > 1 && dist_sq_line <= radius_sq)
                    % Obstacle viewed obliquely so that right vertex defines
                    % velocity obstacle.
                    
                    if ~obstacle2.is_convex                      % !obstacle2->is_convex
                        % Ignore obstacle.
                        continue;
                    end
                    
                    obstacle1 = obstacle2;
                    
                    leg2 = sqrt(dist_sq_2 - radius_sq);
                    left_leg_direction = [relative_pos_2(1)*leg2 - relative_pos_2(2)*obj.radius, relative_pos_2(1)*obj.radius + relative_pos_2(2)*leg2] / dist_sq_2;
                    right_leg_direction = [relative_pos_2(1)*leg2 + relative_pos_2(2)*obj.radius, -relative_pos_2(1)*obj.radius + relative_pos_2(2)*leg2] / dist_sq_2;
                    
                else
                    % Usual situation
                    
                    if obstacle1.is_convex
                        leg1 = sqrt(dist_sq_1 - radius_sq);
                        left_leg_direction = [relative_pos_1(1)*leg1 - relative_pos_1(2)*obj.radius, relative_pos_1(1)*obj.radius + relative_pos_1(2)*leg1] / dist_sq_1;
                    else
                        % Left vertex non-convex; left leg extends cuf-off line.
                        left_leg_direction = -obstacle1.unit_dir;
                    end
                    
                    if obstacle2.is_convex
                        leg2 = sqrt(dist_sq_2 - radius_sq);
                        right_leg_direction = [relative_pos_2(1)*leg2 + relative_pos_2(2)*obj.radius, -relative_pos_2(1)*obj.radius + relative_pos_2(2)*leg2] / dist_sq_2;
                    else
                        % Right vertex non-convex; right leg extends cuf-off line.
                        right_leg_direction = obstacle1.unit_dir;
                    end
                end
                
                % Legs can never point into neighboring edge when convex vertex,
                % take cutoff-line of neighboring edge instead. If velocity
                % projected on "foreign" leg, no constraint is added.
                
                left_neighbor = obstacle1.prev_obstacle;
                
                is_left_leg_foreign = false;
                is_right_leg_foreign = false;
                if obstacle1.is_convex && det([left_leg_direction; -left_neighbor.unit_dir]) >= 0  % && obstacle1->is_convex
                    % Left leg points into obstacle.
                    left_leg_direction = -left_neighbor.unit_dir;
                    is_left_leg_foreign = true;
                end
                if obstacle2.is_convex && det([right_leg_direction; obstacle2.unit_dir]) <= 0
                    % Right leg points into obstacle.
                    right_leg_direction = obstacle2.unit_dir;
                    is_right_leg_foreign = true;
                end
                
                % Compute cut-off centers.
                left_cutoff = inv_time_horizon_obst * (obstacle1.point - obj.position);
                right_cutoff = inv_time_horizon_obst * (obstacle2.point - obj.position);
                cutoffVec = right_cutoff - left_cutoff;
                
                % Project current velocity on velocity obstacle.
                
                % Check if current velocity is projected on cutoff circles.
                if all(obstacle1.point == obstacle2.point)
                    t = 0.5;
                else
                    t = dot((obj.velocity - left_cutoff), cutoffVec) / norm(cutoffVec)^2;
                end
                t_left = dot((obj.velocity - left_cutoff), left_leg_direction);
                t_right = dot((obj.velocity - right_cutoff), right_leg_direction);
                
                if (t < 0 && t_left < 0) || (all(obstacle1.point == obstacle2.point) && t_left < 0 && t_right < 0)
                    % Project on left cut-off circle.
                    unit_w = obj.velocity - left_cutoff;
                    unit_w = unit_w/norm(unit_w);
                    line_direction = [unit_w(2) -unit_w(1)];
                    line_point = left_cutoff + obj.radius * inv_time_horizon_obst * unit_w;
                    obj.orca_lines = [obj.orca_lines; {line_point, line_direction}];
                    continue;
                elseif (t > 1 && t_right < 0)
                    % Project on right cut-off circle.
                    unit_w = obj.velocity - right_cutoff;
                    unit_w = unit_w/norm(unit_w);
                    line_direction = [unit_w(2) -unit_w(1)];
                    line_point = right_cutoff + obj.radius * inv_time_horizon_obst * unit_w;
                    obj.orca_lines = [obj.orca_lines; {line_point, line_direction}];
                    continue;
                end
                
                % Project on left leg, right leg, or cut-off line, whichever is
                % closest to velocity.
                
                if t < 0 || t > 1 || all(obstacle1.point == obstacle2.point)
                    dist_sq_cutoff = Inf;
                else
                    dist_sq_cutoff = norm(obj.velocity - (left_cutoff + t * cutoffVec))^2;
                end
                if t_left < 0
                    dist_sq_left = Inf;
                else
                    dist_sq_left = norm(obj.velocity - (left_cutoff + t_left * left_leg_direction))^2;
                end
                if t_right < 0
                    dist_sq_right = Inf;
                else
                    dist_sq_right = norm(obj.velocity - (right_cutoff + t_right * right_leg_direction))^2;
                end
                
                if dist_sq_cutoff <= dist_sq_left && dist_sq_cutoff <= dist_sq_right
                    % Project on cut-off line
                    line_direction = -obstacle1.unit_dir;
                    line_point = left_cutoff + obj.radius * inv_time_horizon_obst * [-line_direction(2), line_direction(1)];
                    obj.orca_lines = [obj.orca_lines; {line_point, line_direction}];
                    continue;
                elseif dist_sq_left <= dist_sq_right
                    % Project on left leg.
                    if is_left_leg_foreign
                        continue;
                    end
                    line_direction = left_leg_direction;
                    line_point = left_cutoff + obj.radius * inv_time_horizon_obst * [-line_direction(2), line_direction(1)];
                    obj.orca_lines = [obj.orca_lines; {line_point, line_direction}];
                    continue;
                else
                    % Project on right leg.
                    if is_right_leg_foreign
                        continue;
                    end
                    line_direction = -right_leg_direction;
                    line_point = right_cutoff + obj.radius * inv_time_horizon_obst * [-line_direction(2), line_direction(1)];
                    obj.orca_lines = [obj.orca_lines; {line_point, line_direction}];
                    continue;
                end
                
            end % for every obstacle line
            
            num_obst_lines = size(obj.orca_lines, 1);
            
            % Create agent ORCA lines
            
            inv_time_horizon = 1.0 / obj.time_horizon;
            for i = 1:size(obj.agent_neighbors,1)
                other = obj.agent_neighbors{i,2};
                [x_other, y_other] = obj.comm.getPosition(other);
                v_other = obj.comm.getVelocity(other);
                phi_other = obj.comm.getOrientation(other);
                
                relative_position = [x_other - obj.position(1), y_other - obj.position(2)];
                % velocities need to be represented as 2-dimensional vectors
                velocity_other = v_other .* [cos(phi_other), sin(phi_other)];
                relative_velocity = obj.velocity - velocity_other;
                dist_sq = norm(relative_position)^2;
                
                % TODO (someday): Incorporate correct shape of other robot
                combined_radius = obj.radius + obj.radius;
                combined_radius_sq = combined_radius^2;
                
                if dist_sq > combined_radius_sq
                    % No collision
                    w = relative_velocity - (inv_time_horizon*relative_position);
                    w_length = norm(w);
                    dost_product_1 = dot(w, relative_position);
                    
                    if dost_product_1 < 0 && dost_product_1^2 > (combined_radius_sq * w_length^2)
                        % Project on cut-off circle
                        unit_w = w/w_length;
                        
                        %                   y          -x
                        line_direction = [unit_w(2), -unit_w(1)];
                        u = (combined_radius * inv_time_horizon - w_length) * unit_w;
                    else
                        % Project on legs
                        leg = sqrt(dist_sq - combined_radius_sq);
                        
                        if det([relative_position; w]) > 0
                            % Project on left leg
                            line_direction =  [relative_position(1) * leg - relative_position(2) * combined_radius, relative_position(1) * combined_radius + relative_position(2) * leg] / dist_sq;
                        else
                            % Project on right leg
                            line_direction = -[relative_position(1) * leg + relative_position(2) * combined_radius, -relative_position(1) * combined_radius + relative_position(2) * leg] / dist_sq;
                        end
                        
                        dot_product_2 = dot(relative_velocity, line_direction);
                        u = dot_product_2 * line_direction - relative_velocity;
                    end % if project on cut-off circle or else on legs
                    
                else
                    % Collision. Project on cut-off circle of time timeStep
                    inv_time_step = 1 / obj.delta_t;
                    
                    % Vector from cutoff center to relative velocity
                    w = relative_velocity - inv_time_step * relative_position;
                    w_length = norm(w);
                    unit_w = w / w_length;
                    
                    line_direction = [unit_w(2), -unit_w(1)];
                    u = (combined_radius * inv_time_step - w_length) * unit_w;
                end % if collision
                
                line_point = obj.velocity + 0.5*u;
                obj.orca_lines = [obj.orca_lines; {line_point, line_direction}];
            end % for every neighbor
            
            % Calculate new velocity
            
            new_velocity = [0 0];
            
            [line_fail, new_velocity] = obj.linearProgram2(obj.orca_lines, obj.max_speed, obj.velocity_pref, false, new_velocity);
            
            if line_fail < size(obj.orca_lines, 1)
                new_velocity = obj.linearProgram3(obj.orca_lines, num_obst_lines, line_fail, obj.max_speed, new_velocity);
            end
            
            [phi_ref, v_ref] = cart2pol(new_velocity(1), new_velocity(2));
            
        end
        
        function range_sq = insertAgentNeighbor(obj, agent, range_sq)
            % INSERTAGENTNEIGHBOR Inserts an agent neighbor into the set of
            % neighbors of this agent.
            %
            % Works in combination with computeAgentNeighbors of class
            % KdTree.
            %
            % agent: Agent to be inserted.
            % range_sq: The squared range around this agent.
            %
            % Adaptation: Only the id of the other robot (agent) is passed,
            % actual position is later requested from comm.
            
            if (obj.id ~= agent)
                other_position = [];
                [other_position(1), other_position(2)] = obj.comm.getPosition(agent);
                dist_sq = (norm(obj.position - other_position)).^2;

                if (dist_sq < range_sq)
                    if size(obj.agent_neighbors,1) < obj.max_num_neighbors
                        obj.agent_neighbors = [obj.agent_neighbors; {dist_sq, agent}];
                    end

                    i = size(obj.agent_neighbors,1);

                    while (i ~= 1 && dist_sq < obj.agent_neighbors{i-1,1})
                        obj.agent_neighbors{i,1} = obj.agent_neighbors{i-1,1};
                        obj.agent_neighbors{i,2} = obj.agent_neighbors{i-1,2};
                        i = i - 1;
                    end

                    obj.agent_neighbors{i,1} = dist_sq;
                    obj.agent_neighbors{i,2} = agent;

                    if size(obj.agent_neighbors,1) == obj.max_num_neighbors 
                        range_sq = obj.agent_neighbors{end,1};
                    end
                end
            end
        end % function insertAgentNeighbor
        
        function insertObstacleNeighbor(obj, obstacle, range_sq)
            % Inserts a static obstacle neighbor into the set of neighbors
            % of this agent.
            %
            % Works in combination with computeObstacleNeighbors of class
            % KdTree.
            %
            % obstacle: The static obstacle to be inserted.
            % range_sq: The squared range around this agent.
            
            next_obstacle = obstacle.next_obstacle;

            dist_sq = distSqPointLineSegment(obstacle.point, next_obstacle.point, obj.position);

            if (dist_sq < range_sq)
                obj.obstacle_neighbors = [obj.obstacle_neighbors; {dist_sq, obstacle}];

                i = size(obj.obstacle_neighbors,1);

                while (i ~= 1 && dist_sq < obj.obstacle_neighbors{i - 1,1})
                    obj.obstacle_neighbors{i,1} = obj.obstacle_neighbors{i-1,1};
                    obj.obstacle_neighbors{i,2} = obj.obstacle_neighbors{i-1,2};
                    i = i - 1;
                end

                obj.obstacle_neighbors{i,1} = dist_sq;
                obj.obstacle_neighbors{i,2} = obstacle;
            end
        end % function insertObstacleNeighbor
        
        function [success, result] = linearProgram1(obj, lines, line_nr, radius, opt_velocity, direction_opt, result)
            % LINEARPROGRAM1 Solves a one-dimensional linear program on a
            % specified line subject to linear constraints defined by lines
            % and a circular constraint.
            %
            % lines: Lines defining the linear constarints.
            % line_nr: The specified line constraint.
            % radius: The radius of the circular constraint.
            % opt_velocity: The optimization velocity.
            % direction_opt: True if the direction should be optimized.
            % 
            % result: A reference to the result of the linear program.
            % success: True if successful.
            
            line_point = lines{line_nr,1};
            line_direction = lines{line_nr,2};
            
            dotprocuct = dot(line_point, line_direction);
            discriminant = dotprocuct^2 + radius^2 + norm(line_point)^2;
            
            if discriminant < 0
                % Max speed circle fully invalidates line line_nr
                success = false;
                return;
            end
            
            sqrt_discriminant = sqrt(discriminant);
            t_left = -dotprocuct - sqrt_discriminant;
            t_right = -dotprocuct + sqrt_discriminant;
            
            for i = 1:line_nr-1
                other_line_point = lines{i,1};
                other_line_direction = lines{i,2};
                
                denominator = det([line_direction; other_line_direction]);
                numerator = det([other_line_direction; line_point - other_line_point]);
                
                if abs(denominator) <= obj.RVO_EPSILON
                    % Lines line_nr and i are (almost) parallel
                    if numerator < 0
                        success = false;
                        return;
                    else
                        continue;
                    end
                end % if lines are (almost) parallel
                
                t = numerator / denominator;
                
                if denominator >= 0
                    % Line i bounds line line_nr on the right.
                    t_right = min(t_right, t);
                else
                    % Line i bounds line line_nr on the left.
                    t_left = max(t_left, t);
                end
                
                if t_left > t_right
                    success = false;
                    return;
                end
            end % for i = 1:line_nr-1
            
            if direction_opt
                % Optimize direction
                if dot(opt_velocity, line_direction) > 0
                    % Take the right extreme.
                    result = line_point + t_right * line_direction;
                else
                    % Take the left extreme.
                    result = line_point + t_left * line_direction;
                end
            else
                % Optimize closest point.
                t = dot(line_direction, (opt_velocity - line_point));
                
                if t < t_left
                    result = line_point + t_left * line_direction;
                elseif t > t_right
                    result = line_point + t_right * line_direction;
                else
                    result = line_point + t*line_direction;
                end
            end % if direction opt
            
            success = true;
        end % function linearProgram1
        
        function [line_fail, result] = linearProgram2(obj, lines, radius, opt_velocity, direction_opt, result)
            % LINEARPROGRAM2 Solves a two-dimensional linear program
            % subject to linear constraints defined by lines and a circular
            % constraint.
            %
            % lines: Lines defining the linear constraints.
            % radius: The radius of the circular constraint.
            % opt_velocity: The optimization velocity.
            % direction_opt: True if the direction should be optimized.
            %
            % result: Result of the linear program.
            % line_fail: The number of the line it fails on, and the number
            % of lines if successful.
            
            num_lines = size(lines, 1);
            
            if direction_opt
                % Optimize direction. Note that the optimization velocity is of
                % unit length in this case.
                result = opt_velocity * radius;
            elseif norm(opt_velocity)^2 > radius^2
                % Optimize closest point and outside circle.
                result = opt_velocity/norm(opt_velocity) * radius;
            else
                % Optimize closest point and inside circle.
                result = opt_velocity;
            end % if direction_opt
            
            for i = 1:num_lines
                line_point = lines{i,1};
                line_direction = lines{i,2};
                if det([line_direction; line_point - result]) > 0
                    % Resulst does not satisfy constraint i. Compute new optimal
                    % result.
                    temp_result = result;
                    
                    [lp1_success, result] = obj.linearProgram1(lines, i, radius, opt_velocity, direction_opt, result);
                    
                    if ~lp1_success
                        result = temp_result;
                        line_fail = i;
                        return;
                    end
                    
                end % if constraint is satisfied
                
            end % for every line
            
            line_fail = num_lines;
        end % function linearProgram2
        
        function result =  linearProgram3(obj, lines, numObstLines, beginLine, radius, result)
            % LINEARPROGRAM3 Solves a two-dimensional linear program
            % subject to linear constraints defined by lines and a circular
            % constraint.
            %
            % lines: Lines defining the linear constraints.
            % numObstLines: Count of obstacle lines.
            % beginLine: The line on which the 2-d linear program failed.
            % radius: The radius of the circular constraint.
            % direction_opt: True if the direction should be optimized.
            %
            % result: Result of the linear program.
            
            distance = 0;
            num_lines = size(lines,1);
            
            for i = beginLine:num_lines
                
                line_point = lines{i,1};
                line_direction = lines{i,2};
                
                if det([line_direction; line_point - result]) > distance
                    % Result does not satisfy constraint of line i.
                    proj_lines = lines(1:numObstLines,:);
                    
                    for j = numObstLines+1:i-1
                        obstLinePoint = lines{j,1};
                        obstLineDirection = lines{j,2};
                        
                        determinant = det([line_direction; obstLineDirection]);
                        
                        if abs(determinant) < obj.RVO_EPSILON
                            % Line i and j are parallel.
                            if dot(line_direction, obstLineDirection) > 0
                                % Line i and line j point in the same direction.
                                continue;
                            else
                                % Line i and line j point in opposite direction.
                                new_linepoint = 0.5 * (line_point + obstLinePoint);
                            end % if lines point in the same direction
                        else
                            new_linepoint = line_point + (det([obstLineDirection; line_point - obstLinePoint]) / determinant) * line_direction;
                        end % if lines are parallel
                        
                        new_line_direction = obstLineDirection - line_direction;
                        new_line_direction = new_line_direction/norm(new_line_direction);
                        
                        proj_lines = [proj_lines; {new_linepoint, new_line_direction}];
                        
                    end % for every obstacle line
                    
                    temp_result = result;
                    
                    [line_fail, result] = obj.linearProgram2(proj_lines, radius, [-line_direction(2), line_direction(1)], true, result);
                    if line_fail < size(proj_lines,1)
                        % This should in principle not happen. The result ist by
                        % definition already in the feasible region of this linear
                        % program. If it fails, it is due to small floating point
                        % error, and the current result is kept.
                        result = temp_result;
                    end
                    
                    distance = det([line_direction; line_point-result]);
                end % if result does not satisfy constraint of line i
            end % for i = beginLine:num_lines
            
        end % function linearProgram3
        
    end % methods
    
end % classdef
