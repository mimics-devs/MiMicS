function [obstacle_no, obstacles] = addObstacle(obstacles, vertices)
% ADDOBSTACLE - Adds a new obstacle in the format ORCA requires. Normally
% polygons, but simple lines are also possible.
%
% vertices: List of vertices of the polygonal obstacle in counterclockwise
% order. For a "negative" obstacle, e.g. a bounding polygon around the
% environment, the vertices should be listed in clockwise order.
% obstacles: The data structure to add this obstacle to
% obstacle_no: The number of the first vertex of the obstacle, or -1 when 
% the number of vertices is less than two.

    if size(vertices, 1) < 2 || size(vertices, 2) < 2
        fprintf('Error in addObstacle: Wrong format of parameter vertices');
        obstacle_no = -1;
        return
    end
    
    % obstacles_.size() in C++, +1 since indexing in MATLAB starts with 1 not 0
    obstacle_no = numel(obstacles) + 1; 
    
    for i = 1:size(vertices, 1)
        obstacle = Obstacle();
        obstacle.point = vertices(i, :);
        
        if i ~= 1
            obstacle.prev_obstacle = obstacles(end);
            obstacle.prev_obstacle.next_obstacle = obstacle;
        end
        
        if i == size(vertices, 1)
            obstacle.next_obstacle = obstacles(obstacle_no);
            obstacle.next_obstacle.prev_obstacle = obstacle;
        end
        
        if i == size(vertices, 1) 
            obstacle.unit_dir = vertices(1,:) - vertices(i,:);
        else
            obstacle.unit_dir = vertices(i+1,:) - vertices(i,:);
        end
        obstacle.unit_dir = obstacle.unit_dir/norm(obstacle.unit_dir);
        
        if size(vertices, 1) == 2
            obstacle.is_convex = 1;
        else
            %obstacle->isConvex_ = (leftOf(vertices[(i == 0 ? vertices.size() - 1 : i - 1)], vertices[i], vertices[(i == vertices.size() - 1 ? 0 : i + 1)]) >= 0.0f);
            
            if i == 1
                lo = leftOf(vertices(end,:), vertices(i,:), vertices(i+1,:));
            elseif i == size(vertices,1)
                lo = leftOf(vertices(i-1,:), vertices(i,:), vertices(1,:));
            else
                lo = leftOf(vertices(i-1,:), vertices(i,:), vertices(i+1,:));
            end
            
            if lo > 0 
                obstacle.is_convex = 1;
            else
                obstacle.is_convex = 0;
            end
        end
        
        obstacle.id = numel(obstacles) + 1;
        obstacles = [obstacles, obstacle];
    end % for every vertice
end %function