classdef ObstacleTreeNode < handle
    %OBSTACLETREENODE Defines an obstacle kd-tree node.
    
    properties
        % The left obstacle tree node.
        left
        % The obstacle number.
        obstacle
        % The right obstacle tree node.
        right
    end
    
    methods   
    end
    
end

