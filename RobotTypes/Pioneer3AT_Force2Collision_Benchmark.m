% Pioneer3AT variant for use with the cave benchmark

sptr_dyn = tf(1, [0 1 0]);    % 0.5 1 0
steer_dyn = tf(1, [0.0001 1]);
v_pid = [0 0 0];%[22.4749 7.0996 11.2515];
phi_pid = [20 0 3];
v_max = 1;
v_min = -1;
omega_max = 1;
vehicle_width = 0.24;
vehicle_length = 0.4;
safety_radius = sqrt( (vehicle_width/2)^2 + (vehicle_length/2)^2) * 1.1;
min_steer_radius = @(v) 1*v;

ranger_minmax = [0.1 8];
ranger_fov = 180;
ranger_num_beams = 180;

noise_pos = 0;
noise_odom_pos = 0;
noise_v = 0;
noise_a = 0;
noise_phi = 0;
noise_odom_phi = 0;
noise_ranger_mean = 0;
noise_ranger_stddev = 0;
