% ROBOT TYPE DEFINITION FOR SIMULATOR

% 'Pioneer3AT with noise for tuning parameter values of algorithms
sptr_dyn = tf(1, [0.5 1 0]);
steer_dyn = tf(1, [0.2 1]);
v_pid = [22.4749 7.0996 11.2515];
phi_pid = [20 0 3];
v_max = 1;
v_min = -1;
omega_max = 1;
vehicle_width = 0.5;
vehicle_length = 0.7;
safety_radius = 0.44;
min_steer_radius = @(v) 1*v;

ranger_minmax = [0.1 30];
ranger_fov = 270;
ranger_num_beams = 270;

noise_pos = 0.05;
noise_odom_pos = 0.01;      %TODO Get more realistic value
noise_v = 0.05;
noise_a = 0.05;
noise_phi = 0.0873;    % 5°
noise_odom_phi = 0.0175;
noise_ranger_mean = 0;
noise_ranger_stddev = 0.05;
