% ROBOT TYPE DEFINITION FOR SIMULATOR

sptr_dyn = tf(1, [0.5 1 0]);
steer_dyn = tf(1, [0.2 1]);
v_pid = [22.4749 7.0996 11.2515];
phi_pid = [20 0 3];
v_max = 1;
v_min = 0;
omega_max = 1;
vehicle_width = 0.1;
vehicle_length = 0.1;
safety_radius = sqrt( (vehicle_width/2)^2 + (vehicle_length/2)^2) * 1.1;
min_steer_radius = @(v) 1*v;

ranger_minmax = [0.1 2];
ranger_fov = 360;
ranger_num_beams = 120;

noise_pos = 0;
noise_odom_pos = 0;
noise_v = 0;
noise_a = 0;
noise_phi = 0;
noise_odom_phi = 0;
noise_ranger_mean = 0;
noise_ranger_stddev = 0;
