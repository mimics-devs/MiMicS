classdef Config < handle
%CONFIG Stores MiMicS related configuration data and constants.

    properties (Constant) 
        % Version number
        MIMICS_VERSION = '0.1 (May 5, 2017)';
        
        % If a robot's velocity is smaller than this value, it is considered stationary
        VELOCITY_EPS = 0.001;   
        
        % Different codes that will be used in the log of a simulation
        TERMINATION_CODES = struct('COMPLETE',1,'MAXITER',2,'DEADLOCK',3,'ALG_ERROR',4,'SETUP_ERROR',5);
        
        % Path to configuration file to load during startup
        CONFIG_FILE_PATH = ['.' filesep 'mimics.conf'];
        
        % Necessary directories
        NECESSARY_DIRS = {'Simulator',    ...
                ['Simulator' filesep 'General'], ...
                ['Simulator' filesep 'Drawing'], ...
                ['Simulator' filesep 'Menu'],    ...
                ['Simulator' filesep 'RobotModeling'] ...
                ['Simulator' filesep 'Util']};
    end

    properties
        % Timespan between simulation steps
        delta_t;
        
        % Timespan between plot updates. Should ideally be a multitude of
        % delta_t
        delta_t_plot; 
        
        % margin around the initial stage and map to be drawn (in m)
        plot_margin;
        
         % Default distance at which a robot is considered to have reached its goal.     
        dist_arrival;
        
        % Should a robot's distance to arrival be increased if a crash happened nearby? Often avoids time-consuming deadlock situations.
        enable_dist_arrival_inc;
        
        % Should the simulation be partially parallelized?
        enable_pct;
        
        % Directory for user-defined algorithms
        dir_algorithms;
        % Directory to collect simulation results
        dir_results;
        % Directory for user-defined scenarios
        dir_scenarios;
        % Directory for user-defined maps
        dir_maps;
        % Directory for user-defined robot types
        dir_robot_types;
        
        % List of properties to be logged by each robot, seperated by semicolons. To see a full list of properties that can be logged, look at the fieldnames of RobotModel.UNITS.
        robot_log_extra;
        
    end % properties (SetAccess = protected)
    
     methods (Access = private)
        function obj = Config
            obj.loadFromFile();
        end
        
    end %private methods
    
    methods (Static)
        
        function inst = getInstance()
        %GETINSTANCE Returns the one and only instance of this class. 
        %
        %   inst = getInstance()
        %
        % If there is no instance, an new will be created. 
        %
        % On creation, this class will load settings from a configuration
        % file or create a new one if it doesn't exist.
            persistent config_instance
            if isempty(config_instance)
                config_instance = Config;
                inst = config_instance;
            else
                inst = config_instance;
            end
        end
        
    end %static methods
    
    methods
        
        function checkDirectories(obj)
        %CHECKDIRECTORIES Makes sure directories MiMicS needs for execution
        %are still there.
            for dir_id = 1:length(obj.NECESSARY_DIRS)
                if ~exist( obj.NECESSARY_DIRS{dir_id}, 'dir')
                    error(['Path ''' obj.NECESSARY_DIRS{dir_id} ''' does not exist,' 'please reinstall']);
                else
                    addpath(genpath(obj.NECESSARY_DIRS{dir_id}));
                end
            end
        end % function checkDirectories
        
        function restoreDefaults(obj)
        % RESTOREDEFAULTS Restores factory settings of this file and saves
        % them.
        %
        %   obj.restoreDefaults()
        %
        % See also SAVETOFILE.
            
            obj.delta_t = 0.1;
            obj.dist_arrival = 0.25; 
            obj.enable_dist_arrival_inc = true;
            obj.enable_pct = false;
            obj.delta_t_plot = 0.1; 
            obj.plot_margin = 1.5;     
            obj.robot_log_extra = '';
            obj.dir_algorithms = ['.' filesep 'Algorithms'];
            obj.dir_results = ['.' filesep 'Results'];
            obj.dir_scenarios = ['.' filesep 'Scenarios'];
            obj.dir_maps = ['.' filesep 'Scenarios' filesep 'Maps'];
            obj.dir_robot_types = ['.' filesep 'RobotTypes'];
            fprintf('Factory settings of configuration restored.\n');
            obj.saveToFile();
        end
        
        function loadFromFile(obj)
        % LOADFROMFILE Loads the configuration from the designated file.
        %
        %   obj.loadFromFile()
        %
        % The path to the file is defined as the constant CONFIG_FILE_PATH
        % and typically './mimics.conf'. If this file does not exist, it
        % will be created with default settings.
            
            if ~exist(obj.CONFIG_FILE_PATH, 'file')
                fprintf('The configuration file was not found and will now be created with default settings.\n');
                obj.restoreDefaults();
                return;
            end
            
            conf_file = fopen(obj.CONFIG_FILE_PATH, 'r');
            if conf_file == -1
                error(['Error while trying to open file: ' obj.CONFIG_FILE_PATH]);
            end
            
            try
                data = textscan(conf_file, '%s = %s');
                keys = data{1,1,:,:};
                values = data{1,2,:,:};
                v = str2double(values); 
                idx = ~isnan(v);
                values(idx) = num2cell(v(idx));
                for i = 1:numel(keys)
                    obj.(keys{i}) = values{i};
                end
                fprintf('Loaded configuration from %s\n', obj.CONFIG_FILE_PATH);
            catch ME
                error('The error %s occured while loading the configuration file.\nTry deleting mimics.conf to restore the defaults on the next run.', ME.identifier);
            end
            
            fclose(conf_file); 
            
            if (obj.enable_pct > 0) && (license('test', 'distrib_computing_toolbox') <= 0)
               warning('enable_pct was set to true, but the distributed computing toolbox is not available. Setting enable_pct to false.');
               obj.enable_pct = false;
               obj.saveToFile();
            end
        end % function
        
        function saveToFile(obj)
        % SAVETOFILE Saves the current values of this class into the config
        % file.
        %
        %   obj.saveToFile()
        %
        % The path to the file is defined as the constant CONFIG_FILE_PATH
        % and typically './mimics.conf'. In there, the data will be stored
        % line by line in the format 'key = value'.
            
            conf_file = fopen(obj.CONFIG_FILE_PATH, 'w');
            if conf_file == -1
                error(['Error while trying to open file: ' obj.CONFIG_FILE_PATH]);
            end
            if (obj.enable_pct > 0) && (license('test', 'distrib_computing_toolbox') <= 0)
               warning('enable_pct was requested, but the distributed computing toolbox is not available. Setting enable_pct to false.');
               obj.enable_pct = false;
            end
            
            fprintf(conf_file, '%s = %g\n', 'delta_t', obj.delta_t); 
            fprintf(conf_file, '%s = %g\n', 'dist_arrival', obj.dist_arrival);
            fprintf(conf_file, '%s = %g\n', 'enable_dist_arrival_inc', obj.enable_dist_arrival_inc);
            fprintf(conf_file, '%s = %g\n', 'enable_pct', obj.enable_pct);
            fprintf(conf_file, '%s = %g\n', 'delta_t_plot', obj.delta_t_plot);
            fprintf(conf_file, '%s = %g\n', 'plot_margin', obj.plot_margin); 
            fprintf(conf_file, '%s = %s\n', 'robot_log_extra', obj.robot_log_extra); 
            fprintf(conf_file, '%s = %s\n', 'dir_algorithms', obj.dir_algorithms);
            fprintf(conf_file, '%s = %s\n', 'dir_robot_types', obj.dir_robot_types);
            fprintf(conf_file, '%s = %s\n', 'dir_scenarios', obj.dir_scenarios);
            fprintf(conf_file, '%s = %s\n', 'dir_maps', obj.dir_maps);
            fprintf(conf_file, '%s = %s',   'dir_results', obj.dir_results);   
            fclose(conf_file);
            
            fprintf('Saved configuration in %s\n', obj.CONFIG_FILE_PATH);
        end
        
       
    end % methods
    
end

